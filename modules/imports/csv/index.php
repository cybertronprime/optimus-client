<!DOCTYPE html>
<html lang="fr">
	
<head>
	<title>IMPORTS CSV</title>
	<meta name="viewport" content="width=410, initial-scale=1">
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/windows.css">
	<link rel="stylesheet" href="/css/statictable.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/windows.js"></script>
</head>

<body>

	<script type="text/javascript">
	
	const query = new URLSearchParams(window.location.search);
		
	api_call(query.get('server'),'optimus/logged', 'GET', {}, 'init');
	
	function init()
	{
		ajax = new XMLHttpRequest();
		ajax.open('GET', 'https://cloud.' + query.get('server') + '/files/' + query.get('db') + '/import.csv', false);
		ajax.withCredentials = true;
		ajax.send();
		CSV = ajax.responseText;
		CSV_lines = CSV.split(/\r\n|\n/);
		
		table = document.createElement('table');
		table.classList.add('statictable');
		document.body.appendChild(table);
		
		for(i=1; i < CSV_lines.length - 1; i++)
		{
			columns = CSV_lines[i].split(/;/);
			tr = table.insertRow();
			tr.style.border = '1px solid #000000';
			tr.id = i;
			td1 = tr.insertCell();
			td1.innerHTML = columns[0];
			td2 = tr.insertCell();
			td2.style.textAlign = 'left';
			td2.innerHTML = columns[1];
		}
		
		button = document.createElement('button');
		button.innerHTML = 'IMPORTER';
		button.onclick = function(){import_all(CSV)}
		document.body.appendChild(button);
	}
	
	function import_all(CSV)
	{
		CSV_lines = CSV.split(/\r\n|\n/);
		
		table = document.createElement('table');
		table.classList.add('statictable');
		document.body.appendChild(table);
		
		for(i=1; i < CSV_lines.length - 1; i++)
		{
			columns = CSV_lines[i].split(/;/);
			imported = api_call_sync(query.get('server'),'optimus/' + query.get('db') + '/dossiers', 'POST', {"numero":columns[0], "nom":columns[1].replace('+','-').replace('+','-')}, 'init');
			if (imported.code == 201)
				document.getElementById(i).style.background = '#00FF00';
			else
				document.getElementById(i).style.background = '#FF0000';
		}
	}

	</script>
</body>
</html>