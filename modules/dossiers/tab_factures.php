<?php
die('fonction à venir');
if ($_GET['start']=='') $_GET['start'] = '2000-01-01';
if ($_GET['end']=='') $_GET['end'] = date('Y').'-12-31';
if ($_GET['paid']=='') $_GET['paid'] = 0;
if ($_GET['notpaid']=='') $_GET['notpaid'] = 1;
?>

<!doctype html>
<html lang="fr" style="overflow-x:hidden">
	<head>
		<meta charset="utf-8" />
		<link href="/css/main.css" rel="stylesheet" />
		<link href="/css/statictable.css" rel="stylesheet" />
		<style>
			table tbody tr:hover
			{
				background-color:#FF8000;
			}	
		</style>
	</head>

	<body align="center">
		
		<table align="center" style="margin-top:20px">
			<tr style="background-color:transparent">
				<td  style="border:0px;font:bold 14px Verdana" colspan="2">PERIODE<br/><br/><input type="date" id="start" onblur="submit_form()" value="<?=$_GET['start']?>" /> AU <input type="date" id="end" onblur="submit_form()" value="<?=$_GET['end']?>" /><br/><br/></td>
			</tr>
			<tr style="background-color:transparent">
				<td style="border:0px;font:bold 14px Verdana">Payées<br/><input type="checkbox" id="paid" onclick="submit_form()" <?= ($_GET['paid']==1)?'checked':''?> /></td>
				<td style="border:0px;font:bold 14px Verdana">Impayées<br/><input type="checkbox" id="notpaid" onclick="submit_form()" <?= ($_GET['notpaid']==1)?'checked':''?> /></td>
			</tr>
		</table>
		
		<table class="statictable" align="center" style="margin-top:20px">
			<thead>
				<tr>
					<td align="center" width="150">FACTURE</td>
					<td align="center" width="100">DATE</td>
					<td align="center" width="100">MONTANT</td>
					<td align="center" width="100">PAYE</td>
					<td align="center" width="100">RESTE</td>
				</tr>
			</thead>
			<tbody>
				<?php
					$interventions_query = mysqli_query($connection,"SELECT * FROM " . $_GET['db'].".interventions WHERE db IS NOT NULL AND dossier = " . $_GET['id'] . " ORDER BY date_ouverture");
					while($interventions = mysqli_fetch_array($interventions_query))
					{
						$facture_query = mysqli_query($connection,"SELECT * FROM " . $interventions['db'] . ".factures WHERE db = '" . $_GET['db'] . "' AND intervention = " . $interventions['id'] . " AND date >= '" . mysqli_real_escape_string($connection,$_GET['start']) . "' AND date <= '" .  mysqli_real_escape_string($connection,$_GET['end']) . "'");
						if (mysqli_num_rows($facture_query)>0)
						{
							$facture = mysqli_fetch_array($facture_query);
							
							$recettes_query = mysqli_query($connection,"SELECT SUM(montant) as paid FROM " . $interventions['db'] . ".compta_recettes WHERE facture = " . $facture['id']);
							$recettes = mysqli_fetch_array($recettes_query);
							
							$reste = $facture['total'] - $recettes['paid'];
							
							if (($_GET['paid']==1 && $reste==0) OR ($_GET['notpaid']==1 && $reste > 0))
							{
								if (in_array($interventions['db'].'.factures',$user['read']))
								{
									echo '<tr style="cursor:pointer" onclick="window.open(\'/modules/factures/editor.php?db=' . $interventions['db'] . '&id=' . $facture['id'] . '\',\'editor\')">';
										echo '<td align="left">' . $facture['numero'] . ' (' . $interventions['db'] . ')</td>';
										echo '<td>' . date('d/m/Y',strtotime($facture['date'])) . '</td>';
										echo '<td align="right">' . number_format($facture['total'],2,',',' ') . ' &euro;</td>';
										echo '<td align="right">' . number_format($recettes['paid'],2,',',' ') . ' &euro;</td>';
										echo '<td align="right"' . (($reste > 0)?' style="color:#FF2000"':'') . '>' . number_format($reste,2,',',' ') . ' &euro;</td>';
									echo '</tr>';
									
									$total_montant += $facture['total'];
									$total_paid += $recettes['paid'];
									$total_reste += $reste;
								}
								else
								{
									echo '<tr>';
										echo '<td align="left">??-?????? (' . $interventions['db'] . ')</td>';
										echo '<td>??/??/????</td>';
										echo '<td align="right">????? &euro;</td>';
										echo '<td align="right">????? &euro;</td>';
										echo '<td align="right">????? &euro;</td>';
									echo '</tr>';
								}
								
							}
						}
					}
				?>
			</tbody>
			<tfoot style="background-color:transparent">
				<?php
					echo '<tr>';
						echo '<td colspan="2" style="border:0px;">&nbsp;</td>';
						echo '<td align="right" style="background-color:#5E79B0">' . number_format($total_montant,2,',',' ') . ' &euro;</td>';
						echo '<td align="right" style="background-color:#5E79B0">' . number_format($total_paid,2,',',' ') . ' &euro;</td>';
						echo '<td align="right" style="background-color:#5E79B0' . (($total_reste > 0)?';color:#FF8000"':'') . '">' . number_format($total_reste,2,',',' ') . ' &euro;</td>';
					echo '</tr>';
				?>
			</tfoot>
		</table>
		
		<script>
			function submit_form()
			{
				lien  = '<?=$_SERVER['SCRIPT_NAME']?>';
				lien += '?db=<?=$_GET['db']?>';
				lien += '&id=<?=$_GET['id']?>';
				lien += '&start='+document.getElementById('start').value;
				lien += '&end='+document.getElementById('end').value;
				if (document.getElementById('paid').checked==true) lien += '&paid=1'; else lien += '&paid=0';
				if (document.getElementById('notpaid').checked==true) lien += '&notpaid=1'; else lien += '&notpaid=0';
				window.location = lien;
			}
		</script>
		
	</body>
</html>