<!DOCTYPE html>
<html lang="fr">
	<head>
		<link rel="stylesheet" href="/css/main.css">
		<script src="/js/main.js"></script>
		<script src="/js/api_call.js"></script>
		<script src="/js/editor.js"></script>
	</head>
	
	<body>
	
		<textarea id="notes" onblur="api_call(query.get('server'),'optimus/'+query.get('db')+'/dossiers/'+itm.id,'PATCH',{'notes':this.value},'updated',this)" style="position: fixed;top:50%;left:50%;width:90%;height:90%;transform: translate(-50%, -50%);"></textarea>
		
		<script type="text/javascript">
			const query = new URLSearchParams(window.location.search);
			module = 'dossiers';
			itm = {};
			api_call(query.get('server'),'optimus/'+query.get('db')+'/dossiers/'+query.get('id'),'GET',{}, 'init');
			
			function init(response)
			{
				itm = response.data[0];
				authorizations = response.authorizations;
				editor_init();
				document.body.style.display = 'inline';
			}
			
			var update_link = new BroadcastChannel('update_link');
			update_link.onmessage = function (ev) {window.location.reload()}
			window.onblur = function(){update_link.postMessage('update_link')}
		</script>
	</body>
</html>