<!DOCTYPE html>
<html>
	<head>
		<title>FILE EXPLORER</title>
		<meta charset="UTF-8">
		<link href="/css/davexplorer.css" rel="stylesheet" type="text/css" />
		<script src="/js/davclient.js"></script>
		<script src="/js/davexplorer.js"></script>
		<script src="/js/api_call.js"></script>
	</head>
	
	<body>
	
	</body>
	
	<script>
	const query = new URLSearchParams(window.location.search);
	api_call(query.get('server'), 'optimus/'+query.get('db')+'/dossiers/'+query.get('id'), 'GET', {},'init');
	
	function init(response)
	{
		client = new dav.Client({baseUrl : "https://cloud." + query.get('server')});
		davexplorer = new webdav_explorer("https://cloud."+query.get('server'),"/files/"+query.get('db')+"/==DOSSIERS==/"+response.data[0].nom,1);
		container = davexplorer.init();
		document.body.appendChild(container);
	}
	</script>
</html>
	
