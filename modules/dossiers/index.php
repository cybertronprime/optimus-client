<!DOCTYPE html>
<html lang="fr">
	
<head>
	<title>DOSSIERS</title>
	<meta name="viewport" content="width=410, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="/modules/dossiers/favicon-32x32.png">
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/windows.css">
	<link rel="stylesheet" href="/css/dynamictable.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/windows.js"></script>
	<script src="/js/dynamictable.js"></script>
	<script src="/js/number_format.js"></script>
	<script src="/js/cookies.js"></script>
</head>

<body>

	<script type="text/javascript">

		const query = new URLSearchParams(window.location.search);
		
		dynamictable = new Object();
		dynamictable.user_server = get_cookie('server');
		dynamictable.user_db = get_cookie('db');
		dynamictable.server = query.get('server');
		dynamictable.db = query.get('db');
		dynamictable.module = 'dossiers';
		dynamictable.grid = <?php include_once('dossiers.json');?>;
		dynamictable.advanced_search = [];
		dynamictable.global_search = "";
		dynamictable.page = 1;
		dynamictable.results = 30;
		dynamictable.version = 45;
		
		api_call(query.get('server'),'optimus/'+query.get('db')+'/settings', 'GET', {'module':dynamictable.module}, 'init');
		
		function init(response)
		{
			dynamictable_init(dynamictable);
			insert = document.createElement('button');
			insert.innerHTML = 'NOUVEAU DOSSIER';
			insert.onclick = function()
			{
				api_call(query.get('server'),'optimus/'+query.get('db')+'/dossiers', 'POST', {}, 'dossier_created');
			}
			document.getElementById('controls').appendChild(insert);
		}
		
		function dossier_created(dossier)
		{
			refresher = document.createElement('iframe');
			refresher.src = 'https://webmail.'+query.get('server')+'/?_task=settings&_action=folders';
			refresher.style.display = 'none';
			document.body.appendChild(refresher);
			editor = window.open('/modules/dossiers/editor.php?server='+query.get('server')+'&db='+query.get('db')+'&id='+dossier.data.id,'editor');
			setTimeout(function(){window.location.reload()},500);
		}
		
		function dossier(row,column)
		{
			td.style.cursor='pointer';
			td.style.color='#0000B0';
			td.onclick=function()
			{
				editor = window.open('/modules/dossiers/editor.php?server='+query.get('server')+'&db='+query.get('db')+'&id='+row[0],'editor');
			}
			return row[column];
		}
		
		function folder(row,column)
		{
			td.style.cursor='pointer';
			td.innerHTML = '<img src="/modules/fichiers/favicon-32x32.png" style="width:16px"/>';
			td.onclick=function()
			{
				console.log(row);
				window.open('/modules/dossiers/tab_fichiers.php?server='+query.get('server')+'&db='+query.get('db')+'&id='+ row[0])
			}
			return '<img src="/modules/fichiers/icon.svg" style="width:16px;filter:drop-shadow(1px 1px black)"/>';
		}
		
		function emails(row,column)
		{
			td.style.cursor='pointer';
			td.innerHTML = 'link';
			td.onclick=function()
			{
				console.log(row);
				window.open('/modules/dossiers/tab_emails.php?server='+query.get('server')+'&db='+query.get('db')+'&id='+ row[0])
			}
			return '<img src="/modules/emails/icon.svg" style="width:16px;filter:drop-shadow(1px 1px black)"/>';
		}
		
		var update_link = new BroadcastChannel('update_link');
		update_link.onmessage = function (ev) {window.location.reload()}
		window.onblur = function(){update_link.postMessage('update_link')}
	</script>
</body>
</html>