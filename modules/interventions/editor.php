<!doctype html>
<html>
	<head>
		<title>DILIGENCES</title>
		<link rel="shortcut icon" type="image/png" href="/modules/interventions/favicon-32x32.png">
		<link href="/css/main.css" rel="stylesheet" type="text/css">
		<link href="/css/editor.css" rel="stylesheet" type="text/css">
		<link href="/css/statictable.css" rel="stylesheet" type="text/css">
		<script src="/js/api_call.js"></script>
		<script src="/js/main.js"></script>
		<script src="/js/editor.js"></script>
		<script src="/js/number_format.js"></script>
		<script src="/js/cookies.js"></script>
	</head>
	
	<body>

		<table id="diligences" class="statictable" align="center" style="border-collapse:collapse;margin-top:20px">
			<thead style="background:transparent;color:#000000">
				<tr>
					<td align="center" style="font:bold 14px Verdana;border:0px;white-space:nowrap" colspan="3">
						<div id="structure_de_facturation" style="display:none">
							Structure de facturation<br/>
							<select id="structures" style="width:auto" onchange="structure=this.value.split('`');api_call(structure[0],'optimus/' + structure[1] + '/members','GET',{},'get_members')"></select>
						</div>
						<br/>
						Taux Horaire : <input id="tarif" type="number" step="1" class="unstyled" style="width:62px;text-align:right" onfocus="this.classList.add('editing')" onchange="api_call(query.get('server'),'optimus/' + query.get('db') + '/dossiers/' + query.get('dossier') + '/interventions/' + query.get('id'),'PATCH',{'tarif':this.value},'updated',this);" onblur="this.classList.remove('editing')" onkeydown="if (event&&event.keyCode==13 ) this.blur()"/><br/>
						<br/>
						<img id="reset" src="/lib/fontawesome/sync.svg" style="width:12px;margin-right:4px;vertical-align:-2px" onclick="document.getElementById('timer').value = '00:00:00'"/>
						<input id="timer" type="time" step='1' min="00:00:00" value="00:00:00" class="unstyled" required>
						<img id="pause" src="/lib/fontawesome/pause.svg" style="width:10px;vertical-align:-2px;margin-left:4px;" onclick="clearInterval(timer);this.style.display='none';document.getElementById('play').style.display=''"/>
						<img id="play" src="/lib/fontawesome/play.svg" style="width:10px;vertical-align:-2px;margin-left:4px;display:none" onclick="timer = setInterval(function(){document.getElementById('timer').stepUp()},1000);this.style.display='none';document.getElementById('pause').style.display=''"/><br/>
						<br/>
					</td>
					<td id="intervention_info" align="center" style="border:0px" colspan="5">
						<input id="dossier_numero" readonly style="border:0;background:transparent;font:bold 24px Verdana;width:700px;text-align:center;outline:0"/><br/>
						<input id="dossier_nom" readonly style="border:0;background:transparent;font:bold 24px Verdana;width:700px;text-align:center;outline:0"/><br/>
						<input id="intervention_description" style="border:0;background:transparent;font:bold 24px Verdana;width:700px;text-align:center" onfocus="this.classList.add('editing')" onchange="api_call(query.get('server'),'optimus/' + query.get('db') + '/dossiers/' + query.get('dossier') + '/interventions/' + query.get('id'),'PATCH',{'description':this.value},'updated',this)" onblur="this.classList.remove('editing')" onkeydown="if (event&&event.keyCode==13 ) this.blur()"/><br/>
					</td>
					<td style="font:bold 12px Verdana;border:0px;padding:auto"  colspan="2" align="right">
						HONORAIRES :<br/>
						FRAIS :<br/>
						TOTAL HT :<br/>
						DEBOURS :<br/>
						TVA :<br/>
						<br/>
						TOTAL TTC :<br/>
					</td>
					<td style="font:bold 12px Verdana;border:0px" colspan="2" align="right">	
						<span id="total_honoraires"></span><br/>
						<span id="total_frais"></span><br/>
						<span id="total_ht"></span><br/>
						<span id="total_debours"></span><br/>
						<span id="total_tva"></span><br/>
						<br/>
						<span id="total_ttc"></span><br/>
					</td>
				</tr>
				<tr style="background-color:#5E79B0;color:#FFFFFF;font:bold 10px Verdana">
					<td align="center" width="22" style="border:1px solid #000000;cursor:pointer"><input type="checkbox" onclick="for (i=0; i<document.getElementById('diligences').tBodies[0].rows.length; i++) document.getElementById('diligences').tBodies[0].rows[i].cells[0].lastChild.checked=this.checked"></td>
					<td align="center" width="66" style="border:1px solid #000000">DATE</td>
					<td align="center" width="150" style="border:1px solid #000000">INTERVENANT</td>
					<td align="center" width="150" style="border:1px solid #000000">BENEFICIAIRE</td>
					<td align="center" width="36" style="border:1px solid #000000">COM</td>
					<td align="center" width="540" style="border:1px solid #000000">DESCRIPTION</td>
					<td align="center" width="70" style="border:1px solid #000000">CATEGORIE</td>
					<td align="center" width="126" style="border:1px solid #000000">TYPE</td>
					<td align="center" width="65" style="border:1px solid #000000">COEF</td>
					<td align="center" width="65" style="border:1px solid #000000">TARIF</td>
					<td align="center" width="65" style="border:1px solid #000000">TOTAL</td>
					<td align="center" width="24" style="border:1px solid #000000"><img src="/lib/fontawesome/help.svg" style="width:12px;filter:invert()" onclick="help()"/></td>
				</tr>
			</thead>
			<tbody>
			</tbody>
			<tfoot>
				<tr>
					<td style="text-align:center;border-right:0">
						<img src="/lib/fontawesome/level-up.svg" style="width:10px;margin-left:14px;transform:rotate(90deg)"/>
					</td>
					<td style="text-align:center;border-left:0">
						<img src="/lib/fontawesome/cut.svg" style="width:12px;margin-right:6px;filter:invert();cursor:pointer" title="Couper" onclick="api_call(query.get('server'),'allspark/logged','GET',{},'selection_cut')"/>
						<img src="/lib/fontawesome/copy.svg" style="width:12px;margin-right:6px;filter:invert();cursor:pointer" title="Copier" onclick="selection_copy()"/>
						<img src="/lib/fontawesome/paste.svg" style="width:12px;margin-right:6px;filter:invert();cursor:pointer" title="Coller" onclick="api_call(query.get('server'),'allspark/logged','GET',{},'selection_paste')"/>
						<img src="/lib/fontawesome/trash.svg" style="width:12px;filter:invert();cursor:pointer" title="Supprimer" onclick="api_call(query.get('server'),'allspark/logged','GET',{},'selection_delete')"/>
					</td>
				</tr>
			</tfoot>
		</table>
		
		<br/>
		
		<button id="diligence_create_button" onclick="diligence_create()">AJOUTER UNE DILIGENCE</button><br/><br/>
		<button id="facture_create_button" onclick="facture_create()">FACTURER</button>&nbsp;
		<button onclick="print()">IMPRIMER</button><br/><br/>
		<button id="facture_button" style="display:none">AFFICHER LA FACTURE</button> 
		
		<br/><br/>
		
		<select id="tva_rates" style="display:none"></select>
		<select id="intervenants" style="display:none"></select>
		<select id="members" style="display:none"></select>

	</body>

	<script language="JavaScript" type="text/javascript">	
		const query = new URLSearchParams(window.location.search);
		module = 'diligences';
		facture = 0;

		api_call(query.get('server'),'optimus/' + query.get('db') + '/dossiers/' + query.get('dossier') + '/interventions/' + query.get('id'),'GET',{},'init');
		function init(intervention)
		{
			facture = intervention.data[0].facture;
			document.getElementById('facture_button').onclick = function(){window.open('/modules/factures/editor.php?server='+intervention.data[0].server+'&db='+intervention.data[0].db+'&id='+intervention.data[0].facture,'editor')}
			dossier = api_call_sync(query.get('server'),'optimus/' + query.get('db') + '/dossiers/' + intervention.data[0].dossier,'GET',{});
			document.getElementById('dossier_numero').value = dossier.data[0].numero;
			document.getElementById('dossier_nom').value = dossier.data[0].nom;
			document.getElementById('intervention_description').value = intervention.data[0].description || '';
			document.getElementById('tarif').value = intervention.data[0].tarif;
			document.getElementById('timer').value = intervention.data[0].timer;
			
			populate(document.getElementById('tva_rates'), 'tva_rates', false);
			document.getElementById('tva_rates').value = 6;
			
			users = api_call_sync(query.get('server'),'optimus/users','GET',{});
			for (user of users.data)
				document.getElementById('intervenants').options[document.getElementById('intervenants').options.length] = new Option(user.email, user.id);
			
			structures = api_call_sync(query.get('server'),'optimus/' + query.get('db') + '/structures','GET',{});
			for (structure of structures.data)
				document.getElementById('structures').options[document.getElementById('structures').options.length] = new Option(structure.db, structure.server+'`'+structure.db+'`'+structure.id);
			for (structure of structures.data)
				if(intervention.data[0].server == structure.server && intervention.data[0].db == structure.db)
					document.getElementById('structures').value = structure.server+'`'+structure.db+'`'+structure.id;
			if (document.getElementById('structures').options.length > 1)
				document.getElementById('structure_de_facturation').style.display = 'block';
			
			api_call(intervention.data[0].server,'optimus/' + intervention.data[0].db + '/members','GET',{},'init_2');
		}
		
		function init_2(members)
		{
			for (member of members.data)
				document.getElementById('members').options[document.getElementById('members').options.length] = new Option(member.member, member.id);
			for (member of members.data)
				if(member.member == query.get('db'))
					document.getElementById('members').value = member.id;
			
			diligences = api_call_sync(query.get('server'),'optimus/' + query.get('db') + '/dossiers/' + query.get('dossier') + '/interventions/' + query.get('id') + '/diligences','GET',{});
			for (diligence of diligences.data)
				diligence_display(diligence.id, diligence.date, diligence.intervenant, diligence.beneficiaire, diligence.commission, diligence.description, diligence.categorie, diligence.type, diligence.coefficient, diligence.tarif);
			total();
			
			advanced_mode();
			
			if (facture > 0)
				lock();
		}
		
		function advanced_mode()
		{
			document.getElementById('diligences').rows[1].cells[2].style.display = '';
			document.getElementById('diligences').rows[1].cells[3].style.display = '';
			document.getElementById('diligences').rows[1].cells[4].style.display = '';
			for (row of document.getElementById('diligences').tBodies[0].rows)
			{
				row.cells[2].style.display = '';
				row.cells[3].style.display = '';
				row.cells[4].style.display = '';
			}
			document.getElementById('diligences').tHead.rows[0].cells[0].colSpan=3;
			document.getElementById('diligences').tHead.rows[0].cells[1].colSpan=5;
			
			if (document.getElementById('intervenants').options.length == 1)
			{
				document.getElementById('diligences').rows[1].cells[2].style.display = 'none';
				for (row of document.getElementById('diligences').tBodies[0].rows)
					row.cells[2].style.display = 'none';
				document.getElementById('diligences').tHead.rows[0].cells[1].colSpan--;
			}
			
			if (document.getElementById('members').options.length == 1)
			{
				document.getElementById('diligences').rows[1].cells[3].style.display = 'none';
				document.getElementById('diligences').rows[1].cells[4].style.display = 'none';
				for (row of document.getElementById('diligences').tBodies[0].rows)
				{
					row.cells[3].style.display = 'none';
					row.cells[4].style.display = 'none';
				}
				document.getElementById('diligences').tHead.rows[0].cells[0].colSpan--;
				document.getElementById('diligences').tHead.rows[0].cells[1].colSpan--;
			}
		}
		
		function get_members(structure)
		{
			structure = document.getElementById('structures').value.split('`');
			
			intervention = api_call_sync(query.get('server'),'optimus/' + query.get('db') + '/dossiers/' + query.get('dossier') + '/interventions/' + query.get('id'),'PATCH',{"server":structure[0],"db":structure[1]});
			
			document.getElementById('members').innerHTML = '';
			members = api_call_sync(structure[0],'optimus/' + structure[1] + '/members','GET',{});
			for (member of members.data)
				document.getElementById('members').options[document.getElementById('members').options.length] = new Option(member.member, member.id);
			advanced_mode();
			
			for (row of document.getElementById('diligences').tBodies[0].rows)
			{
				row.cells[3].lastChild.innerHTML=document.getElementById('members').innerHTML;
				for (member of members.data)
					if(member.member == query.get('db'))
						row.cells[3].lastChild.value = member.id;
				row.cells[3].lastChild.onchange();
			}
		}
		
		function diligence_create()
		{
			api_call(query.get('server'),'allspark/logged','GET',{},'diligence_created');
		}
		
		function diligence_created(input)
		{
			if (input.data)
			{
				intervenant = input.data.id;
			
				coefficient = document.getElementById('timer').value.split(':');
				coefficient = (parseInt(coefficient[0]) + (coefficient[1]/60)).toFixed(2);
				
				diligence = api_call_sync(query.get('server'),'optimus/' + query.get('db') + '/dossiers/' + query.get('dossier') + '/interventions/' + query.get('id') + '/diligences','POST',{'intervention':query.get('id'),'intervenant':intervenant,'beneficiaire':document.getElementById('members').value,'coefficient':coefficient,'tarif':document.getElementById('tarif').value});
			}
			else
			{
				input.intervention = query.get('id');
				diligence = api_call_sync(query.get('server'),'optimus/' + query.get('db') + '/dossiers/' + query.get('dossier') + '/interventions/' + query.get('id') + '/diligences','POST',input);
			}
			
			if (diligence.code == 201)
			{
				diligence = diligence.data;
				diligence_display(diligence.id, diligence.date, diligence.intervenant, diligence.beneficiaire, diligence.commission, diligence.description, diligence.categorie, diligence.type, diligence.coefficient, diligence.tarif);
				document.getElementById('diligences').tBodies[0].lastChild.cells[5].lastChild.focus();
				advanced_mode();
			}
		}

		function diligence_delete(id)
		{
			api_call(query.get('server'),'optimus/'+query.get('db')+'/dossiers/'+query.get('dossier')+'/interventions/'+query.get('id')+'/diligences/'+id,'DELETE',{},'diligence_deleted',document.getElementById(id))
		}
		
		function diligence_deleted(diligence,obj)
		{
			if (diligence.code == 200)
			{
				document.getElementById(obj.id).remove();
				total();
			}
		}

		function diligence_display(id, date, intervenant, beneficiaire, commission, description, categorie, type, coefficient, tarif)
		{
			table = document.getElementById('diligences');

			tr = table.tBodies[0].insertRow();
			tr.style.font='11px Verdana';
			tr.id = id; 

			td0 = tr.insertCell();
			td0.style.border='1px solid #000000';
			td0.onclick = function(event){if (event.srcElement.type!='checkbox') this.lastChild.click();}
			td0.innerHTML = '<input type="checkbox" />';

			td1 = tr.insertCell();
			td1.style.textAlign = "center";
			input = document.createElement('input');
			input.id = 'date';
			input.type = 'date';
			input.required = true;
			input.value = date;
			input.classList.add('unstyled');
			input.style = "width:98px;height:13px;border:0px;outline:0px;font:normal 11px Verdana;background-color:transparent";
			input.maxLength = 10;
			input.onfocus = function(){this.classList.add('editing')};
			input.onkeydown = function(){if (event&&event.keyCode==13 ) this.blur()};
			input.onchange = function(){api_call(query.get('server'),'optimus/'+query.get('db')+'/dossiers/'+query.get('dossier')+'/interventions/'+query.get('id')+'/diligences/'+this.parentNode.parentNode.id,'PATCH',{[this.id]:this.value},'updated',this)}
			input.onblur = function(){this.classList.remove('editing')}
			td1.appendChild(input);
			td1.oncontextmenu = function(){this.lastChild.focus(); this.lastChild.value = new Date().getFullYear() + '-' + (new Date().getMonth()+1).toString().padStart(2,'0') + '-' + new Date().getDate().toString().padStart(2,'0'); return false;}

			td2 = tr.insertCell();
			td2.style.textAlign = 'left';
			td2.style.padding = '1px';
			td2.appendChild(document.getElementById('intervenants').cloneNode(true));
			td2.lastChild.classList.add('unstyled');
			td2.lastChild.style.display = 'inline';
			td2.lastChild.style = "width:auto;height:20px;min-width:150px;border:0px;background:transparent;outline:0px;font:normal 11px Verdana;-webkit-appearance:none";
			td2.lastChild.id = 'intervenant';
			td2.lastChild.value = intervenant;
			td2.lastChild.onfocus = function(){this.classList.add('editing')};
			td2.lastChild.onchange = function(){api_call(query.get('server'),'optimus/'+query.get('db')+'/dossiers/'+query.get('dossier')+'/interventions/'+query.get('id')+'/diligences/'+this.parentNode.parentNode.id,'PATCH',{[this.id]:this.value},'updated',this)}
			td2.lastChild.onblur = function(){this.classList.remove('editing')}

			td3 = tr.insertCell();
			td3.style.textAlign = 'left';
			td3.style.padding = '1px';
			td3.classList.add('beneficiaire');
			td3.appendChild(document.getElementById('members').cloneNode(true));
			td3.lastChild.classList.add('unstyled');
			td3.lastChild.style.display = 'inline';
			td3.lastChild.style = "width:auto;height:20px;min-width:150px;border:0px;background:transparent;outline:0px;font:normal 11px Verdana;-webkit-appearance:none";
			td3.lastChild.id = 'beneficiaire';
			td3.lastChild.value = beneficiaire;
			td3.lastChild.onfocus = function(){this.classList.add('editing')};
			td3.lastChild.onchange = function(){api_call(query.get('server'),'optimus/'+query.get('db')+'/dossiers/'+query.get('dossier')+'/interventions/'+query.get('id')+'/diligences/'+this.parentNode.parentNode.id,'PATCH',{[this.id]:this.value},'updated',this)}
			td3.lastChild.onblur = function(){this.classList.remove('editing')}

			td4 = tr.insertCell();
			td4.style.textAlign = "center";
			td4.classList.add('commission');
			input = document.createElement('input');
			input.id = 'commission';
			input.value = commission;
			input.type = 'number';
			input.setAttribute('min',0);
			input.setAttribute('max',1);
			input.setAttribute('step',0.01);
			input.classList.add('unstyled');
			input.style = "width:24px;height:13px;border:0px;outline:0px;font:normal 11px Verdana;background-color:transparent;text-align:right";
			input.onfocus = function(){this.select();this.classList.add('editing')};
			input.onkeydown = function(){if (event&&event.keyCode==13 ) this.blur()};
			input.onchange = function(){api_call(query.get('server'),'optimus/'+query.get('db')+'/dossiers/'+query.get('dossier')+'/interventions/'+query.get('id')+'/diligences/'+this.parentNode.parentNode.id,'PATCH',{[this.id]:this.value},'updated',this)}
			input.onblur = function(){this.classList.remove('editing')}
			td4.appendChild(input);
			td4.oncontextmenu = function(){this.lastChild.focus();this.lastChild.value='0.00';return false}

			td5 = tr.insertCell();
			td5.style.textAlign = "left";
			textarea = document.createElement('textarea');
			textarea.id = 'description';
			textarea.style = "padding:3px;width:540px;resize:none;overflow:hidden;border:0px;outline:0px;font:normal 11px Verdana;background-color:transparent";
			textarea.value = description;
			textarea.onfocus = function(){this.select();this.classList.add('editing')};
			textarea.onkeyup = function(){this.style.height=(this.value.match(/\n/g)?this.value.match(/\n/g).length:0)*13+12+'px';}
			textarea.onkeydown = function(){if(!event.ctrlKey && event.keyCode==13) this.style.height=this.offsetHeight+12+'px';}
			textarea.onchange = function(){api_call(query.get('server'),'optimus/'+query.get('db')+'/dossiers/'+query.get('dossier')+'/interventions/'+query.get('id')+'/diligences/'+this.parentNode.parentNode.id,'PATCH',{[this.id]:this.value},'updated',this)}
			textarea.onblur = function(){this.classList.remove('editing')}
			textarea.onkeyup();
			td5.appendChild(textarea);
			td5.oncontextmenu = function(){this.lastChild.focus();this.lastChild.value='';this.lastChild.onkeyup();return false}

			td6 = tr.insertCell();
			td6.style.textAlign = 'left';
			td6.style.padding = '1px';
			select = document.createElement('select');
			select.id = 'categorie';
			select.style = "border:0px;background:transparent;outline:0px;height:20px;width:74px;font:normal 11px Verdana;-webkit-appearance:none";
			select.onfocus = function(){this.classList.add('editing')};
			select.onload = function(){populate(this, 'diligences_categories', false, false, categorie)}
			select.onchange = function()
			{
				populate(this.parentNode.parentNode.cells[7].lastChild, 'diligences_subcategories', false, false, 1, this.value);
				api_call(query.get('server'),'optimus/'+query.get('db')+'/dossiers/'+query.get('dossier')+'/interventions/'+query.get('id')+'/diligences/'+this.parentNode.parentNode.id,'PATCH',{[this.id]:this.value,"type":1},'updated',this)
			}
			select.onblur = function(){this.classList.remove('editing')}
			td6.appendChild(select);
			select.onload();

			td7 = tr.insertCell();
			td7.style.textAlign = 'left';
			td7.style.padding = '1px';
			select = document.createElement('select');
			select.id = 'type';
			select.style = "border:0px;background:transparent;outline:0px;height:20px;width:120px;font:normal 11px Verdana;-webkit-appearance:none";
			select.onfocus = function(){this.classList.add('editing')};
			select.onload = function(){populate(this, 'diligences_subcategories', false, false, type, categorie)};
			select.onchange = function(){api_call(query.get('server'),'optimus/'+query.get('db')+'/dossiers/'+query.get('dossier')+'/interventions/'+query.get('id')+'/diligences/'+this.parentNode.parentNode.id,'PATCH',{[this.id]:this.value},'updated',this)}
			select.onblur = function(){this.classList.remove('editing')}
			td7.appendChild(select);
			select.onload();

			td8 = tr.insertCell();
			td8.style.textAlign = "center";
			input = document.createElement('input');
			input.id = 'coefficient';
			input.value = coefficient;
			input.type = 'number';
			input.setAttribute('min',0);
			input.setAttribute('max',99999999.99);
			input.setAttribute('step',0.01);
			input.classList.add('unstyled');
			input.style = "text-align:right;width:74px;height:13px;border:0px;outline:0px;font:normal 11px Verdana;background-color:transparent";
			input.onfocus = function(){this.select();this.classList.add('editing')};
			input.onkeydown = function(){if (event&&event.keyCode==13 ) this.blur()};
			input.onchange = function()
			{
				api_call(query.get('server'),'optimus/'+query.get('db')+'/dossiers/'+query.get('dossier')+'/interventions/'+query.get('id')+'/diligences/'+this.parentNode.parentNode.id,'PATCH',{[this.id]:this.value},'updated',this);
				this.parentNode.parentNode.cells[10].getElementsByTagName('INPUT')[0].value = (this.parentNode.parentNode.cells[8].getElementsByTagName('INPUT')[0].value*this.parentNode.parentNode.cells[9].getElementsByTagName('INPUT')[0].value).toFixed(2);
				total()
			}
			input.onblur = function(){this.classList.remove('editing')}
			td8.appendChild(input);
			td8.oncontextmenu = function(){this.lastChild.focus();this.lastChild.value='0.00';this.lastChild.onchange();return false}

			td9 = tr.insertCell();
			td9.style.textAlign = "center";
			input = document.createElement('input');
			input.id = 'tarif';
			input.value = tarif;
			input.type = 'number';
			input.setAttribute('min',0);
			input.setAttribute('max',99999999.99);
			input.setAttribute('step',0.01);
			input.classList.add('unstyled');
			input.style = "text-align:right;width:74px;height:13px;border:0px;outline:0px;font:normal 11px Verdana;background-color:transparent";
			input.onfocus = function(){this.select();this.classList.add('editing')};
			input.onkeydown = function(){if (event&&event.keyCode==13 ) this.blur()};
			input.onchange = function()
			{
				api_call(query.get('server'),'optimus/'+query.get('db')+'/dossiers/'+query.get('dossier')+'/interventions/'+query.get('id')+'/diligences/'+this.parentNode.parentNode.id,'PATCH',{[this.id]:parseFloat(this.value).toFixed(2)},'updated',this);
				this.parentNode.parentNode.cells[10].getElementsByTagName('INPUT')[0].value=(this.parentNode.parentNode.cells[8].getElementsByTagName('INPUT')[0].value*this.parentNode.parentNode.cells[9].getElementsByTagName('INPUT')[0].value).toFixed(2);
				total();
			}
			input.onblur = function(){this.classList.remove('editing')}
			td9.appendChild(input);
			td9.oncontextmenu = function(){this.lastChild.focus();this.lastChild.value='0.00';this.lastChild.onchange();return false}

			td10 = tr.insertCell();
			td10.style.textAlign = "left";
			input = document.createElement('input');
			input.id = 'total';
			input.type = 'number';
			input.setAttribute('readonly','readonly');
			input.classList.add('unstyled');
			input.value = (td8.getElementsByTagName('INPUT')[0].value * td9.getElementsByTagName('INPUT')[0].value).toFixed(2);
			input.style = "text-align:right;width:74px;height:13px;border:0px;outline:0px;font:normal 11px Verdana;background-color:transparent";
			td10.appendChild(input);
			tr.appendChild(td10);

			td11 = tr.insertCell();
			td11.style.textAlign = "center";
			td11.innerHTML = '<img src="/lib/fontawesome/trash.svg" style="height:14px;margin-left:4px;margin-right:4px">';
			td11.style.cursor = 'pointer';
			td11.onclick = function(){if(confirm('Etes vous sûr ?')) diligence_delete(this.parentNode.id);}
		}


		function total()
		{
			var honoraires=0;
			var frais=0;
			var debours=0;
			var tva = document.getElementById('tva_rates').options[document.getElementById('tva_rates').selectedIndex].text;
			for(x=0;x<(document.getElementById('diligences').tBodies[0].rows.length);x++)
			{
				if (document.getElementById('diligences').tBodies[0].rows[x].cells[6].lastChild.value==1)
					honoraires += parseFloat(document.getElementById('diligences').tBodies[0].rows[x].cells[10].firstChild.value);
				if (document.getElementById('diligences').tBodies[0].rows[x].cells[6].lastChild.value==2)
					frais += parseFloat(document.getElementById('diligences').tBodies[0].rows[x].cells[10].firstChild.value);
				if (document.getElementById('diligences').tBodies[0].rows[x].cells[6].lastChild.value==3)
					debours += parseFloat(document.getElementById('diligences').tBodies[0].rows[x].cells[10].firstChild.value);
			}
			document.getElementById('total_honoraires').innerHTML = number_format(honoraires,2,'.',' ');
			document.getElementById('total_frais').innerHTML = number_format(frais,2,'.',' ');
			document.getElementById('total_frais').innerHTML = number_format(frais,2,'.',' ');
			document.getElementById('total_ht').innerHTML = number_format(honoraires+frais,2,'.',' ');
			document.getElementById('total_debours').innerHTML = number_format(debours,2,'.',' ');
			document.getElementById('total_debours').innerHTML = number_format(debours,2,'.',' ');
			document.getElementById('total_tva').innerHTML = number_format((honoraires+frais)*tva/100,2,'.',' ');
			total_ttc = (honoraires+frais)*(1+tva/100)+debours;
			document.getElementById('total_ttc').innerHTML = number_format(total_ttc,2,'.',' ');

			api_call(query.get('server'),'optimus/'+query.get('db')+'/dossiers/'+query.get('dossier')+'/interventions/'+query.get('id'),'PATCH',{'honoraires':honoraires.toFixed(2),'frais':frais.toFixed(2),'debours':debours.toFixed(2)})
		}

		function facture_create()
		{
			client = 0;
			intervenants = api_call_sync(query.get('server'),'optimus/' + query.get('db') + '/dossiers/' +  query.get('dossier') + '/intervenants','GET',{});
			for (intervenant of intervenants.data)
				if (intervenant.qualite == 10)
					client = intervenant.contact;
				
			total = parseFloat(document.getElementById('total_ttc').innerHTML.replace(/\s/g,'')).toFixed(2)
			if (total == 0)
			{
				alert("Il n'est pas possible de créer une facture dont le montant total est de zéro");
				return false;
			}
			
			api_call(document.getElementById('structures').value.split('`')[0],'optimus/'+document.getElementById('structures').value.split('`')[1]+'/factures','POST',
			{
				'server' : query.get('server'),
				'db' : query.get('db'),
				'client' : client,
				'template' : 0,
				'dossier' : query.get('dossier'),
				'intervention' : query.get('id'),
				'total' : total
			},'facture_created');
		}
		
		function facture_created(response)
		{
			if (response.code == 201)
				api_call(query.get('server'),'optimus/' + query.get('db') + '/dossiers/' + query.get('dossier') + '/interventions/' + query.get('id'),'PATCH',{'facture' : response.data.id,},'facture_open');
		}
		
		function facture_open(response)
		{
			window.open('/modules/factures/editor.php?server=' + document.getElementById('structures').value.split('`')[0] + '&db=' + document.getElementById('structures').value.split('`')[1] + '&id=' + response.data.facture,'editor')
		}

		function lock()
		{
			document.getElementById('diligences').tHead.rows[1].cells[11].style.display='none';
			for (row of document.getElementById('diligences').tBodies[0].rows)
			{
				for (cell of row.cells)
					cell.lastChild.disabled = true;
				row.cells[11].style.display='none';
			}
			document.getElementById('diligences').tFoot.style.display = 'none';
			window.removeEventListener('paste',system_paste);
			window.removeEventListener('cut',system_cut);
			document.getElementById('diligence_create_button').style.display = 'none';
			document.getElementById('facture_create_button').style.display = 'none';
			document.getElementById('facture_button').style.display = '';
		}
		
		function unlock()
		{
			document.getElementById('diligences').tHead.rows[1].cells[11].style.display='';
			for (row of document.getElementById('diligences').tBodies[0].rows)
			{
				for (cell of row.cells)
					cell.lastChild.disabled = false;
				row.cells[11].style.display='';
			}
			document.getElementById('diligences').tFoot.style.display = '';
			window.addEventListener('paste',system_paste);
			window.addEventListener('cut',system_cut);
			document.getElementById('diligence_create_button').style.display = '';
			document.getElementById('facture_create_button').style.display = '';
			document.getElementById('facture_button').style.display = 'none';
		}
		
		function selection_copy()
		{
			i = 0;
			for (row of document.getElementById('diligences').tBodies[0].rows)
				if (row.cells[0].lastChild.checked == true)
					i++;

			if (i == 0)
			{
				api_call_sync(query.get('server'),'optimus/'+get_cookie('user')+'/settings','DELETE',{'user':query.get('db'),'module':'clipboard'});
				return false;
			}
			
			var clipboard = new Object();
			clipboard['server'] = query.get('server');
			clipboard['db'] = query.get('db');
			clipboard['module'] = 'diligences';
			clipboard['dossier'] = query.get('dossier');
			clipboard['intervention'] = query.get('id');
			clipboard['data'] = new Array();
			for (row of document.getElementById('diligences').tBodies[0].rows)
				if (row.cells[0].lastChild.checked == true)
					clipboard['data'][clipboard['data'].length] = 
					{
						"id" : row.id,
						"date" : row.cells[1].lastChild.value,
						"intervenant" : row.cells[2].lastChild.value,
						"beneficiaire" : row.cells[3].lastChild.value,
						"commission" : row.cells[4].lastChild.value,
						"description" : row.cells[5].lastChild.value,
						"categorie" : row.cells[6].lastChild.value,
						"type" : row.cells[7].lastChild.value,
						"coefficient" : row.cells[8].lastChild.value,
						"tarif" : row.cells[9].lastChild.value
					}
			api_call_sync(query.get('server'),'optimus/'+get_cookie('user')+'/settings','POST',{'user':query.get('db'),'module':'clipboard','settings':{'data':clipboard}});
		}
		
		function selection_paste(event)
		{
			clipboard = api_call_sync(query.get('server'),'optimus/'+get_cookie('user')+'/settings','GET',{'user':query.get('db'),'module':'clipboard'});
			try {clipboard = JSON.parse(clipboard.data.data)}catch (e){return false}
			if (!event.code)
				event.preventDefault();
			
			if (clipboard.module != 'diligences')
			{
				alert('Les données contenues dans le presse papier ne sont pas compatibles pour être collées ici');
				return false;
			}
			for (diligence of clipboard.data)
				diligence_created(diligence);
			total();
		}
		
		function selection_cut(event)
		{
			selection_copy();
			selection_delete();
		}
		
		function selection_delete(event)
		{
			if(confirm('Etes vous sûr de vouloir supprimer toutes les lignes sélectionnées ?'))
				for (row of document.getElementById('diligences').tBodies[0].rows)
					if (row.cells[0].lastChild.checked == true)
						diligence_delete(row.id);
		}
		
		function print()
		{
			var xhr = new XMLHttpRequest();
			xhr.open('GET', 'https://api.'+query.get('server')+'/optimus/'+query.get('db')+'/dossiers/'+query.get('dossier')+'/interventions/'+query.get('id')+'?data={"format":"pdf"}', true);
			xhr.withCredentials = true;
			xhr.onload = function(e) 
			{
				if (this.status == 200) 
				{
					var element = document.createElement('a');
					element.setAttribute('href', 'data:application/pdf;base64,' + this.response);
					element.setAttribute('download', 'FICHE ' + query.get('id') + '.pdf');
					element.style.display = 'none';
					document.body.appendChild(element);
					element.click();
					document.body.removeChild(element);
				}
			};
			xhr.send();
		}
		
		
		function help()
		{
			curtain_open();
			alertbox = alert_open();
			helptext = '<div>';
			helptext += '<table style="text-align:left;border-spacing:12px">';
			helptext += '<tr><td colspan="2"><b><u>RACCOURCIS CLAVIER :</u></b><br/><br/></td></tr>';
			helptext += '<tr><td>CTRL + &nbsp;&uarr;</td><td>Se déplacer dans la case du dessus</td></tr>';
			helptext += '<tr><td>CTRL + &rarr;</td><td>Se déplacer dans la case de droite</td></tr>';
			helptext += '<tr><td>CTRL + &nbsp;&darr;</td><td>Se déplacer dans la case du bas</td></tr>';
			helptext += '<tr><td>CTRL + &larr;</td><td>Se déplacer dans la case de gauche</td></tr>';
			helptext += '<tr><td><br/></td></tr>';
			helptext += '<tr><td>CTRL + X</td><td>Couper les lignes sélectionnées</td></tr>';
			helptext += '<tr><td>CTRL + C</td><td>Copier les lignes sélectionnées</td></tr>';
			helptext += '<tr><td>CTRL + V</td><td>Coller les lignes précédemment coupées ou copiées</td></tr>';
			helptext += '<tr><td><br/></td></tr>';
			helptext += '<tr><td>CTRL + ENTREE</td><td>Créer une nouvelle ligne</td></tr>';
			helptext += '<tr><td>CTRL + SUPPR</td><td>Supprimer la ligne active</td></tr>';
			helptext += '</table>';
			helptext += '</div>';
			alertbox.innerHTML = helptext;
			alertbox.onclick = function(){alert_close();curtain_close()}
		}
		
		window.addEventListener('copy',function(){selection_copy(event)});
		window.addEventListener('paste',function(){selection_paste(event)});
		window.addEventListener('cut',function(){selection_cut(event)});
		window.addEventListener('keydown',function(event)
		{
			if (event.ctrlKey)
			{
				if (event.keyCode==37 && document.activeElement.parentNode.previousSibling)
					document.activeElement.parentNode.previousSibling.lastChild.focus();
				else if (event.keyCode==38 && document.activeElement.parentNode.parentNode.previousSibling.lastChild)
					document.activeElement.parentNode.parentNode.previousSibling.cells[document.activeElement.parentNode.cellIndex].lastChild.focus();
				else if (event.keyCode==39 && document.activeElement.parentNode.nextSibling)
					document.activeElement.parentNode.nextSibling.lastChild.focus();
				else if (event.keyCode==40 && document.activeElement.parentNode.parentNode.nextSibling)
					document.activeElement.parentNode.parentNode.nextSibling.cells[document.activeElement.parentNode.cellIndex].lastChild.focus();
				else if (event.keyCode==13)
					diligence_create();
				else if (event.keyCode==46)
				{
					event.preventDefault();
					row_to_delete = document.activeElement.parentNode.parentNode.id;
					if(document.activeElement.parentNode.parentNode.previousSibling.cells)
						document.activeElement.parentNode.parentNode.previousSibling.cells[document.activeElement.parentNode.cellIndex].lastChild.focus();
					else if(document.activeElement.parentNode.parentNode.nextSibling.cells)
						document.activeElement.parentNode.parentNode.nextSibling.cells[document.activeElement.parentNode.cellIndex].lastChild.focus();
					diligence_delete(row_to_delete);
				}
			}
		})
		
		window.onload = function()
		{
			timer = setInterval(function(){document.getElementById('timer').stepUp()},1000)
			timer_save = setInterval(function(){api_call(query.get('server'),'optimus/' + query.get('db') + '/dossiers/' + query.get('dossier') + '/interventions/' + query.get('id'),'PATCH',{'timer':document.getElementById('timer').value});},10000)
		}
		
	</script>
</html>