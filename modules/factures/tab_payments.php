<!DOCTYPE html>
<html lang="fr">
	<head>
		<link rel="stylesheet" href="/css/main.css">
		<link rel="stylesheet" href="/css/statictable.css">
		<script src="/js/api_call.js"></script>
		<script src="/js/number_format.js"></script>
	</head>
	
	<body align="center">
		
		<table id="paiements" class="statictable" align="center" cellpadding="5" cellspacing="0" border="1" bordercolor="000000" style="border-collapse:collapse;margin-top:40px">
			<thead>
				<tr height="24" style="background-color:#5E79B0;color:#FFFFFF">
					<td align="center" width="80">DATE</td>
					<td align="center" width="80">MONTANT</td>
					<td align="center" width="500">DESCRIPTION</td>
				</tr>
			</thead>
			<tbody id="recettes">
			</tbody>
		</table>
		
		<table align="center" style="padding:5px;margin-top:20px;line-height:20px;border:1px solid #000000">
			<tr>
				<td align="right">
					Le client a payé :<br/>
					Sur un total de :<br/>
					Solde à payer :<br/>
				</td>
				<td align="right" style="font-weight:bold;color:#000000">
					<input id="paye" type="text" class="unstyled" style="background:transparent;border:0;text-align:right" readonly"/><br>
					<input id="total" type="text" class="unstyled" style="background:transparent;border:0;text-align:right" readonly"/><br>
					<input id="reste" type="text" class="unstyled" style="background:transparent;border:0;text-align:right" readonly"/><br>
			</tr>
		</table>
		
		<br/>
		
		<progress id="progress" min="0" max="100" value="70" style="height:36px;width:400px"></progress><br/>
		<input id="percent" style="position:relative;top:-26px;height:12px;width:auto;background:transparent;border:0;text-align:center;font-weight:bold">
	
	</body>
	
	<script>
	
	const query = new URLSearchParams(window.location.search);
	
	api_call(query.get('server'),'optimus/'+query.get('db')+'/factures/'+query.get('id'),'GET',{},'init');
	function init(response)
	{
		recettes = api_call_sync(query.get('server'),'optimus/'+query.get('db')+'/recettes','GET', {"filters":[{"facture":query.get('id')}]});
		for (recette of recettes.data)
		{
			paid = 0;
			operation = api_call_sync(query.get('server'),'optimus/'+query.get('db')+'/operations/'+recette.operation,'GET', {});
			tr = document.getElementById('recettes').insertRow();
			tr.style = "background-color:#FFFFFF;font: normal 12px Verdana";
			
			td0 = tr.insertCell();
			td0.style.textAlign = "center";
			td0.innerHTML = new Date(recette.date).toLocaleDateString('fr-FR');
			
			td1 = tr.insertCell();
			td1.style.textAlign = "right";
			td1.innerHTML = recette.montant;
			
			td2 = tr.insertCell();
			td2.style.textAlign = "left";
			td2.innerHTML = operation.data[0].description;
			
			paid += recette.montant;
		}
		document.getElementById('paye').value = number_format(paid,2,'.',' ') + ' €';
		document.getElementById('total').value = number_format(response.data[0].total,2,'.',' ') + ' €';
		document.getElementById('reste').value = number_format(response.data[0].total - paid,2,'.',' ') + ' €';
		document.getElementById('progress').value = Math.round(paid / response.data[0].total * 100);
		document.getElementById('percent').value = Math.round(paid / response.data[0].total * 100) + ' %';
	}
	
	</script>
</html>