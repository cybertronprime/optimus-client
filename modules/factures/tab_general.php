<!DOCTYPE html>
<html lang="fr">
	<head>
		<link rel="stylesheet" href="/css/main.css">
		<link rel="stylesheet" href="/css/editor.css">
		<script src="/js/main.js"></script>
		<script src="/js/api_call.js"></script>
		<script src="/js/editor.js"></script>
		<script src="/js/davclient.js"></script>
		<script src="/js/number_format.js"></script>
	</head>
	
	<body style="text-align:center">
		
		<div class="flex_container" style="max-width:926px;margin:auto">
			
			<div class="editor" style="min-width:410px">
				<div>FACTURE</div>
				<div id="fields">
					Numéro : 			<input type="text" tabindex="01" id="numero" minlength="8" maxlength="8" style="width:278px" readonly disabled/><br/>
					Avocat : 			<select tabindex="02" id="db" style="width:288px" readonly disabled></select><br/>
					Client : 			<select tabindex="03" id="client" onchange="simple_save(this)" onblur="this.classList.remove('editing')" style="width:261px"></select>&nbsp;<img style="vertical-align:middle;cursor:pointer;width:24px" src="/modules/contacts/icon.svg" onclick="edit_contact(document.getElementById('client'))"><br/>
					Dossier : 			<select tabindex="04" id="dossier" onchange="simple_save(this);change_dossier()" onblur="this.classList.remove('editing')" style="width:261px"></select>&nbsp;<img style="vertical-align:middle;cursor:pointer;width:24px" src="/modules/dossiers/icon.svg" onclick="edit_dossier()"><br/>
					Intervention :  	<select tabindex="05" id="intervention" onchange="change_intervention(true)" onblur="this.classList.remove('editing')" style="width:261px"></select>&nbsp;<img style="vertical-align:middle;cursor:pointer;width:24px;margin-bottom:4px" src="/modules/interventions/icon.svg" onclick="edit_intervention()"><br/>
					Date : 				<input type="date" 	tabindex="06" id="date" onblur="simple_save(this)" style="width:278px" ><br/>
					TVA : 				<select tabindex="07" id="tva" onchange="change_tva()" onblur="this.classList.remove('editing')" style="width:288px" ></select><br/>
					Modèle : 			<select tabindex="08" id="template" onchange="simple_save(this)" onblur="this.classList.remove('editing')" style="width:288px"></select><br/>
					Langue : 			<select tabindex="09" id="language" onchange="simple_save(this)" onblur="this.classList.remove('editing')" style="width:288px"></select><br/>
					<br>
					Relance 1 : 		<input type="date" tabindex="11" id="reminder1" onblur="simple_save(this)" style="width:278px" /><br/>
					Relance 2 : 		<input type="date" tabindex="12" id="reminder2" onblur="simple_save(this)" style="width:278px" /><br/>
					Relance 3 : 		<input type="date" tabindex="13" id="reminder3" onblur="simple_save(this)" style="width:278px" /><br/>
					Relance 4 : 		<input type="date" tabindex="14" id="reminder4" onblur="simple_save(this)" style="width:278px" /><br/>
					Relance 5 : 		<input type="date" tabindex="15" id="reminder5" onblur="simple_save(this)" style="width:278px;-webkit-input-placeholder:{color:#FF00FF}" /><br/>
					Provision : 		<input type="checkbox" tabindex="16" id="provision" onblur="simple_save(this)" style="margin-right:272px;vertical-align:middle" /><br/>
					Irrecouvrable :		<input type="checkbox" tabindex="17" id="irrecouvrable" onblur="simple_save(this)" style="margin-right:272px;vertical-align:middle" /><br/>
					<button id="generate_facture_pdf_button" style="width:130px;margin-right:125px" onclick="generate_facture_pdf()" />FACTURE PDF</button><br/>
					<button id="generate_facture_detaille_pdf_button" style="width:200px;margin-right:90px" onclick="generate_facture_pdf(1)" />FACTURE PDF DETAILLE</button><br/>
				</div>
			</div>
			
			<div>
				<div class="editor" style="min-width:410px">
					<div>DETAIL</div>
					<div>
						Honoraires : 	<input readonly id="honoraires" style="width:130px;text-align:right" /><br/>
						Frais : 		<input readonly id="frais" style="width:130px;text-align:right" /><br/>
						<hr/>
						TOTAL HT : 		<input readonly id="total_ht" style="width:130px;text-align:right" /><br/>
						TVA : 			<input readonly id="total_tva" style="width:130px;text-align:right" /><br/>
						<hr/>
						Débours : 		<input readonly id="debours" style="width:130px;text-align:right" /><br/>
						<hr/>
						TOTAL TTC : 	<input readonly id="total" style="width:130px;text-align:right" /><br/>
					</div>
				</div>
				
				<div class="editor" style="min-width:410px">
					<div>NOTES</div>
					<div style="padding-right:30px;padding-bottom:5px">
						<textarea id="notes" onblur="simple_save(this)" style="width:100%;height:238px;margin-right:50px"></textarea>
					</div>
				</div>
			</div>
			
			<div class="editor"  style="min-width:410px">
					<div style="display:none"></div>
					<div style="text-align:center">
						<button onclick="api_call(query.get('server'),'optimus/'+query.get('db')+'/factures/'+query.get('id'),'DELETE', {})"><img src="/lib/fontawesome/trash.svg" style="height:16px;opacity:0.6;vertical-align:-4px"/>&nbsp;&nbsp;<span style="color:#FF0000">Supprimer</span></button>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<button onclick="parent.window.open('about:blank', '_self');parent.window.close()"><img src="/lib/fontawesome/times.svg" style="width:14px;filter:contrast(60%);vertical-align:-6px">&nbsp;&nbsp;Quitter</button>
					</div>
				</div>

		</div>

		<script type="text/javascript">
	
			const query = new URLSearchParams(window.location.search);
			module = 'factures';
			itm = {};
			authorizations = {};
			tva_rates = {};
			
			api_call(query.get('server'),'optimus/'+query.get('db')+'/factures/'+query.get('id'),'GET', {},'init');
			function init(response)
			{
				itm = response.data[0];
				authorizations = response.authorizations;
				
				get_tva_rates();
				get_contacts();
				get_dossiers();
				get_interventions();
				get_templates();
				get_db();
				populate(document.getElementById('language'),'languages', true, false,itm.language)
				editor_init();
				change_intervention();
				
				parent.window.document.title = 'FACTURE N° ' + itm.numero;
				
				recettes = api_call_sync(query.get('server'),'optimus/'+query.get('db')+'/recettes','GET', {"fields":["id"],"filters":[{"facture":itm.facture}]});
				if (recettes.data.length > 0)
					lock();
			}
			
			function get_db()
			{
				members = api_call_sync(itm.server,'optimus/'+query.get('db')+'/members','GET',{});
				if (members.data)
					members.data.sort(function(x,y){var a = String(x.description).toUpperCase(); var b = String(y.description).toUpperCase(); if (a > b) return 1;if (a < b) return -1;return 0;});
				for (member of members.data)
					document.getElementById('db').options[document.getElementById('db').options.length] = new Option(member.member,member.member);
			}
			
			function get_tva_rates()
			{
				if (localStorage.getItem('tva_rates'))
					tva_rates = JSON.parse(localStorage.getItem('tva_rates'));
				else
				{
					tva_rates = api_call_sync('optimus-avocats.fr','constants/','GET',{'db':'tva_rates'});
					if (tva_rates.code ==200)
						localStorage.setItem('tva_rates',JSON.stringify(tva_rates));
				}
				document.getElementById('tva').innerHTML='';
				for (rate of tva_rates.data)
				{
					document.getElementById('tva').options[document.getElementById('tva').options.length] = new Option(rate.description, rate.id);
					document.getElementById('tva').options[document.getElementById('tva').options.length-1].setAttribute('rate',rate.value);
				}
			}
			
			function get_contacts()
			{
				document.getElementById('client').innerHTML='<option value="0"></option>';
				contacts = api_call_sync(itm.server,'optimus/'+itm.db+'/contacts','GET',{"fields":["id","firstname","lastname","company_name"]});
				for (i=0;i<contacts.data.length;i++)
					contacts.data[i].description = (contacts.data[i].lastname+' '+contacts.data[i].firstname+' '+contacts.data[i].company_name).trim(),contacts.data[i].id;
				if (contacts.data)
					contacts.data.sort(function(x,y){var a = String(x.description).toUpperCase(); var b = String(y.description).toUpperCase(); if (a > b) return 1;if (a < b) return -1;return 0;});
				for (contact of contacts.data)
					document.getElementById('client').options[document.getElementById('client').options.length] = new Option(contact.description,contact.id);
			}
	
			function get_dossiers()
			{
				document.getElementById('dossier').innerHTML='<option value="0"></option>';
				dossiers = api_call_sync(itm.server,'optimus/'+itm.db+'/dossiers','GET',{"fields":["id","nom"]});
				if (dossiers.data)
					dossiers.data.sort(function(x,y){var a = String(x.nom).toUpperCase(); var b = String(y.nom).toUpperCase(); if (a > b) return 1;if (a < b) return -1;return 0;});
				for (dossier of dossiers.data)
					document.getElementById('dossier').options[document.getElementById('dossier').options.length] = new Option(dossier.nom,dossier.id);
			}
			
			function get_interventions()
			{
				document.getElementById('intervention').innerHTML='<option value="0"></option>';
				interventions = api_call_sync(itm.server,'optimus/'+itm.db+'/dossiers/'+itm.dossier+'/interventions','GET',{});
				if (interventions.data)
					interventions.data.sort(function(x,y){var a = String(x.date_ouverture); var b = String(y.date_ouverture); if (a > b) return 1;if (a < b) return -1;return 0;});
				for (intervention of interventions.data)
					document.getElementById('intervention').options[document.getElementById('intervention').options.length] = new Option('FICHE N°'+intervention.id+' ('+intervention.date_ouverture.substring(8,10)+'/'+intervention.date_ouverture.substring(5,7)+'/'+intervention.date_ouverture.substring(0,4)+')',intervention.id);
			}
			
			function get_templates()
			{
				client = new dav.Client({baseUrl : 'https://cloud.'+query.get('server')});
				client.propFind('/files/'+query.get('db')+'/==MODELES FACTURES==', ['{DAV:}getcontentlength', '{DAV:}getlastmodified', '{DAV:}resourcetype', '{DAV:}getetag'],'1').then(
				function(result) 
				{
					var re = /(?:\.([^.]+))?$/;
					document.getElementById('template').innerHTML='<option value="0"></option>';
					for (var i=1; i<result['body'].length; i++)
					{
						extension = re.exec(result['body'][i]['href'])[1];
						lastmodified = new Date(result['body'][i]['propStat'][0]['properties']['{DAV:}getlastmodified']).toLocaleDateString('fr-FR');
						etag = result['body'][i]['propStat'][0]['properties']['{DAV:}getetag'];
						filename = decodeURIComponent(result['body'][i]['href']).replace('/files/'+query.get('db')+'/==MODELES FACTURES==','').replace('/','').replace('/','').replace(/\.[^/.]+$/, "");
						extension = re.exec(result['body'][i]['href'])[1];
						if (extension == 'pdf')
							document.getElementById('template').options[document.getElementById('template').options.length] = new Option(filename+'.'+'pdf' + ' ('+lastmodified+')', filename + '.pdf');
						document.getElementById('template').value = itm.template;
					}
				})
			}
			
			function change_dossier()
			{
				api_call_sync(itm.server,'optimus/'+query.get('server')+'/factures/'+query.get('id'),'PATCH',{'dossier':document.getElementById('dossier').value,'intervention':0});
				get_interventions();
				change_intervention(save);
			}
			
			function change_intervention(save)
			{
				if (document.getElementById('intervention').value==0)
				{
					document.getElementById('honoraires').value = '0,00';
					document.getElementById('frais').value = '0,00';
					document.getElementById('debours').value = '0,00';
					document.getElementById('total_ht').value = '0,00';
					document.getElementById('total_tva').value = '0,00';
					document.getElementById('total').value = '0,00';
					api_call_sync(query.get('server'),'optimus/'+query.get('db')+'/factures/'+query.get('id'),'PATCH',{'intervention':0,'total':0});
				}
				else
				{
					intervention = api_call_sync(query.get('server'),'optimus/'+document.getElementById('db').value+'/dossiers/'+document.getElementById('dossier').value+'/interventions/'+document.getElementById('intervention').value,'GET', {});
					document.getElementById('honoraires').value = number_format(intervention.data[0].honoraires,2,'.',' ');
					document.getElementById('frais').value = number_format(intervention.data[0].frais,2,'.',' ');
					document.getElementById('debours').value = number_format(intervention.data[0].debours,2,'.',' ');
					document.getElementById('total_ht').value = number_format(parseFloat(intervention.data[0].honoraires) + parseFloat(intervention.data[0].frais),2,'.',' ');
					document.getElementById('total_tva').value = number_format(parseFloat(document.getElementById('total_ht').value.replace(' ',''))*parseFloat(document.getElementById('tva').options[document.getElementById('tva').selectedIndex].getAttribute('rate'))/100,2,'.',' ');
					document.getElementById('total').value = number_format(parseFloat(document.getElementById('total_ht').value.replace(' ',''))+parseFloat(document.getElementById('total_tva').value.replace(' ',''))+parseFloat(document.getElementById('debours').value.replace(' ','')),2,'.',' ');
				}
				if (save == true)
					api_call_sync(query.get('server'),'optimus/'+query.get('db')+'/factures/'+query.get('id'),'PATCH',{'intervention':document.getElementById('intervention').value, 'total':parseFloat(document.getElementById('total').value.replace(/\s/g,'')).toString()});
			}
			
			function change_tva()
			{
				document.getElementById('total_tva').value = number_format(parseFloat(document.getElementById('total_ht').value.replace(' ',''))*parseFloat(document.getElementById('tva').options[document.getElementById('tva').selectedIndex].getAttribute('rate'))/100,2,'.',' ');
				document.getElementById('total').value = number_format(parseFloat(document.getElementById('total_ht').value.replace(' ',''))+parseFloat(document.getElementById('total_tva').value.replace(' ',''))+parseFloat(document.getElementById('debours').value.replace(' ','')),2,'.',' ');
				api_call_sync(query.get('server'),'optimus/'+query.get('db')+'/factures/'+query.get('id'),'PATCH', {'tva':document.getElementById('tva').value,'total':parseFloat(document.getElementById('total').value.replace(/\s/g,'')).toString()});
			}
		
			function edit_contact(obj)
			{
				if (obj.value==0) 
				{
					if(confirm('Voulez-vous créer un nouveau contact ?')) 
					{
						new_id = db_insert(document.getElementById('db').value,'contacts','id','NULL');
						window.open('/modules/contacts/editor.php?server='+itm.server+'&db='+document.getElementById('db').value+'&id='+new_id,'editor');
					}
				} 
				else 
						window.open('/modules/contacts/editor.php?server='+itm.server+'&db='+document.getElementById('db').value+'&id='+obj.value,'editor');
			}
			
			function edit_dossier()
			{
				if (document.getElementById('dossier').value!=0) 
					window.open('/modules/dossiers/editor.php?server='+itm.server+'&db='+document.getElementById('db').value+'&id='+document.getElementById('dossier').value,'editor');
			}
			
			function edit_intervention()
			{
				if (document.getElementById('intervention').value!=0) 
					window.open('/modules/interventions/editor.php?server='+itm.server+'&db='+document.getElementById('db').value+'&dossier='+document.getElementById('dossier').value+'&id='+document.getElementById('intervention').value,'editor');
			}
			
			function generate_facture_pdf(detail)
			{
				curtain = curtain_open();
				curtain.style.backgroundColor = 'transparent';
				document.documentElement.classList.add('wait');
				var xhr = new XMLHttpRequest();
				if (detail==1)
					xhr.open('GET', 'https://api.'+query.get('server')+'/optimus/'+query.get('db')+'/factures/'+query.get('id')+'?data={"format":"pdf","detail":"1"}', true);
				else
					xhr.open('GET', 'https://api.'+query.get('server')+'/optimus/'+query.get('db')+'/factures/'+query.get('id')+'?data={"format":"pdf"}', true);
				xhr.withCredentials = true;
				xhr.onload = function(e) 
				{
					if (this.status == 200) 
					{
						var element = document.createElement('a');
						element.href = 'data:application/pdf;base64,' + this.response;
						element.download = document.getElementById('numero').value+'.pdf';
						element.click();
						curtain_close();
						document.documentElement.classList.remove('wait');
					}
				};
				xhr.send();
			}
			
			function lock()
			{
				//document.getElementById('client').disabled = true;
				document.getElementById('dossier').disabled = true;
				document.getElementById('intervention').disabled = true;
				document.getElementById('date').disabled = true;
				document.getElementById('tva').disabled = true;
			}
			
			function unlock()
			{
				document.getElementById('numero').disabled = false;
				document.getElementById('db').disabled = false;
				document.getElementById('client').disabled = false;
				document.getElementById('dossier').disabled = false;
				document.getElementById('intervention').disabled = false;
				document.getElementById('date').disabled = false;
				document.getElementById('tva').disabled = false;
			}
			
			var update_link = new BroadcastChannel('update_link');
			update_link.onmessage = function (ev) {window.location.reload()}
			window.onblur = function(){update_link.postMessage('update_link')}
		</script>
	</body>
</html>