<!DOCTYPE html>
<html lang="fr">
	<head>
		<link rel="stylesheet" href="/css/main.css" />
		<link rel="stylesheet" href="/css/statictable.css" />
		<script type="text/javascript" src="/js/main.js"></script>
		<script type="text/javascript" src="/js/api_call.js"></script>
		<script type="text/javascript">
			const query = new URLSearchParams(window.location.search);
			api_call(query.get('server'), 'optimus/'+query.get('db')+'/contacts/'+query.get('id'), 'GET', {}, 'init');
			function init(response)
			{
				if (response.data[0].siret)
					window.open("https://www.infogreffe.fr/infogreffe/ficheIdentite.do?siren=" + response.data[0].siret);
				else
					notification("Merci de renseigner le N° SIRET de l'entreprise pour activer ce module","#FF0000");
			}
		</script>
	</head>
</html>
