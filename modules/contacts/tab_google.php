<!DOCTYPE html>
<html lang="fr">
	<head>
		<link rel="stylesheet" href="/css/main.css" />
		<link rel="stylesheet" href="/css/statictable.css" />
		<script type="text/javascript" src="/js/api_call.js"></script>
		<script type="text/javascript">
			const query = new URLSearchParams(window.location.search);
			api_call(query.get('server'), 'optimus/'+query.get('db')+'/contacts/'+query.get('id'), 'GET', {}, 'init');
			function init(response)
			{
				window.open('https://www.google.fr/?gws_rd=ssl#q=' + response.data[0].firstname + ' ' + response.data[0].lastname + ' ' + response.data[0].company_name + ' ' + response.data[0].city_name);
			}
		</script>
	</head>
</html>