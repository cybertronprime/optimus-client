<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta name="viewport" content="width=410, initial-scale=1">
		<link rel="shortcut icon" type="image/png" href="/modules/contacts/favicon-32x32.png">
		<link rel="stylesheet" href="/css/main.css">
		<link rel="stylesheet" href="/css/tabs.css">
		<script src="/js/main.js"></script>
	</head>

	<body style="text-align:center;overflow-y:hidden" onload="load_tab()">
		<div class="tabs" onclick="if (event.target.name=='tabs') document.getElementById('0').checked = false">
			<input id="0" type="checkbox"/><label for="0"></label>
			<input id="1" type="radio" name="tabs" onclick="load_tab()" link="tab_general.php" checked/><label for="1">GENERAL</label>
			<input id="2" type="radio" name="tabs" onclick="load_tab()" link="tab_notes.php"><label for="2">NOTES</label>
			<input id="3" type="radio" name="tabs" onclick="load_tab()" link="tab_factures.php"><label for="3">FACTURES</label>
			<input id="4" type="radio" name="tabs" onclick="load_tab()" link="tab_juridictions.php"><label for="4">JURIDICTIONS</label>
			<input id="5" type="radio" name="tabs" onclick="load_tab()" link="tab_huissiers.php"><label for="5">HUISSIERS</label>
			<input id="6" type="radio" name="tabs" onclick="load_tab()" link="tab_rcs.php"><label for="6">RCS</label>
			<input id="7" type="radio" name="tabs" onclick="load_tab()" link="tab_bodacc.php"><label for="7">BODACC</label>
			<input id="8" type="radio" name="tabs" onclick="load_tab()" link="tab_infogreffe.php"><label for="8">INFOGREFFE</label>
			<input id="9" type="radio" name="tabs" onclick="load_tab()" link="tab_cfe.php'"><label for="9">CFE</label>
			<input id="10" type="radio" name="tabs" onclick="load_tab()" link="tab_plan.php"><label for="10">PLAN</label>
			<input id="11" type="radio" name="tabs" onclick="load_tab()" link="tab_google.php"><label for="11">GOOGLE</label>
			<input id="12" type="radio" name="tabs" onclick="load_tab()" link="tab_pages_jaunes.php"><label for="12">PAGES JAUNES</label>
		</div>
		<div class="tab_separator"></div>
		<iframe id="frame" marginwidth="0" marginheight="0" frameborder="0" onload="this.style.height=(window.innerHeight - 57) + 'px'" style="width:100%"></iframe>
	</body>
</html>