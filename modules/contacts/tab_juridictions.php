<!DOCTYPE html>
<html lang="fr">
	<head>
		<link rel="stylesheet" href="/css/main.css">
		<script src="/js/main.js"></script>
		<script src="/js/api_call.js"></script>
	</head>
	
	<body>
	
		<div id="container" class="grid_container" style="visibility:hidden">
		
		</div>
			
		<script>
		
		const query = new URLSearchParams(window.location.search);
		itm = {};
		authorizations = {};
		juridictions = {};

		api_call(query.get('server'), 'optimus/'+query.get('db')+'/contacts/'+query.get('id'), 'GET', {}, 'init');
		function init(response)
		{
			itm = response.data[0];
			authorizations = response.authorizations;
			display_juridictions();
			document.getElementById('container').style.visibility = 'visible';
		}
		
		function display_juridictions()
		{
			juridictions = api_call_sync('optimus-avocats.fr', 'juridictions/', 'GET', {"commune_insee":itm.city});
			
			for (i=0; i < juridictions.data.length; i++)
			{
				module = document.createElement('div');
				module.classList.add('module');
				module.style.width = '100%';
				document.getElementById('container').appendChild(module);
				
				div1 = document.createElement('div');
				div1.innerHTML = juridictions.data[i].nom.toUpperCase();
				module.appendChild(div1);
				
				div2 = document.createElement('div');
				div2.style.textAlign = 'center';
				div2.innerHTML += juridictions.data[i].addresse1 + '<br/>';
				div2.innerHTML += (juridictions.data[i].addresse2 ?  juridictions.data[i].addresse2 + '<br/>' : '');
				div2.innerHTML += juridictions.data[i].code_postal + ' ' + juridictions.data[i].commune + '<br/>';
				div2.innerHTML += (juridictions.data[i].addresse2 ? '' : '<br/>');
				div2.innerHTML += '<br/>';
				div2.innerHTML += (juridictions.data[i].telephone ? "Tél : " + juridictions.data[i].telephone + '<br/>' : '');
				div2.innerHTML += (juridictions.data[i].fax ? "Fax : " + juridictions.data[i].fax + '<br/>' : '<br/>');
				div2.innerHTML += '<img src="/lib/fontawesome/download.svg" style="width:16px;float:right;cursor:pointer" onclick="import_juridictions(' + i + ')"/>';
				div2.innerHTML += juridictions.data[i].courriel + '<br/>';
				module.appendChild(div2);
			}
			
		}
		
		function import_juridictions(id)
		{
			exists = api_call_sync(query.get('server'),'optimus/'+query.get('db')+'/contacts','GET',{"filters":[{"company_name":juridictions.data[id].nom}]});
			if(exists.data.length > 0)
			{
				notification('Cette juridiction existe déjà dans la base contact !','red');
				return false;
			}
			
			ville = api_call_sync('optimus-avocats.fr','commune/','GET',{"commune_insee":juridictions.data[id].commune_insee});
			api_call(query.get('server'), 'optimus/'+query.get('db')+'/contacts', 'POST', 
			{
				"categorie":50,
				"type":30,
				"lastname":null,
				"company_name":juridictions.data[id].nom,
				"address":juridictions.data[id].addresse1 + '\n' + (juridictions.data[id].addresse2 ? juridictions.data[id].addresse2 + '\n' : '') + juridictions.data[id].code_postal + ' ' + juridictions.data[id].commune,
				"phone":juridictions.data[id].telephone,
				"fax":juridictions.data[id].fax,
				"email":juridictions.data[id].courriel,
				"zipcode":ville.data[0].code_postal,
				"city_name":ville.data[0].nom,
				"city":juridictions.data[id].commune_insee
			}
			, 'juridiction_imported');
		}

		
		function juridiction_imported()
		{
			notification('La juridiction a été importée dans la base contact','green');
		}

		var update_link = new BroadcastChannel('update_link');
		update_link.onmessage = function (ev) {window.location.reload()}
		window.onblur = function(){update_link.postMessage('update_link')}
		</script>
	</body>	
</html>