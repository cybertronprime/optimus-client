<!DOCTYPE html>
<html lang="fr">
	<head>
		<link rel="stylesheet" href="/css/main.css">
		<link rel="stylesheet" href="/css/editor.css">
		<script src="/js/main.js"></script>
		<script src="/js/api_call.js"></script>
		<script src="/js/editor.js"></script>
	</head>

	<body style="text-align:center">

		<div id="container" class="flex_container" style="visibility:hidden;max-width:926px;margin:auto">

			<div>
			
				<div class="editor" style="min-width:410px">
					<div>CATEGORIE</div>
					<div>
						Catégorie : <select id="categorie" tabindex="1" onchange="simple_save(this)" onblur="this.classList.remove('editing')" style="width:306px"></select><br/>
						Type : <select id="type" tabindex="2" onchange="simple_save(this);change_type(this.value)" onblur="this.classList.remove('editing')" style="width:306px"></select><br/>
					</div>
				</div>

				<div id="etatcivil" class="editor" style="min-width:410px" >
					<div>ETAT CIVIL</div>
					<div>
						<span id="personne_physique">
						Titre : <select id="title" tabindex="3" onchange="simple_save(this)" onblur="this.classList.remove('editing')" style="width:310px"></select><br/>
						Prénom : <input id="firstname" type="text" tabindex="4" onblur="simple_save(this)" style="width:300px"/><br/>
						Nom : <input id="lastname" type="text" tabindex="5" onblur="simple_save(this)" style="width:300px"/><br/>
						<br/>
						Né(é) le : <input id="birth_date" tabindex="6" type="date" onblur="simple_save(this)" style="width:300px"/><br/>
						CP : <input id="birth_zipcode" type="text" tabindex="7" onblur="simple_save(this)" onchange="get_cities(document.getElementById('birth_city'),this.value)" style="width:300px"/><br/>
						Ville : <select id="birth_city" tabindex="8" onchange="simple_save(this);document.getElementById('birth_city_name').value = this.options[this.selectedIndex].text;document.getElementById('birth_city_name').onblur()" onblur="this.classList.remove('editing')" style="width:310px"></select>
						<input id="birth_city_name"  tabindex="9" type="text" style="width:300px" onblur="simple_save(this)"/><br/>
						Pays : <select id="birth_country" tabindex="10" onblur="simple_save(this)" onchange="change_country()" style="width:310px"></option></select><br/>
						<br/> 
						Situation  : <select id="marital_status" tabindex="11" onblur="simple_save(this)" style="width:310px"></option></select><br/>
						<br/>
						N° Insee  : <input id="insee" type="text" tabindex="12" onblur="simple_save(this)" onchange="get_birth_cities(this.value)" style="width:300px"/>
						</span>
						<br/>
					</div>
				</div>
				
				<div class="editor" style="min-width:410px">
					<div>DONNEES FINANCIERES</div>
					<div>
						IBAN : <input id="iban" type="text" tabindex="13" onblur="simple_save(this)" style="width:276px" /><br/>
						BIC : <input id="bic" type="text" tabindex="14" onblur="simple_save(this)" style="width:276px" /><br/>
						TVA Intracom : <input id="tva" type="text" tabindex="15" onblur="simple_save(this)" style="width:276px" /><br/>
					</div>
				</div>
			
			</div>
			

			<div>
				
				<div class="editor" style="min-width:410px">
					<div>RCS</div>
					<div>
						N° RCS : <input id="siret" tabindex="16" type="text" maxlength="9" onblur="simple_save(this)" style="width:159px"/>&nbsp;
						<button onclick="get_rcsinfos()" style="cursor:pointer"><img src="/lib/fontawesome/landmark.svg" style="width:14px;filter:contrast(60%);vertical-align:-4px"/>&nbsp;&nbsp;Import RCS</button><br/>

						Dénomination : <input id="company_name" type="text" tabindex="17" onblur="simple_save(this)" style="width:274px"/><br/>
						Type : <select id="company_type" tabindex="18" onchange="simple_save(this)" onblur="this.classList.remove('editing')" style="width:284px"></select><br/>
						Capital : <input id="company_capital" type="number" tabindex="19" onblur="simple_save(this)" style="width:274px"/><br/>
						Ressort RCS : <select id="company_greffe" tabindex="20" onchange="simple_save(this)" onblur="this.classList.remove('editing')" style="width:284px"></select><br/>
					</div>
				</div>
				
				<div class="editor" style="min-width:410px">
					<div>COORDONNEES</div>
					<div>
						<span style="position:relative;top:-32px">Adresse : </span><textarea id="address" tabindex="21" onblur="simple_save(this)" style="width:298px;height:60px"/></textarea><br/>
						CP : <input id="zipcode" type="text" tabindex="22" onblur="simple_save(this)" onchange="get_cities(document.getElementById('city'),this.value)" style="width:300px"/><br/>
						Ville : <select id="city" tabindex="23" onchange="simple_save(this);document.getElementById('city_name').value = this.options[this.selectedIndex].text;document.getElementById('city_name').onblur()" onblur="this.classList.remove('editing')" style="width:310px"></select><input id="city_name" type="text" style="width:300px" onblur="simple_save(this)"/><br/>
						Pays : <select id="country" tabindex="24" onblur="simple_save(this)" onchange="change_country()" style="width:310px"></option></select><br/>
						<br/>
						<img src="/lib/fontawesome/phone.svg" style="width:16px;filter:contrast(60%);vertical-align:-4px;margin-right:8px"><input id="phone" type="text" tabindex="25" onblur="simple_save(this)" style="width:154px"/>
						<img src="/lib/fontawesome/phone.svg" style="width:16px;filter:contrast(60%);vertical-align:-4px;margin-right:8px"><input id="pro_phone" type="text" tabindex="26" onblur="simple_save(this)" style="width:154px"/><br/>
						<img src="/lib/fontawesome/fax.svg" style="width:16px;filter:contrast(60%);vertical-align:-4px;margin-right:8px"><input id="fax" type="text" tabindex="27" onblur="simple_save(this)" style="width:154px"/>
						<img src="/lib/fontawesome/fax.svg" style="width:16px;filter:contrast(60%);vertical-align:-4px;margin-right:8px"><input id="pro_fax" type="text" tabindex="28" onblur="simple_save(this)" style="width:154px"/><br/>
						<img src="/lib/fontawesome/mobile.svg" style="width:10px;filter:contrast(60%);vertical-align:-4px;margin-right:11px"><input id="mobile" type="text" tabindex="29" onblur="simple_save(this)" style="width:154px;margin-right:4px"/>
						<img src="/lib/fontawesome/mobile.svg" style="width:10px;filter:contrast(60%);vertical-align:-4px;margin-right:10px"><input id="pro_mobile" type="text" tabindex="30" onblur="simple_save(this)" style="width:154px"/><br/>
						<img src="/lib/fontawesome/email.svg" style="width:16px;filter:contrast(60%);vertical-align:-4px;margin-right:8px;cursor:pointer" onclick="window.open('https://webmail.'+query.get('server')+'/?_task=mail&_action=compose&_to='+document.getElementById('email').value)"><input id="email" type="text" tabindex="31" onblur="simple_save(this)" style="width:154px"/>
						<img src="/lib/fontawesome/email.svg" style="width:16px;filter:contrast(60%);vertical-align:-4px;margin-right:8px;cursor:pointer" onclick="window.open('https://webmail.'+query.get('server')+'/?_task=mail&_action=compose&_to='+document.getElementById('pro_email').value)"><input id="pro_email" type="text" tabindex="32" onblur="simple_save(this)" style="width:154px"/><br/>
						<img src="/lib/fontawesome/website.svg" style="width:16px;filter:contrast(60%);vertical-align:-4px;margin-right:8px;cursor:pointer" onclick="window.open('https://'+document.getElementById('website').value)"><input id="website" type="text" tabindex="33" onblur="simple_save(this)" style="width:154px"/>
						<img src="/lib/fontawesome/website.svg" style="width:16px;filter:contrast(60%);vertical-align:-4px;margin-right:8px;cursor:pointer" onclick="window.open('https://'+document.getElementById('website').value)"><input id="pro_website" type="text" tabindex="34" onblur="simple_save(this)" style="width:154px"/>
					</div>
				</div>

				

				<div class="editor"  style="min-width:410px">
					<div style="display:none"></div>
					<div style="text-align:center">
						<button onclick="contact_delete()"><img src="/lib/fontawesome/trash.svg" style="width:14px;filter:contrast(60%);vertical-align:-4px">&nbsp;&nbsp;<span style="color:#FF0000">Supprimer</span></button>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<button onclick="parent.window.open('about:blank', '_self');parent.window.close()"><img src="/lib/fontawesome/times.svg" style="width:14px;filter:contrast(60%);vertical-align:-6px">&nbsp;&nbsp;Quitter</button>
					</div>
				</div>

			</div>
		</div>

		<script type="text/javascript">
			
			const query = new URLSearchParams(window.location.search);
			module = 'contacts';
			itm = {};
			authorizations = {};
			
			api_call(query.get('server'),'optimus/'+query.get('db')+'/contacts/'+query.get('id'),'GET', {},'init');
			function init(response)
			{
				itm = response.data[0];
				authorizations = response.authorizations;
				populate(document.getElementById('title'), 'contacts_titles', true, true);
				populate(document.getElementById('categorie'), 'contacts_categories', true, true);
				populate(document.getElementById('type'), 'contacts_types', true, false);
				change_type(itm.type);
				populate(document.getElementById('country'), 'pays', true, false);
				populate(document.getElementById('birth_country'), 'pays', true, false);
				populate(document.getElementById('marital_status'), 'contacts_marital_statuses', true, true);
				populate(document.getElementById('company_type'), 'company_types_lvl3', true, true);
				populate(document.getElementById('company_greffe'), 'greffes', true, true);

				get_cities(document.getElementById('city'), itm.zipcode);
				get_cities(document.getElementById('birth_city'), itm.birth_zipcode);
		
				editor_init();
				change_country();
				
				parent.window.document.title = itm.company_name + ' ' + itm.firstname + ' ' + itm.lastname.toUpperCase();
				document.getElementById('container').style.visibility = 'visible';
			}
			
			function get_rcsinfos()
			{
				if (document.getElementById('siret').value.length!=9)
				{
					alert(('Le numéro SIREN doit comporter 9 chiffres'));
					return false;
				}
				if (confirm("Voulez-vous tenter d'importer automatiquement les données RCS ?")==true)
				{
					sirene = new XMLHttpRequest();
					sirene.open('GET', 'https://entreprise.data.gouv.fr/api/sirene/v3/unites_legales/'+itm.siret, false);
					sirene.send();
					sirene = JSON.parse(sirene.responseText);
					
					document.getElementById('title').value = 0;
					if (sirene.unite_legale.categorie_juridique == 1000)
					{
						if (sirene.unite_legale.sexe == 'M')
							document.getElementById('type').value = 10;
						else
							document.getElementById('type').value = 20;
						document.getElementById('firstname').value = (sirene.unite_legale.prenom_usuel ? sirene.unite_legale.prenom_usuel : sirene.unite_legale.prenom_1);
						document.getElementById('lastname').value = (sirene.unite_legale.nom_usage ? sirene.unite_legale.nom_usage : sirene.unite_legale.nom);
					}
					else
					{
						document.getElementById('type').value = 30;
						document.getElementById('firstname').value = '';
						document.getElementById('lastname').value = '';
					}
					change_type(document.getElementById('type').value);
					if (sirene.unite_legale.denomination)
						document.getElementById('company_name').value = sirene.unite_legale.denomination;
					else
						document.getElementById('company_name').value = sirene.unite_legale.denomination_usuelle_1;
					document.getElementById('company_type').value = sirene.unite_legale.categorie_juridique;
					document.getElementById('address').value  = (sirene.unite_legale.etablissement_siege.complement_adresse || '');
					document.getElementById('address').value += (sirene.unite_legale.etablissement_siege.numero_voie ? "\n" + sirene.unite_legale.etablissement_siege.numero_voie : '') + (sirene.unite_legale.etablissement_siege.indice_repetition || '' ) + " " + sirene.unite_legale.etablissement_siege.type_voie + " " + sirene.unite_legale.etablissement_siege.libelle_voie;
					document.getElementById('address').value += (sirene.unite_legale.etablissement_siege.distribution_speciale ? "\n" + sirene.unite_legale.etablissement_siege.distribution_speciale : '');
					document.getElementById('address').value += (sirene.unite_legale.etablissement_siege.code_cedex ? "\n" + sirene.unite_legale.etablissement_siege.code_cedex + " " + sirene.unite_legale.etablissement_siege.libelle_cedex : '');

					document.getElementById('zipcode').value = sirene.unite_legale.etablissement_siege.code_postal;
					get_cities(document.getElementById('city'), sirene.unite_legale.etablissement_siege.code_postal);
					if (document.getElementById('city').options.length > 1)
					{
						document.getElementById('city').value = sirene.unite_legale.etablissement_siege.code_commune;
						document.getElementById('city_name').value = document.getElementById('city').options[document.getElementById('city').selectedIndex].text;
						document.getElementById('country').value = 74;
					}
					else
					{
						document.getElementById('city').value = sirene.unite_legale.etablissement_siege.code_commune;
						document.getElementById('city_name').value = sirene.unite_legale.etablissement_siege.libelle_commune_etranger;
						document.getElementById('country').value = 0;
					}
					change_country();
					
					rcsinfo = api_call_sync('optimus-avocats.fr','rcsinfo/','POST',{"code_insee":itm.siret});
					rcsinfo = JSON.parse(rcsinfo.data);
					if (rcsinfo.result.hits.hits[0])
					{
						document.getElementById('company_capital').value = rcsinfo.result.hits.hits[0]._source.idt_pm_montant_cap;
						document.getElementById('company_greffe').value = rcsinfo.result.hits.hits[0]._source.code_greffe;
					}
						
					simple_save(document.getElementById('type'));
					simple_save(document.getElementById('title'));
					simple_save(document.getElementById('siret'));
					simple_save(document.getElementById('firstname'));
					simple_save(document.getElementById('lastname'));
					simple_save(document.getElementById('company_name'));
					simple_save(document.getElementById('company_type'));
					simple_save(document.getElementById('company_capital'));
					simple_save(document.getElementById('company_greffe'));
					simple_save(document.getElementById('address'));
					simple_save(document.getElementById('zipcode'));
					simple_save(document.getElementById('city'));
					simple_save(document.getElementById('city_name'));
					simple_save(document.getElementById('country'));
				}
			}

			function get_cities(obj,zipcode)
			{
				if (!zipcode || zipcode.length < 2)
					return false;
				obj.innerHTML='<option value="">LOADING...</option>';
				obj.innerHTML='<option value="0"></option>';
				
				results = api_call_sync('optimus-avocats.fr','communes/','GET',{"code_postal":zipcode});
				
				if (results.data)
					for (i=0;i<results.data.length;i++)
						obj.options[i+1] = new Option(results.data[i].nom, results.data[i].commune_insee);
						
				if(obj.options.length > 1)
				{
					obj.selectedIndex = 0;
					obj.nextSibling.value = obj.options[obj.selectedIndex].text;
				}
				obj.focus();
			}

			function change_country()
			{
				if (document.getElementById('country').value == 74)
				{
					document.getElementById('city_name').style.display = 'none';
					document.getElementById('city').style.display = 'inline';
				}
				else
				{
					document.getElementById('city_name').style.display = 'inline';
					document.getElementById('city').style.display = 'none';
				}
				
				if (document.getElementById('birth_country').value == 74)
				{
					document.getElementById('birth_city_name').style.display = 'none';
					document.getElementById('birth_city').style.display = 'inline';
				}
				else
				{
					document.getElementById('birth_city_name').style.display = 'inline';
					document.getElementById('birth_city').style.display = 'none';
				}
			}
			
			function change_type(value)
			{
				if (value == 30)
					document.getElementById('etatcivil').style.display = 'none';
				else
					document.getElementById('etatcivil').style.display = '';
			}

			function contact_delete()
			{
				if(confirm('Etes vous sûr de vouloir supprimer ce contact ?'))
					api_call(query.get('server'),'optimus/'+query.get('db')+'/contacts/'+query.get('id'),'DELETE',{},'contact_deleted');
			}
			
			function contact_deleted()
			{
				parent.window.open('about:blank', '_self');
				parent.window.close();
			}

			var update_link = new BroadcastChannel('update_link');
			update_link.onmessage = function (ev) {window.location.reload()}
			window.onblur = function(){update_link.postMessage('update_link')}
		</script>
	</body>
</html>
