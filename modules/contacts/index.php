<!DOCTYPE html>
<html lang="fr">

<head>
	<title>CONTACTS</title>
	<meta name="viewport" content="width=410, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="/modules/contacts/favicon-32x32.png">
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/windows.css">
	<link rel="stylesheet" href="/css/dynamictable.css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/windows.js"></script>
	<script src="/js/dynamictable.js"></script>
	<script src="/js/number_format.js"></script>
	<script src="/js/cookies.js"></script>
</head>

<body>

	<script type="text/javascript">

		const query = new URLSearchParams(window.location.search);
		
		dynamictable = new Object();
		dynamictable.user_server = get_cookie('server');
		dynamictable.user_db = get_cookie('db');
		dynamictable.server = query.get('server');
		dynamictable.db = query.get('db');
		dynamictable.module = 'contacts';
		dynamictable.grid = <?php include_once('contacts.json');?>;
		dynamictable.advanced_search = [];
		dynamictable.global_search = "";
		dynamictable.page = 1;
		dynamictable.results = 30;
		dynamictable.version = 46;
		
		api_call(query.get('server'),'optimus/'+query.get('db')+'/settings', 'GET', {'module':dynamictable.module}, 'init');
		
		function init(response)
		{
			dynamictable_init(dynamictable);
			insert = document.createElement('button');
			insert.innerHTML = 'NOUVEAU CONTACT';
			insert.onclick = function()
			{
				api_call(query.get('server'),'optimus/'+query.get('db')+'/contacts', 'POST', {}, 'contact_created');
			}
			document.getElementById('controls').appendChild(insert);
		}
		
		function contact_created(contact)
		{
			editor = window.open('/modules/contacts/editor.php?server='+query.get('server')+'&db='+query.get('db')+'&id='+contact.data.id,'editor');
			window.location.reload();
		}
		
		function client(row,column)
		{
			td.style.cursor='pointer';
			td.style.color='#0000B0';
			td.onclick=function()
			{
				editor = window.open('/modules/contacts/editor.php?server='+query.get('server')+'&db='+query.get('db')+'&id='+row[0],'editor');
			}
			return row[column];
		}

		function email(row,column)
		{
			td.style.cursor='pointer';
			td.style.color='#0000B0';
			td.onclick=function(){window.location='https://webmail.'+query.get('server')+'/?_task=mail&_action=compose&_to='+row[column]};
			return row[column];
		}
		
		function website(row,column)
		{
			td.style.cursor='pointer';
			td.style.color='#0000B0';
			td.onclick=function(){window.open('https://'+row[column])};
			return row[column];
		}

		var update_link = new BroadcastChannel('update_link');
		update_link.onmessage = function (ev) {window.location.reload()}
		window.onblur = function(){update_link.postMessage('update_link')}
	</script>
</body>
</html>