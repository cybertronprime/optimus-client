<!DOCTYPE html>
<html lang="fr">
	<head>
		<link rel="stylesheet" href="/css/main.css" />
		<link rel="stylesheet" href="/css/statictable.css" />
		<script type="text/javascript" src="/js/main.js"></script>
		<script type="text/javascript" src="/js/api_call.js"></script>
		<script type="text/javascript">
			const query = new URLSearchParams(window.location.search);
			api_call(query.get('server'), 'optimus/'+query.get('db')+'/contacts/'+query.get('id'), 'GET', {}, 'init');
			function init(response)
			{
				if (response.data[0].siret)
				{
					annonces = api_call_sync('optimus-avocats.fr', 'bodacc/', 'GET', {"siret":response.data[0].siret});
					if (annonces.data.length > 0)
					{
						document.body.innerHTML = '<br/><div style="font:bold 20px Verdana">' + annonces.data.length + ' ANNONCE(S) PUBLIEE(S) AU BODACC</div><br/>';
						
						table = document.createElement('table');
						table.className = 'statictable';
						document.body.appendChild(table);
						
						thead = table.createTHead();
						tr = thead.insertRow();
						tr.insertCell().innerHTML = 'DATE';
						tr.insertCell().innerHTML = 'CATEGORIE';
						tr.insertCell().innerHTML = 'GREFFE DE DEPOT';
						tr.insertCell().innerHTML = 'LIEN';
						
						tbody = table.createTBody();
						for (i=0; i < annonces.data.length; i++)
						{
							tr = tbody.insertRow();
							tr.insertCell().innerHTML = annonces.data[i].date;
							tr.insertCell().innerHTML = annonces.data[i].type;
							tr.insertCell().innerHTML = annonces.data[i].depot;
							tr.insertCell().innerHTML = '<a href="#" onclick="window.open(\'https://www.bodacc.fr/' + annonces.data[i].href +  '\')"><img src="/images/filetypes/htm.png"/></a>';
						}

					}
					else
						document.body.innerHTML = '<br/><div style="font:bold 20px Verdana">AUCUNE ANNONCE N\'A ETE PUBLIEE AU BODACC</div><br/>';
				}
				else
					notification("Merci de renseigner le N° SIRET de l'entreprise pour activer ce module","#FF0000");
			}
		</script>
	</head>

	<body>
	
	</body>
	
</html>