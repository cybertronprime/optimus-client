<!DOCTYPE html>
<html>
	<head>
		<style type="text/css">
		  html { height: 100% }
		  body { height: 100%; margin: 0; padding: 0 }
		  #map-canvas { height: 100%; width:100% }
		</style>
		<script type="text/javascript" src="/js/api_call.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAbmr-EKcnrIdz6xWEORTTnoauDz36jbew&sensor=true"></script>
		<script type="text/javascript">
		
			const query = new URLSearchParams(window.location.search);
			itm = {};
			authorizations = {};
			huissiers = {};
			
			api_call(query.get('server'), 'optimus/'+query.get('db')+'/contacts/'+query.get('id'), 'GET', {}, 'init');
			function init(response)
			{
				itm = response.data[0];
				authorizations = response.authorizations;
				document.getElementById('map-canvas').style.visibility = 'visible';
				
				address = '';
				itm.address = itm.address.split("\n")
				for (i=0; i < itm.address.length; i++)
					if (itm.address[i].search('CEDEX') == -1 && itm.address[i].search('BP') == -1 && itm.address[i].search('CP') == -1 && itm.address[i].search('ZI') == -1)
						address += itm.address[i] + ' ';
				
				var latlon = new XMLHttpRequest();
				latlon.open('GET', 'https://nominatim.openstreetmap.org/search?format=json&limit=3&q=' + address + ', ' + itm.zipcode + ', ' + itm.city_name, false);
				latlon.send();
				lat = JSON.parse(latlon.responseText)[0].lat;
				lon = JSON.parse(latlon.responseText)[0].lon;
				
				initialize(lat, lon);
			}
			
			function initialize(lat, lon)
			{
				var mapOptions = 
				{
					center: new google.maps.LatLng(lat, lon),
					zoom: 18,
					mapTypeId: google.maps.MapTypeId.HYBRID,
					gestureHandling: 'greedy'
				};
				var map = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);
				var marker = new google.maps.Marker(
				{
					position: new google.maps.LatLng(lat, lon),
					map: map,
					title:"CONTACT"
				});
				}
				google.maps.event.addDomListener(window, 'load', initialize);
		</script>
	</head>

	<body style="overflow:hidden">
		<div id="map-canvas"></div>
	</body>
</html>