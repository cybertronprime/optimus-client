<?php
include('../../../../connect.php');
define("BASE_PATH", "/srv/files/" . $user['email']);

if (isset($_GET["path"]))
{
	$list[] = ["name" => "passwords","path" => "passwords","rev" => filemtime('passwords.kdbx'),"dir" => false];
 	header("Content-Type: text/json");
	print json_encode($list);
	return;
}


if (isset($_GET["file"]))
{
	$file = BASE_PATH . "/passwords.kdbx";
	header("Content-Type: application/binary");
	clearstatcache();
	header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($file)).' GMT', true, 200);
	echo file_get_contents($file);
	return;
}


if (isset($_GET["stat"]))
{
	$file = BASE_PATH . "/passwords.kdbx";
	clearstatcache();
	header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($file)).' GMT', true, 200);
	echo "";
	return;
}


if (isset($_GET["save"]))
{
	$file = BASE_PATH . "/passwords.kdbx";
	$rev = $_GET["rev"];
	clearstatcache();
	$current_rev = gmdate('D, d M Y H:i:s', filemtime($file)).' GMT';

	if ($current_rev !== $rev)
	{
		header('HTTP/1.0 500 Revision mismatch');
		 return;
	}

	$contents = file_get_contents("php://input");
	if (strlen($contents) > 0)
		file_put_contents($file, $contents);

	clearstatcache();
	header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($file)).' GMT', true, 200);
	echo "";
	return;
}