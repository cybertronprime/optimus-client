<html lang="fr">
<head>
	<title>OPTIMUS V4.57</title>
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, shrink-to-fit=yes">
	<link rel="icon" type="image/png" sizes="16x16" href="/images/logo/favicon_16x16.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/images/logo/favicon_32x32.png">
	<link rel="icon" type="image/png" sizes="180x180" href="/images/logo/favicon_180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="/images/logo/favicon_192x192.png">
	<link rel="icon" type="image/png" sizes="512x512" href="/images/logo/favicon_512x512.png">
	<link href="/css/main.css" rel="stylesheet" type="text/css">
	<link href="/css/homepage.css" rel="stylesheet" type="text/css">
	<script src="/js/main.js"></script>
	<script src="/js/api_call.js"></script>
	<script src="/js/cookies.js"></script>
	
	<script>
		modules = 	[
						{
							"type":"avocat",
							"name":"Avocat",
							"apps":
							[
								{'name':'contacts', 'description':'Contacts', 'window':'dynamictable'},
								{'name':'dossiers', 'description':'Dossiers', 'window':'dynamictable'},
								{'name':'interventions', 'description':'Interventions', 'window':'dynamictable'},
								//{'name':'domaines', 'description':'Domaines','window':'dynamictable'},
								//{'name':'marques', 'description':'Marques','window':'dynamictable'},
								//{'name':'modeles', 'description':'Modèles','window':'dynamictable'},
								//{'name':'alertes', 'description':'Alertes','window':'dynamictable'},
								{'name':'fichiers', 'description':'Fichiers','window':'fichiers'},
								{'name':'emails', 'description':'Emails','window':'emails'},
								{'name':'agenda', 'description':'Disabled_Agenda','window':'agenda'},
								{'name':'notes', 'description':'Disabled_Notes','window':'notes'},
								{'name':'dictees', 'description':'Disabled_Dictées','window':'dictees'},
								{'name':'ovh-phone', 'description':'Disabled_Téléphone','window':'phone'},
								{'name':'fax', 'description':'Disabled_Fax','window':'fax'},
								{'name':'recouvrements', 'description':'Disabled_Recouvrements','window':'dynamictable'},
							]
						},
						{
							"type":"compta",
							"name":"Comptabilité",
							"apps":
							[
								
								{'name':'factures', 'description':'Factures','window':'dynamictable'},
								{'name':'grand-livre', 'description':'Disabled_Grand Livre','window':'compta'},
								{'name':'releves', 'description':'Disabled_Relevés','window':'compta'},
								{'name':'des', 'description':'Disabled_DES','window':'compta'},
								{'name':'tva', 'description':'Disabled_TVA','window':'compta'},
								{'name':'recettes', 'description':'Disabled_Recettes','window':'compta'},
								{'name':'stats', 'description':'Disabled_Stats','window':'compta'},
								{'name':'benefices', 'description':'Disabled_Bénéfices','window':'compta'},
								{'name':'paie', 'description':'Disabled_Paie','window':'compta'},
								{'name':'repartition-salaries', 'description':'Disabled_Salariés','window':'compta'},
								{'name':'charges-perso', 'description':'Disabled_Charges Perso','window':'compta'},
							]
						},
						{
							"type":"outils",
							"name":"Outils",
							"apps":
							[
								{'name':'visioconference', 'description':'Visioconférence','window':'tools'},
								{'name':'vcard', 'description':'Disabled_Vcard','window':'tools'},
								{'name':'interets', 'description':'Disabled_Intérêts','window':'tools'},
								{'name':'keeweb', 'description':'Disabled_Keeweb','window':'tools'},
								{'name':'help', 'description':'Disabled_Aide','window':'tools'},
							]
						},
						{
							"type":"server",
							"name":"Serveur",
							"apps":
							[
								{'name':'create-server', 'description':'Disabled_Créer','window':'admin'},
								{'name':'admin', 'description':'Disabled_Administrer','window':'admin'},
								{'name':'users-manage', 'description':'Disabled_Utilisateurs','window':'admin'},
								{'name':'backup', 'description':'Disabled_Backup','window':'admin'},
								{'name':'console', 'description':'Disabled_Console','window':'admin'},
							]
						},
					];
	</script>
</head>

<body>
	<img src="/images/logo/optimus-avocats.svg" style="width:calc(100% - 30px);max-width:400px;margin-top:100px" />
	<img src="/images/logout.svg" style="position:absolute;top:5px;right:5px;width:48px;height:48px;filter:drop-shadow(2px 2px 4px rgba(0,0,0,0.5))" onclick="logout()" />
	<div id="server" onchange="api_call(server,'optimus/logged','GET',{},'init');" style="position:absolute;top:10px;left:10px;height:16px;display:flex;align-items: center;"></div>
	<div id="db" style="position:absolute;top:34px;left:10px;height:16px;display:flex;align-items: center;"></div>
	<div id="user" style="position:absolute;top:58px;left:10px;height:16px;font:bold 12px Verdana;border:0px;color:#000000;text-align:left;display:flex;align-items: center;"></div>
	<div id="container"></div>
	OPTIMUS V4.57<a href="mailto:info@optimus-avocats.fr" style="font:normal 12px Verdana;color:#000000;text-decoration:none"></a><br/>
	<span style="color:#00AA00">Ce logiciel s'exécute dans votre navigateur. Aucune donnée ne transite par le serveur optimus-avocats.fr<br/></span>
	<span id="browser_compatibility" style="color:#FF0000;display:none">Ce logiciel est conçu pour être utilisé avec Chrome ou Firefox. Les autres navigateurs n'ont pas été testés de manière poussée.</span>
</body>

<script>

	if (detect_browser() != 'Chrome')
		document.getElementById('browser_compatibility').style.display = '';
	
	if (localStorage.getItem('version') != "<?php echo file_get_contents('VERSION');?>")
	{
		localStorage.clear();
		localStorage.setItem('version',"<?php echo file_get_contents('VERSION');?>");
	}
	
	
	if (get_cookie('server'))
		api_call(get_cookie('server'),'optimus/logged','GET',{},'init');
	else
		login_open();
	
	
	function reload()
	{
		window.location.reload();
	}
	
	
	function init(response)
	{
		version = api_call_sync(get_cookie('server'),'optimus/ping','GET',{});

		document.getElementById('server').innerHTML = '<img src="/lib/fontawesome/server.svg" style="filter:contrast(40%);width:14px"/>&nbsp;&nbsp;<input value="' + get_cookie('server') +'"/>&nbsp;';
		document.getElementById('server').innerHTML += '<span style="color:' + (version.data.date<localStorage.getItem('version')?'#FF0000':'#000000') + '">V.' + version.data.version + ' (' + version.data.date + ') &nbsp; </span>';
		document.getElementById('server').innerHTML += '<img src="/lib/fontawesome/sync.svg" style="height:12px;filter:contrast(40%);margin-top:2px" onclick="this.style.animation = \'rotation 2s infinite linear\';api_call(get_cookie(\'server\'),\'optimus/update\',\'GET\',{},\'reload\');"/><br/>';
		document.getElementById('db').innerHTML = '<img src="/lib/fontawesome/database.svg" style="filter:contrast(40%);width:14px"/>&nbsp;&nbsp;<select id="db_list" style="width:177px"></select><br/>';
		structures = api_call_sync(get_cookie('server'),'optimus/'+get_cookie('db')+'/structures','GET',{});
		for (structure of structures.data)
		{
			members = api_call_sync(get_cookie('server'),'optimus/'+structure.db+'/members','GET',{});
			for (member of members.data)
				document.getElementById('db_list').add(new Option(member.member,member.member));
			document.getElementById('db_list').value = get_cookie('db');
		}
		document.getElementById('user').innerHTML = '<img src="/lib/fontawesome/user.svg" style="filter:contrast(40%);width:14px"/>&nbsp;&nbsp;' + get_cookie('user') +'<br/>';
		
		document.getElementById('container').innerHTML='';
		for (let i=0; i < modules.length ; i++)
		{
			module = document.createElement('div');
			module.id = modules[i].type;
			module.className = 'icon_modules';
			module.style.display = 'block';
			module.style.width = 'calc(100% - 32px)';
			module.style.maxWidth = (modules[i].apps.length*96+32) + 'px';
			module.style.margin = '32px auto';
			document.getElementById('container').appendChild(module);

			titlebar = document.createElement('div');
			titlebar.innerHTML = modules[i].name.toUpperCase();
			module.appendChild(titlebar);

			icon_container = document.createElement('div');
			module.appendChild(icon_container);

			for (let j=0; j < modules[i].apps.length ; j++)
			{
				icon =  document.createElement('span');
				icon.id = modules[i].apps[j].name;
				icon.onclick = function(){window.open('/modules/' + modules[i].apps[j].name + '/index.php?server='+document.getElementById('server').children[1].value+'&db='+document.getElementById('db').children[1].value,modules[i].apps[j].window).focus()};
				icon_container.appendChild(icon);

				icon_image = document.createElement('img');
				icon_image.src = '/modules/' + modules[i].apps[j].name + '/icon.svg';
				icon.appendChild(icon_image);

				icon_text = document.createElement('span');
				icon_text.style.display = 'block';
				icon_text.innerHTML = modules[i].apps[j].description;
				icon.appendChild(icon_text);

				if (modules[i].apps[j].description.includes('Disabled'))
				{
					icon_text.innerHTML = modules[i].apps[j].description.substr(9,99);
					icon.style.opacity = '0.3';
					icon.style.filter = 'grayscale(1)';
					icon.onclick = function(){alert('Cette fonction sera activée dans les prochains jours')};
				}
			}
		}
	}
	
	function logout()
	{
		api_call(get_cookie('server'),'optimus/logout','GET',{},'reload');
	}
	
	window.onmessage = function (event) {if (event.data == 'logged') window.location.reload()}
	// function check_fax()
	// {
		// if (document.getElementById('fax').getElementsByTagName('p')[0])
		// document.getElementById('fax').removeChild(document.getElementById('fax').getElementsByTagName('p')[0]);

		// ajaxcall('modules/fax/counter.php');
		// if (ajax.responseText > 0)
		// {
			// fax_badge = document.createElement('p');
			// ajaxcall('modules/fax/counter.php');
			// fax_badge.innerHTML = ajax.responseText;
			// document.getElementById('fax').appendChild(fax_badge);
		// }
	// }

	// if (document.getElementById('fax'))
	// {
		// check_fax();
		// setInterval('check_fax()',60000);
	// }
</script>
</html>
