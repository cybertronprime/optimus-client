function get_cookie(name) 
{
	var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
	return v ? v[2] : null;
}

function set_cookie(name, value, days) 
{
	var d = new Date;
	d.setTime(d.getTime() + 24*60*60*1000*days);
	document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
}

function delete_cookie(name) 
{ 
	setCookie(name, '', -1);
}