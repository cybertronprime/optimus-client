function dynamictable_init(dynamictable)
{
	dynamictable_load_settings();

	table = document.createElement('table');
	table.style.padding = '15px';
	table.style.margin = 'auto';
	table.style.top = '0px';
	table.style.left = '50%';
	table.style.position = 'fixed';
	table.style.transform = 'translate(-50%, 0)';
	document.body.appendChild(table);

	tr = document.createElement('tr');
	table.appendChild(tr);

	td = document.createElement('td');
	td.id = 'controls';
	td.style.textAlign = 'right';
	td.style.font = 'bold 16px Roboto';
	td.style.whiteSpace = 'nowrap';
	tr.appendChild(td);

	input = document.createElement('input');
	input.type = 'search';
	input.id = 'global_search';
	input.style.height = '26px';
	input.style.padding = '4px 4px 4px 16px';
	input.style.width = '150px';
	input.style.textAlign = 'center';
	input.style.margin = 'auto';
	input.style.display='block';
	input.onkeyup = function()
	{
		if (typeof(globsearch)!='undefined')
			clearTimeout(globsearch);
		globsearch = setTimeout('dynamictable.page=1;dynamictable_update()',200);
	}
	input.onsearch = function(){this.onkeyup()}
	input.setAttribute('autocomplete','off');
	input.setAttribute('placeholder','Recherche Globale');
	input.setAttribute('spellcheck','false');
	td.appendChild(input);

	input = document.createElement('button');
	input.id = 'advanced_search_button';
	input.innerHTML = 'RECHERCHE AVANCEE';
	input.onclick = function()
	{
		curtain_open();
		show_advanced_search(new optimus_window('',410,520,'RECHERCHE AVANCEE',false,false));
	}
	td.appendChild(input);

	input = document.createElement('button');
	input.id = 'advanced_search_reset_button';
	input.className = 'sm';
	input.style.marginLeft=0;
	input.style.borderTopLeftRadius=0;
	input.style.borderBottomLeftRadius=0;
	input.style.borderLeft=0;
	input.innerHTML = '<img src="/lib/fontawesome/trash.svg" style="width:10px"/>';
	input.onclick = function()
	{
		dynamictable.advanced_search = new Array();
		dynamictable.page=1;
		dynamictable_update();
		return false;
	}
	td.appendChild(input);

	input = document.createElement('button');
	input.id = 'show_hide_button';
	input.className = 'sm';
	input.innerHTML = 'AFFICHAGE';
	input.onclick = function()
	{
		curtain_open();
		show_hide(new optimus_window('',250,52+26*(dynamictable.grid.length),'AFFICHAGE',false,false));
	}
	td.appendChild(input);

	input = document.createElement('button');
	input.id = 'show_hide_reset_button';
	input.className = 'sm';
	input.style.marginLeft=0;
	input.style.borderTopLeftRadius=0;
	input.style.borderBottomLeftRadius=0;
	input.style.borderLeft=0;
	input.innerHTML = '<img src="/lib/fontawesome/trash.svg" style="width:10px"/>';
	input.onclick = function()
	{
		for(i in dynamictable.grid)
			dynamictable.grid[i].show = 1;
		dynamictable.page=1;
		dynamictable_update();
		return false;
	}
	td.appendChild(input);
	
	input = document.createElement('button');
	input.id = 'reset_button';
	input.className = 'sm';
	input.innerHTML = 'RESET';
	input.onclick = function()
	{
		settings = api_call_sync(dynamictable.user_server,'optimus/'+dynamictable.user_db+'/settings', 'DELETE', {'module':dynamictable.module});
		window.location.reload();
	}
	td.appendChild(input);


	maintable = document.createElement('table');
	maintable.className = 'dynamictable';
	maintable.oncontextmenu = function(){return false;}
	document.body.insertBefore(maintable,table);

	marker = document.createElement('div');
	marker.className = 'dynamictable_marker';
	marker.style.position = 'fixed';
	marker.style.top = maintable.offsetTop - 15 + 'px';
	marker.style.font = 'normal 12px Roboto';
	marker.style.visibility = 'hidden';
	marker.innerHTML = '&gt;|&lt;';
	maintable.offsetParent.insertBefore(marker,maintable);

	dynamictable_update('init');
	document.getElementById('global_search').value = dynamictable.global_search;

	window.onload = function()
	{
		document.getElementById('global_search').select();
	}

	window.addEventListener('mousewheel',dynamictable_scroll);
	window.addEventListener('resize',function(){dynamictable_footer()});
}

function dynamictable_scroll(event)
{
	if (event.wheelDelta == 120 && dynamictable.page!=1)
	{
		dynamictable.page--;
		dynamictable_footer();
		if (typeof(wheeling)!='undefined')
			clearTimeout(wheeling);
		wheeling = setTimeout('dynamictable_update()',100);
	}
	if (event.wheelDelta == -120 && dynamictable.page != dynamictable.pages)
	{
		dynamictable.page++;
		dynamictable_footer();
		if (typeof(wheeling)!='undefined')
			clearTimeout(wheeling);
		wheeling = setTimeout('dynamictable_update()',100);
	}
	return false;
}

function dynamictable_header()
{
	if(maintable.tHead)
		maintable.removeChild(maintable.tHead);
	var thead = document.createElement('thead');
	maintable.appendChild(thead);

	var tr = document.createElement('tr');
	maintable.tHead.appendChild(tr);

	for (j=0; j < dynamictable.grid.length; j++)
		for (var k in dynamictable.grid)
			if (dynamictable.grid[k].order === j)
			{
				var td = document.createElement('td');
				td.innerHTML = '<span style="display:inline-block;text-overflow:ellipsis">'+ dynamictable.grid[k].title +'</span>';
				if (dynamictable.grid[k].show == 0)
					td.style.display = 'none';
				td.style.whiteSpace = 'nowrap';
				td.className = dynamictable.grid[k].class;
				td.id = k;
				td.order = dynamictable.grid[k].order;
				if (dynamictable.grid[k].searchable==true)
				td.onclick = function()
				{
					if (dynamictable.grid[this.id].sort_order == null)
					{
						higher = -1;
						for (m=0; m < dynamictable.grid.length; m++)
							if(Number.isInteger(dynamictable.grid[m].sort_rank))
								higher = Math.max(higher, dynamictable.grid[m].sort_rank);
						dynamictable.grid[this.id].sort_rank = higher + 1;
						dynamictable.grid[this.id].sort_order = 'ASC';
					}
					else if (dynamictable.grid[this.id].sort_order == 'ASC')
						dynamictable.grid[this.id].sort_order = 'DESC';
					else
					{
						for (m=0; m < dynamictable.grid.length; m++)
							if (dynamictable.grid[m].sort_rank > dynamictable.grid[this.id].sort_rank)
								dynamictable.grid[m].sort_rank--;
						dynamictable.grid[this.id].sort_rank = null;
						dynamictable.grid[this.id].sort_order = null;
					}
					dynamictable.page=1;
					dynamictable_update();
				}
				if (dynamictable.grid[k].searchable == true)
				td.oncontextmenu = function()
				{
					if (Number.isInteger(dynamictable.grid[this.id].sort_rank))
					{
						for (m=0; m < dynamictable.grid.length; m++)
							if (dynamictable.grid[m].sort_rank > dynamictable.grid[this.id].sort_rank)
								dynamictable.grid[m].sort_rank--;
						dynamictable.grid[this.id].sort_rank = null;
						dynamictable.grid[this.id].sort_order = null;
						dynamictable.page=1;
						dynamictable_update();
					}
					return false;
				}
				if (dynamictable.grid[k].searchable == false)
					td.style.cursor = 'default';
				td.draggable = true;
				td.ondragstart = function(event)
				{
					dragged = event.target;
				}
				td.ondrop=function(event)
				{
					if(!event.target.id || event.target.id == 'marker')
						target = event.target.offsetParent;
					else
						target = event.target;
					if(dragged.id != 'marker')
					{
						if (event.offsetX < (event.target.offsetWidth/2) && event.target.id != 'marker')
							for (i=0; i<maintable.rows.length;i++)
								maintable.rows[i].insertBefore(maintable.rows[i].cells[dragged.order],maintable.rows[i].cells[target.order])
						else
							for (i=0; i<maintable.rows.length;i++)
								maintable.rows[i].insertBefore(maintable.rows[i].cells[dragged.order],maintable.rows[i].cells[target.order+1])
					}
					for (m=0; m < maintable.rows[0].cells.length; m++)
					{
						dynamictable.grid[maintable.rows[0].cells[m].id].order = m;
						maintable.rows[0].cells[m].order = m;
					}
					dynamictable_save_settings();
				}
				td.ondragover=function(event)
				{
					if (event.target.id && event.target.id != 'marker' && dragged.id != event.target.id && dragged.id!='marker')
						if (event.offsetX < (event.target.offsetWidth/2))
							marker.style.left = (event.pageX-event.offsetX-7)+'px';
						else
							marker.style.left = (event.pageX+event.target.offsetWidth-event.offsetX-7)+'px';
					return false;
				}
				td.ondragenter=function(event)
				{
					if (dragged.id != event.target.id) 
						marker.style.visibility='visible';
				}
				td.ondragend=function(event)
				{
					marker.style.visibility='hidden';
				}
				maintable.tHead.rows[0].appendChild(td);
				
				//RESIZE RIGHT
				var resize_right = document.createElement('div');
				resize_right.id = 'marker';
				resize_right.style.float = 'right';
				resize_right.style.cursor = 'ew-resize';
				resize_right.style.width = '4px';
				resize_right.style.height = '100%';
				resize_right.draggable = true;
				resize_right.onmouseover = function(event)
				{
					marker.style.left = event.clientX - event.offsetX - 3 + 'px';
					marker.style.visibility='visible';
					this.offsetParent.style.borderRight="1px dashed #FF0000";
				}
				resize_right.onmouseout = function(event)
				{
					marker.style.visibility='hidden';
					this.offsetParent.style.borderRight="1px solid #000000";
				}
				resize_right.onclick = function(event)
				{
					event.stopPropagation();
				}
				resize_right.ondragstart = function(event)
				{
					start = event.screenX;
					marker.style.visibility='visible';
					for (i=0; i<maintable.rows.length; i++)
						maintable.rows[i].cells[event.target.offsetParent.order].style.borderRight="1px dashed #FF0000";
				}
				resize_right.ondrag = function(event)
				{
					var input = maintable.rows[1].cells[event.target.offsetParent.order].getElementsByTagName('input')[0];
					var td = maintable.rows[1].cells[event.target.offsetParent.order];
					//NE MARCHE PAS SOUS FIREFOX - offsetX toujours égal à 0 sur le ondrag. Fix dans ondragend
					input.style.width = Math.max(input.offsetWidth + (detect_browser()=='Firefox'?0:event.offsetX-10),td.offsetWidth - 10, 50) + 'px';
					marker.style.left = maintable.offsetLeft + input.offsetParent.offsetLeft + input.offsetWidth - 1 +'px';
					if (maintable.offsetWidth >= window.innerWidth)
					{
						document.body.style.width = maintable.offsetWidth + 10 + 'px';
						document.body.style.width = maintable.offsetWidth + 'px';
					}
					else
						document.body.style.width = window.innerWidth + 'px';
					dynamictable_footer();
				}
				resize_right.ondragend = function(event)
				{
					if (detect_browser()=='Firefox')
					{
						var input = maintable.rows[1].cells[event.target.offsetParent.order].getElementsByTagName('input')[0];
						input.style.width = Math.max(input.offsetWidth + event.screenX - start,td.offsetWidth - 10, 50) + 'px';
						dynamictable_footer();
					}
					
					if (maintable.offsetWidth <= window.innerWidth)
						document.body.style.width = window.innerWidth + 'px';
						
					dynamictable.grid[event.target.offsetParent.id].width = event.target.offsetParent.offsetWidth+1;
					for (i=0; i<maintable.rows.length; i++)
						maintable.rows[i].cells[event.target.offsetParent.order].style.borderRight="1px solid #000000";
					
					dynamictable_save_settings();
				}
				td.appendChild(resize_right);
			}
	
	//SEARCH INPUTS
	var tr = document.createElement('tr');
	maintable.tHead.appendChild(tr);
	for (j=0; j < dynamictable.grid.length; j++)
		for (var k in dynamictable.grid)
			if (dynamictable.grid[k].order === j)
			{
				var td = document.createElement('td');
				if (dynamictable.grid[k].show == 0)
					td.style.display = 'none';
				td.style.backgroundColor = '#FF8000';
				td.className = dynamictable.grid[k].class;
				tr.appendChild(td);
				
				var sinput = document.createElement('input');
				sinput.type = 'search';
				sinput.style.margin = '5px 3px 5px 3px';
				sinput.style.textAlign = dynamictable.grid[k].align;
				sinput.style.height = '22px';
				sinput.style.width = Math.max(50,td.offsetWidth-7,dynamictable.grid[k].width-8)+'px';
				sinput.style.border = '1px solid #000000';
				sinput.style.font = 'normal 12px Roboto';
				if (dynamictable.grid[k].filter)
					sinput.value = dynamictable.grid[k].filter;
				sinput.onsearch = function(){this.onkeyup()}
				sinput.onkeyup = function()
				{
					if (typeof(colsearch)!='undefined')
						clearTimeout(colsearch);
					colsearch = setTimeout('dynamictable.page=1;dynamictable_column_search();',200);
				}
				sinput.oncontextmenu = function(){this.value='';dynamictable_column_search()}
				sinput.spellcheck = false;
				if (dynamictable.grid[k].searchable == false)
					sinput.style.visibility = 'hidden';
				td.appendChild(sinput);
				
			}
}

function dynamictable_header_sorts()
{
	for (j=0; j < dynamictable.grid.length; j++)
		for (var k in dynamictable.grid)
			if (dynamictable.grid[k].order == j)
			{
				var td = maintable.rows[0].cells[j];
				if (td.getElementsByTagName('canvas')[0])
					td.removeChild(td.getElementsByTagName('canvas')[0]);
				if (td.getElementsByTagName('sub')[0])
					td.removeChild(td.getElementsByTagName('sub')[0]);
				
				if (Number.isInteger(dynamictable.grid[k].sort_rank))
				{
					var canvas = document.createElement('canvas');
					canvas.width=11;
					canvas.height=7;
					td.appendChild(canvas);
					
					context = canvas.getContext('2d');
					if (dynamictable.grid[k].sort_order == 'ASC')
					{
						context.moveTo(10.5,6.5);
						context.lineTo(-0.5,6.5);
						context.lineTo(5.5,-0.5);
						context.lineTo(10.5,6.5);
					}
					else
					{
						context.moveTo(-0.5,0.5);
						context.lineTo(10.5,0.5);
						context.lineTo(5.5,7.5);
						context.lineTo(-0.5,0.5);
					}
					context.fillStyle = window.getComputedStyle(canvas)['color'];
					context.strokeStyle = window.getComputedStyle(canvas)['stroke'];
					context.fill();
					context.stroke();
					
					var sub = document.createElement('sub');
					sub.innerHTML = dynamictable.grid[k].sort_rank + 1;
					td.appendChild(sub);
				}
			}
}

function dynamictable_body()
{
	if(maintable.tBodies[0])
		maintable.removeChild(maintable.tBodies[0]);

	var tbody = document.createElement('TBODY');
	maintable.appendChild(tbody);
	for (i in data)
	{
		var tr = document.createElement('tr');
		tbody.appendChild(tr);
		
		for (j in data[i])
			for (l=0; l < dynamictable.grid.length; l++)
				if (dynamictable.grid[l].order == j)
				{
					td = document.createElement('td');
					if (dynamictable.grid[l].show == 0)
						td.style.display = 'none';
					td.style.textAlign = dynamictable.grid[l].align;
					td.className = dynamictable.grid[l].class;
					
					td.innerHTML = dynamictable_format(data[i], l, dynamictable.grid[l].data_type);
						
					tr.appendChild(td);
				}
	}
}

function dynamictable_footer()
{
	if (typeof footer == 'undefined')
	{
		footer = document.createElement('div');
		footer.className = 'dynamictable_footer';
		document.body.appendChild(footer);
	}

	footer.innerHTML='';

	if (maintable.offsetWidth > window.innerWidth-8)
		footer.style.width = window.innerWidth-8+'px';
	else
		footer.style.width = maintable.offsetWidth-8+'px';

	footer.style.left = maintable.offsetLeft+'px';

	if (document.documentElement.offsetHeight > window.innerHeight)
		footer.style.top = window.innerHeight - 41 + 'px';
	else
		footer.style.top = maintable.offsetTop + maintable.offsetHeight + 'px';

	dynamictable.pages = parseInt((dynamictable.total-1)/dynamictable.results+1);

	footer.innerHTML += '<div style="float:left">Page<input id="page" type="text" value="'+dynamictable.page+'" style="width:30px;text-align:center" onkeypress="if (event && event.keyCode==13) {dynamictable.page=parseInt(this.value);if (dynamictable.page>dynamictable.pages) dynamictable.page=dynamictable.pages; if(dynamictable.page<1) dynamictable.page=1;dynamictable_update()}"></div>';
	footer.innerHTML += '<div style="float:right"><input id="results" type="text" value="'+dynamictable.results+'" style="width:30px;text-align:center" onkeypress="if (event && event.keyCode==13) {dynamictable.page=1;dynamictable.results=parseInt(this.value);dynamictable_update()}" onclick="this.select()">/ page</div>';
	footer.innerHTML += ((dynamictable.total>0)?(dynamictable.page-1)*dynamictable.results+1:'0') + ' à ' + ((dynamictable.page*dynamictable.results>dynamictable.total)?dynamictable.total:dynamictable.page*dynamictable.results) + ' sur ' + dynamictable.total + '<br>&nbsp;';
	if (dynamictable.pages <= 8)
		for (i=1; i<=dynamictable.pages; i++)
			footer.innerHTML += '<span id="'+i+'" style="color:'+((dynamictable.page==i)?'#FF8000':'')+'" onclick="dynamictable.page=parseInt(this.id);dynamictable_update()">'+i+'</span>&nbsp;';
	else
	{
		footer.innerHTML += '<span id="1" style="color:'+((dynamictable.page==1)?'#FF8000':'')+'" onclick="dynamictable.page=parseInt(this.id);dynamictable_update()">1</span>&nbsp;';
		if (dynamictable.page<=4)
		{
			for (i=2; i<=6; i++)
				footer.innerHTML += '<span id="'+i+'" style="color:'+((dynamictable.page==i)?'#FF8000':'')+'" onclick="dynamictable.page=parseInt(this.id);dynamictable_update()">'+i+'</span>&nbsp;';
			footer.innerHTML += '... ';
		}
		else if (dynamictable.page > dynamictable.pages-4)
		{
			footer.innerHTML += '... ';
			for (i=dynamictable.pages-5; i<=dynamictable.pages-1; i++)
				footer.innerHTML += '<span id="'+i+'" style="color:'+((dynamictable.page==i)?'#FF8000':'')+'" onclick="dynamictable.page=parseInt(this.id);dynamictable_update()">'+i+'</span>&nbsp;';
		}
		else
		{
			footer.innerHTML += '... ';
			for (i=(dynamictable.page-2); i<=(dynamictable.page+2); i++)
				footer.innerHTML += '<span id="'+i+'" style="color:'+((dynamictable.page==i)?'#FF8000':'')+'" onclick="dynamictable.page=parseInt(this.id);ddynamictable_update()">'+i+'</span>&nbsp;';
				footer.innerHTML += '... ';
		}
		footer.innerHTML += '<span id="'+dynamictable.pages+'" style="color:'+((dynamictable.page==dynamictable.pages)?'#FF8000':'')+'" onclick="dynamictable.page=parseInt(this.id);dynamictable_update()">'+dynamictable.pages+'</span>';
	}
}

function dynamictable_format(value,column,format)
{
	if (dynamictable.grid[column].onclick)
		result = eval(dynamictable.grid[column].onclick + "(value,column)");
	else
		result = value[column];

	if (result instanceof Array)
		result = value[column][1];

	if (format=='date')
		if (result)
			return result.substring(8,10) + '/' + result.substring(5,7) + '/' + result.substring(0,4);
		else
			return '';
	else if (format=='money')
		return number_format(result,2,'.',' ') + ' &euro;';
	else if (format=='truefalse')
		if (result==1)
			return '<img src="/images/checked.gif">';
		else
			return '&nbsp;';
	else if (format == 'text')
		return result;
	else if (format == 'integer')
		if (result)
			return parseInt(result);
		else
			return '';
	else if (format == 'decimal')
		return parseFloat(result);
	else if (format == 'rate')
		return parseFloat(result).toFixed(2) + ' %';
}

function show_hide(window)
{
	window.content.style.background = "url('/images/light_bg.jpg')";
	window.content.style.font = 'bold 14px Roboto';
	window.content.style.textAlign = 'left';
	window.content.style.lineHeight = '26px';
	window.content.style.padding = '10px';

	for (i=0; i < dynamictable.grid.length; i++)
	{
		var checkbox = document.createElement('input');
		checkbox.setAttribute('type','checkbox');
		checkbox.setAttribute('onclick','if (this.checked==true) show_column('+i+'); else hide_column('+i+')');
		if (document.getElementById(i).style.display != 'none')
			checkbox.setAttribute('checked','checked');
		window.content.appendChild(checkbox);
		window.content.innerHTML += '&nbsp;' + dynamictable.grid[i].title + '<br/>';
	}
}

function show_advanced_search(window)
{
	window.content.style.background = "url('/images/light_bg.jpg')";
	window.content.style.textAlign = 'center';

	var table = document.createElement('table');
	table.id = "advanced_search";
	table.style.paddingTop = '20px';
	window.content.appendChild(table);

	var tr = document.createElement('tr');
	
	var td = document.createElement('td');
	td.style.textAlign = 'center';
	td.style.lineHeight = '30px';
	tr.appendChild(td);

	var fields = document.createElement('select');
	fields.style.width='120px';
	fields.options[fields.options.length] = new Option('','');
	for (i=0; i < dynamictable.grid.length;i++)
		if (dynamictable.grid[i].searchable==true)
			fields.options[fields.options.length] = new Option(dynamictable.grid[i].title,i);
	td.appendChild(fields);

	var operators = document.createElement('select');
	operators.style.width='120px';
	operators.style.marginLeft='10px';
	operators.options[operators.options.length] = new Option('','');
	operators.options[operators.options.length] = new Option('est égal à','==');
	operators.options[operators.options.length] = new Option('est différent de','<>');
	operators.options[operators.options.length] = new Option('est plus grand que','>');
	operators.options[operators.options.length] = new Option('est plus grand ou égal à','>=');
	operators.options[operators.options.length] = new Option('est plus petit que','<');
	operators.options[operators.options.length] = new Option('est plus petit ou égal à','<=');
	operators.options[operators.options.length] = new Option('débute par','LIKE%');
	operators.options[operators.options.length] = new Option('contient','%LIKE%');
	operators.options[operators.options.length] = new Option('finit par','%LIKE');
	operators.options[operators.options.length] = new Option('ne débute pas par','NOT LIKE%');
	operators.options[operators.options.length] = new Option('ne contient pas','%NOT LIKE%');
	operators.options[operators.options.length] = new Option('ne finit pas par','%NOT LIKE');
	operators.options[operators.options.length] = new Option('ne contient rien','IS NULL');
	operators.options[operators.options.length] = new Option('contient quelque chose','IS NOT NULL');
	td.appendChild(operators);

	needle = document.createElement('input');
	needle.style.width='120px';
	needle.style.marginLeft='10px';
	td.appendChild(needle);

	boolean = document.createElement('select');
	boolean.style.width='60px';
	boolean.options[boolean.options.length] = new Option('AND','AND');
	boolean.options[boolean.options.length] = new Option('OR','OR');
	td.appendChild(boolean);

	for (i=0; i<7;i++)
		table.appendChild(tr.cloneNode(true));
	table.rows[6].cells[0].children[3].style.display='none';

	button = document.createElement('button');
	button.id = 'advanced_search_launcher';
	button.type = 'button';
	button.innerHTML = 'SEARCH';
	button.style.margin = '20px';
	button.onclick = function()
	{
		dynamictable.advanced_search = [];
		for (u=0; u<7; u++)
			if(document.getElementById('advanced_search').rows[u].cells[0].getElementsByTagName('select')[0].value)
			{
				dynamictable.advanced_search[u] = new Object();
				dynamictable.advanced_search[u].field = document.getElementById('advanced_search').rows[u].cells[0].getElementsByTagName('select')[0].value;
				dynamictable.advanced_search[u].operator = document.getElementById('advanced_search').rows[u].cells[0].getElementsByTagName('select')[1].value;
				dynamictable.advanced_search[u].value =  document.getElementById('advanced_search').rows[u].cells[0].getElementsByTagName('input')[0].value;
				dynamictable.advanced_search[u].next =  document.getElementById('advanced_search').rows[u].cells[0].getElementsByTagName('select')[2].value;
			}
		dynamictable.page=1;
		dynamictable_update();
		window.close();
	}
	content.appendChild(button);
	
	for (i=0; i < Object.keys(dynamictable.advanced_search).length; i++)
	{
		table.rows[i].cells[0].getElementsByTagName('select')[0].value = dynamictable.advanced_search[i].field;
		table.rows[i].cells[0].getElementsByTagName('select')[1].value = dynamictable.advanced_search[i].operator;
		table.rows[i].cells[0].getElementsByTagName('input')[0].value = dynamictable.advanced_search[i].value;
		table.rows[i].cells[0].getElementsByTagName('select')[2].value = dynamictable.advanced_search[i].next;
	}

	for (i=0; i<7; i=i+1)
		table.rows[i].cells[0].getElementsByTagName('input')[0].onkeypress = function(){if (event && event.keyCode==13) document.getElementById('advanced_search_launcher').click()};
		
	document.getElementById('advanced_search').rows[0].cells[0].getElementsByTagName('select')[0].focus();
}

function hide_column(id)
{
	for (j=0; j<maintable.tBodies[0].rows.length+2; j++)
		maintable.rows[j].cells[dynamictable.grid[id].order].style.display = 'none';

	maintable.rows[1].cells[dynamictable.grid[id].order].getElementsByTagName('input')[0].value='';

	dynamictable.grid[id].show = 0;
	dynamictable.grid[id].filter = null;

	dynamictable_update();
}

function show_column(id)
{
	for (j=0; j<maintable.tBodies[0].rows.length+2; j++)
		maintable.rows[j].cells[dynamictable.grid[id].order].style.display = '';

	dynamictable.grid[id].show = 1;

	dynamictable_update();
}

function dynamictable_column_search()
{
	for (i=0; i < dynamictable.grid.length;i++)
	{
		dynamictable.grid[i].filter = maintable.tHead.rows[1].cells[dynamictable.grid[i].order].getElementsByTagName('input')[0].value;
		if (maintable.tHead.rows[1].cells[dynamictable.grid[i].order].getElementsByTagName('input')[0] == document.activeElement)
			focus = dynamictable.grid[i].order;
	}

	dynamictable.page=1;
	dynamictable_update();
	maintable.tHead.rows[1].cells[focus].getElementsByTagName('input')[0].focus();
	return true;
}

function update_advanced_search_button()
{
	if (dynamictable.advanced_search.length==0)
	{
		document.getElementById('advanced_search_button').classList.remove("norightborder");
		document.getElementById('advanced_search_reset_button').style.display = 'none';
	}
	else
	{
		document.getElementById('advanced_search_button').classList.add("norightborder");
		document.getElementById('advanced_search_reset_button').style.display = 'inline';
	}
}

function update_show_hide_button()
{

	if (dynamictable.grid.reduce(function(a, b) { return a + b.show; }, 0) == dynamictable.grid.length)
	{
		document.getElementById('show_hide_button').classList.remove("norightborder");
		document.getElementById('show_hide_reset_button').style.display = 'none';
	}
	else
	{
		document.getElementById('show_hide_button').classList.add("norightborder");
		document.getElementById('show_hide_reset_button').style.display = 'inline';
	}
}

function dynamictable_save_settings()
{
	settings = new Object();
	settings.global_search = document.getElementById('global_search').value;
	settings.advanced_search = dynamictable.advanced_search;
	settings.results = dynamictable.results;
	settings.array_type = 'numeric';
	settings.page = dynamictable.page;
	settings.version = dynamictable.version;

	settings.fields = new Object();
	
	settings.fields = [];
	for (i=0; i < dynamictable.grid.length; i++)
	{
		settings.fields[i] = new Object();
		settings.fields[i].name = dynamictable.grid[i].name;
		settings.fields[i].dblink = dynamictable.grid[i].dblink;
		settings.fields[i].filter = dynamictable.grid[i].filter;
		settings.fields[i].sort_rank = dynamictable.grid[i].sort_rank;
		settings.fields[i].sort_order = dynamictable.grid[i].sort_order;
		settings.fields[i].show = dynamictable.grid[i].show;
		settings.fields[i].order = dynamictable.grid[i].order;
		settings.fields[i].width = dynamictable.grid[i].width;
	}

	api_call(dynamictable.user_server,'optimus/'+get_cookie('user')+'/settings', 'POST', {'user':dynamictable.db,'module':dynamictable.module,'settings':settings});
	return settings;
}

function dynamictable_load_settings()
{
	settings = api_call_sync(dynamictable.server,'optimus/'+get_cookie('user')+'/settings', 'GET', {'user':dynamictable.db,'module':dynamictable.module});
	settings = settings.data;

	if (typeof settings.version != 'undefined' && settings.version != dynamictable.version)
	{
		settings = api_call_sync(dynamictable.user_server,'optimus/'+get_cookie('user')+'/settings', 'DELETE', {'user':dynamictable.db,'module':dynamictable.module});
		window.location.reload();
	}

	if (typeof settings.fields != 'undefined')
	{
		settings.fields = JSON.parse(settings.fields);
		for (i=0; i < dynamictable.grid.length; i++)
		{
			dynamictable.grid[i].filter = settings.fields[i].filter;
			dynamictable.grid[i].sort_rank = settings.fields[i].sort_rank;
			dynamictable.grid[i].sort_order = settings.fields[i].sort_order;
			dynamictable.grid[i].show = settings.fields[i].show;
			dynamictable.grid[i].order = settings.fields[i].order;
			dynamictable.grid[i].width = settings.fields[i].width;
		}
	}

	if (typeof settings.advanced_search != 'undefined')
		dynamictable.advanced_search = JSON.parse(settings.advanced_search);
	else if (typeof dynamictable.advanced_search === 'undefined')
		dynamictable.advanced_search = new Array();

	if (typeof settings.global_search != 'undefined')
		dynamictable.global_search = JSON.parse(settings.global_search);
	else if (typeof dynamictable.global_search === 'undefined')
		dynamictable.global_search = '';

	if (typeof settings.results != 'undefined')
		dynamictable.results = JSON.parse(settings.results);
	else if (typeof dynamictable.results === 'undefined')
		dynamictable.results = 30;
	
	if(screen.width <= 576)
		dynamictable.results = 999;

	if (typeof settings.page != 'undefined')
		dynamictable.page = JSON.parse(settings.page);
	else if (typeof dynamictable.page === 'undefined')
		dynamictable.page = 1;

	return dynamictable;
}

function dynamictable_update(init)
{
	if (typeof(database)!='undefined')
		dynamictable_save_settings();

	database = api_call_sync(dynamictable.server,'optimus/'+dynamictable.db+'/'+dynamictable.module, 'GET', dynamictable_save_settings());
	dynamictable.total = database.total || 0;
	database = database.data;

	if (database)
		data = database.slice(0);
	else
		data = [];

	dynamictable_body();
	dynamictable_header();
	dynamictable_header_sorts();
	dynamictable_footer();
	update_advanced_search_button();
	update_show_hide_button();
}

window.onresize = function()
{
	dynamictable_header();
}