function api_call(server, endpoint, method, data, return_function, obj)
{
	if (typeof login_iframe === 'object')
		return setTimeout(function(){api_call(server, endpoint, method, data, return_function, obj)},500);

	if (method == 'GET')
	{
		fetch_endpoint = 'https://api.' + server + '/' + endpoint + '?data=' + JSON.stringify(data);
		fetch_options = {headers: {}, method: method, credentials: "include"};
	}
	else
	{
		fetch_endpoint = 'https://api.' + server + '/' + endpoint;
		fetch_options = {headers: {}, method: method, credentials: "include", body: JSON.stringify(data)};
	}

	fetch(fetch_endpoint,fetch_options)
	.then(response => response.json())
	.then(function(response)
	{
		if (response.code === 401 && response.message == 'Access denied - No Token')
		{
			login_open(server);
			api_call(server, endpoint, method, data, return_function, obj);
			return false;
		}
		else if (response.code >= 400)
			alert("ERREUR " + response.code + "\n"+response.message);
		if (window[return_function])
			window[return_function](response,obj);
		return true;
	})
	//.catch(error => console.log("Error : " + error));
}


function api_call_sync(server, endpoint, method, data)
{
	if (typeof login_iframe === 'object')
		return setTimeout(function(){api_call_sync(server, endpoint, method, data)},500);
	
	ajax = new XMLHttpRequest();
	if (method == 'GET')
		ajax.open(method, 'https://api.' + server + '/' + endpoint + '?data=' + JSON.stringify(data), false);
	else
		ajax.open(method, 'https://api.' + server + '/' + endpoint, false);
	//ajax.setRequestHeader("Content-type", "application/json");
	ajax.withCredentials = true;
	ajax.send(JSON.stringify(data));
	
	if(ajax.readyState == 4)
		if (ajax.responseText)
		{
			response = JSON.parse(ajax.responseText);
			if (response.code === 401 && response.message == 'Access denied' && response.error == 'No Token')
			{
				login_open(server);
				setTimeout(function(){api_call_sync(server, endpoint, method, data)},500);
				return false;
			}
			else if (response.code >= 400)
				alert("ERREUR " + response.code + "\n" + response.message);
			return response;
		}
		else
			return true;
	else
		return false;
}

function login_open(server)
{
	login_iframe = document.createElement('iframe');
	login_iframe.frameBorder=0;
	login_iframe.style.position = 'fixed';
	login_iframe.style.background = '';
	login_iframe.style.left = '0';
	login_iframe.style.top = '0';
	login_iframe.style.width = '100%';
	login_iframe.style.height = '100%';
	if(navigator.userAgent.indexOf("Chrome") > -1 )
		login_iframe.style.backdropFilter = 'brightness(50%) blur(3px)';
	else
		login_iframe.style.backgroundColor = 'rgba(0, 0, 0, 0.7)';
	login_iframe.src = "/login.php?server="+server;
	document.body.appendChild(login_iframe);
}


function login_close()
{
	login_iframe.parentNode.removeChild(login_iframe);
	login_iframe = undefined;
}


var messageEventHandler = function(event)
{
	if(event.data == 'logged')
	login_close();
}

window.addEventListener('message', messageEventHandler,false);