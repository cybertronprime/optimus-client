function load_jsfile (path)
{
	var obj = document.createElement('script');
	obj.setAttribute("type","text/javascript");
	obj.setAttribute("src", path);
	document.getElementsByTagName("head")[0].appendChild(obj);
}

function load_cssfile (path)
{
	var obj = document.createElement("link");
	obj.setAttribute("rel", "stylesheet");
	obj.setAttribute("type", "text/css");
	obj.setAttribute("href", path);
	document.getElementsByTagName("head")[0].appendChild(obj);
}

function curtain_open()
{
	body_overflow = document.body.style.overflow;
	document.body.style.overflow = 'hidden';
	curtain = document.createElement('div');
	curtain.className = 'curtain';
	document.body.appendChild(curtain);
	return curtain;
}

function curtain_close()
{
	document.body.removeChild(curtain);
	document.body.style.overflow = body_overflow;
}

function alert_open()
{
	alertbox = document.createElement('div');
	alertbox.className = 'alert';
	document.body.appendChild(alertbox);
	return alertbox;
}

function alert_close()
{
	document.body.removeChild(alertbox);
}

function notification(text, color)
{
	timer = (new Date()).getTime();
	window['notificationbox'+timer] = document.createElement('div');
	window['notificationbox'+timer].classList.add('notification');
	window['notificationbox'+timer].style.color = color;
	window['notificationbox'+timer].innerHTML = text;
	if (typeof curtain != 'undefined')
	{
		curtain.appendChild(window['notificationbox'+timer]);
		setTimeout("curtain.removeChild(window['notificationbox'+"+timer+"]);",2800);
	}
	else
	{
		document.body.appendChild(window['notificationbox'+timer]);
		setTimeout("document.body.removeChild(window['notificationbox'+"+timer+"]);",2800);
	}
}

function load_tab()
{
	for (obj of document.getElementsByName('tabs'))
		if (obj.checked == true)
			document.getElementById('frame').src = obj.getAttribute('link') + window.location.search;
}

function detect_browser()
{
	if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 )
		return 'Opera';
	else if(navigator.userAgent.indexOf("Chrome") != -1 )
		return 'Chrome';
	else if(navigator.userAgent.indexOf("Safari") != -1)
		return 'Safari';
	else if(navigator.userAgent.indexOf("Firefox") != -1 )
		return 'Firefox';
	else if((navigator.userAgent.indexOf("MSIE") != -1 ))
		return 'IE';
	else if((navigator.userAgent.indexOf("Edge") != -1 ))
		return 'Edge';
	else if((navigator.userAgent.indexOf("Opera") != -1 ))
		return 'Opera';
	else
		return 'Unknown';
} 