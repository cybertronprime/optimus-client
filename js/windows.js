windowZ = 999;

var optimus_window = function(url,w,h,title,movable,resizable)
{
	this.container = document.createElement('div');
	this.container.className = 'window';
	this.container.style.width = w + 'px';
	this.container.style.height = h + 'px';
	this.container.style.zIndex = windowZ++;
	this.container.style.transform = 'translate(-50%, -50%)';
	this.container.style.left = '50%';
	this.container.style.top = '50%';
	document.body.appendChild(this.container);
	
	this.titlebar = document.createElement('div');
	this.titlebar.className = 'window_titlebar';
	if (title)
		this.titlebar.innerHTML = title;
	this.titlebar.draggable = true;
	this.titlebar.onmousedown = function(){this.offsetParent.style.zIndex = windowZ++}
	if (movable==true)
	{
		this.titlebar.style.cursor = 'move';
		this.titlebar.ondragstart = function(event)
		{
			event.target.offsetParent.style.opacity = 0.7;
			x = event.target.offsetParent.offsetLeft - event.pageX;
			y = event.target.offsetParent.offsetTop - event.pageY;
		}
		this.titlebar.ondrag = function(event)
		{
			if (event.pageX>0)
				this.offsetParent.style.left=x+event.pageX+'px';
			if (event.pageY>0)
				this.offsetParent.style.top=y+event.pageY+'px';
		}
		this.titlebar.ondragend = function(event)
		{
			for (i=0;i<document.body.getElementsByTagName('iframe').length;i++)
				document.body.getElementsByTagName('iframe')[i].style.display = 'block';
			event.target.offsetParent.style.opacity = 1;
			this.offsetParent.style.left=x+event.pageX+'px';
			this.offsetParent.style.top=y+event.pageY+'px';
		}
	}
	if (resizable==true)
		this.titlebar.ondblclick = function()
		{
			this.offsetParent.max.onclick();
		}
	this.container.appendChild(this.titlebar);

	if (url!='')
	{
		this.content = document.createElement('iframe');
		this.content.className = 'window_content';
		this.content.style.height = this.container.offsetHeight-this.titlebar.offsetHeight-2+'px';
		this.content.src = url;
		this.container.appendChild(this.content);
	}
	else
	{
		this.content = document.createElement('div');
		this.content.className = 'window_content';
		this.content.id = 'content';
		this.container.appendChild(this.content);
	}
	
	
	if (resizable==true)
	{
		this.min = document.createElement('canvas');
		this.min.className = 'window_minimize_button';
		this.titlebar.appendChild(this.min);
		this.min.width=26;
		this.min.height=18;
		context = this.min.getContext('2d');
		context.fillStyle = '#FFFFFF';
		context.strokeStyle = '#535353';
		context.fillRect(6.5,10.5,12,3);
		context.strokeRect(6.5,10.5,12,4);
			
		this.max = document.createElement('canvas');
		this.max.className = 'window_maximize_button';
		this.titlebar.appendChild(this.max);
		this.max.width=26;
		this.max.height=18;
		context = this.max.getContext('2d');
		context.fillStyle = '#FFFFFF';
		context.strokeStyle = '#535353';
		context.fillRect(6.5,5.5,12,9);
		context.strokeRect(6.5,5.5,12,9);
		context.strokeRect(9.5,8.5,6,3);
		context.clearRect(9.5,8.5,6,3);
		
		this.res = document.createElement('canvas');
		this.res.className = 'window_restore_button';
		this.titlebar.appendChild(this.res);
		this.res.width=26;
		this.res.height=18;
		context = this.res.getContext('2d');
		context.fillStyle = '#FFFFFF';
		context.strokeStyle = '#535353';
		context.fillRect(9.5,3.5,9,9);
		context.strokeRect(9.5,3.5,9,9);
		context.fillRect(7.5,5.5,9,9);
		context.strokeRect(7.5,5.5,9,9);
		context.strokeRect(10.5,8.5,3,3);
		context.clearRect(10.5,8.5,3,3);
		
		this.min.onclick = function()
		{
			this.offsetParent.getElementsByClassName('window_minimize_button')[0].style.visibility = 'hidden';
			this.offsetParent.getElementsByClassName('window_restore_button')[0].style.visibility = 'visible';
			this.offsetParent.getElementsByClassName('window_maximize_button')[0].style.visibility = 'visible';
			this.offsetParent.offsetParent.style.height = '28px';
			this.offsetParent.offsetParent.children[1].style.display = 'none';
			this.offsetParent.offsetParent.style.width = w + 'px';
		}
		
		this.max.onclick = function()
		{
			this.offsetParent.getElementsByClassName('window_maximize_button')[0].style.visibility = 'hidden';
			this.offsetParent.getElementsByClassName('window_restore_button')[0].style.visibility = 'visible';
			this.offsetParent.getElementsByClassName('window_minimize_button')[0].style.visibility = 'visible';
			this.offsetParent.offsetParent.style.left = '0px';
			this.offsetParent.offsetParent.style.top = '0px';
			this.offsetParent.offsetParent.style.transform = 'translate(0, 0)';
			this.offsetParent.offsetParent.style.width = window.innerWidth + 'px';
			this.offsetParent.offsetParent.style.height = window.innerHeight + 'px';
			this.offsetParent.offsetParent.children[1].style.display = 'block';
			this.offsetParent.offsetParent.children[1].style.width = window.innerWidth + 'px';
			this.offsetParent.offsetParent.children[1].style.height = window.innerHeight - 28 + 'px';
		}
		
		this.res.onclick = function()
		{
			this.offsetParent.getElementsByClassName('window_restore_button')[0].style.visibility = 'hidden';
			this.offsetParent.getElementsByClassName('window_maximize_button')[0].style.visibility = 'visible';
			this.offsetParent.getElementsByClassName('window_minimize_button')[0].style.visibility = 'visible';
			this.offsetParent.offsetParent.style.width = w + 'px';
			this.offsetParent.offsetParent.style.height = h + 'px';
			this.offsetParent.offsetParent.style.transform = 'translate(-50%, -50%)';
			this.offsetParent.offsetParent.style.left = '50%';
			this.offsetParent.offsetParent.style.top = '50%';
			this.offsetParent.offsetParent.children[1].style.display = 'block';
		}
	}
	
	this.clo = document.createElement('canvas');
	this.clo.className = 'window_close_button';
	this.titlebar.appendChild(this.clo);
	this.clo.width=46;
	this.clo.height=18;
	context = this.clo.getContext('2d');
	context.font = 'bold 18px Courier New';
	context.fillStyle = '#FFFFFF';
	context.strokeStyle = '#535353';
	context.lineWidth=1;
	context.moveTo(19.5,13.5);
	context.lineTo(21.5,13.5);
	context.lineTo(23.5,11.5);
	context.lineTo(25.5,13.5);
	context.lineTo(27.5,13.5);
	context.lineTo(28.5,12.5);
	context.lineTo(25.5,9.5);
	context.lineTo(25.5,8.5);
	context.lineTo(28.5,5.5);
	context.lineTo(27.5,4.5);
	context.lineTo(25.5,4.5);
	context.lineTo(23.5,6.5);
	context.lineTo(21.5,4.5);
	context.lineTo(19.5,4.5);
	context.lineTo(18.5,5.5);
	context.lineTo(21.5,8.5);
	context.lineTo(21.5,9.5);
	context.lineTo(18.5,12.5);
	context.lineTo(19.5,13.5);
	context.fill();
	context.stroke();

	this.clo.onclick = function()
	{
		if (this.offsetParent)
			document.body.removeChild(this.offsetParent.offsetParent);
		if(document.getElementsByClassName('window_titlebar').length==0)
			curtain_close();
		if (typeof datagrid_update == 'function')
			datagrid_update();
	}
	
	this.close = function(){this.clo.onclick()}
	this.mininimize = function(){this.min.onclick()}
	this.maximize = function(){this.max.onclick()}
	this.restore = function(){this.res.onclick()}
}