function lightbox_open(width, height)
{
	for (i=0; i < document.body.childNodes.length; i++)
		if ('style' in document.body.childNodes[i])
			document.body.childNodes[i].style.filter = 'blur(1px)';

	curtain = document.createElement('div');
	curtain.id = 'curtain';
	curtain.style.background = '#000000';
	curtain.style.opacity = '0.5';
	curtain.style.border = '0px';
	curtain.style.position = 'fixed';
	curtain.style.top = '0px';
	curtain.style.left = '0px';
	curtain.style.width = '100%';
	curtain.style.height = '100%';
	document.body.appendChild(curtain);

	div = document.createElement('div');
	div.id = 'lightbox';
	div.style.width = width+'px';
	div.style.height = height+'px';
	div.style.top = '50%';
	div.style.left = '50%';
	div.style.position = 'fixed';
	div.style.textAlign = 'center';
	div.style.transform = 'translate(-50%, -50%)';
	document.body.appendChild(div);

	return div;
}

function lightbox_close()
{
	for (i=0; i < document.body.childNodes.length; i++)
		if ('style' in document.body.childNodes[i])
			document.body.childNodes[i].style.filter = '';
		
	if (typeof curtain != 'undefined')
	{
		document.body.removeChild(curtain);
		delete curtain;
	}
	
	if (typeof lightbox != 'undefined')
	{
		document.body.removeChild(lightbox);
		delete lightbox;
	}
}


var webdav_explorer = function(server,root,sort_column)
{
	var _this = this;
	this.server = server;
	this.root = root;
	this.sort_column = sort_column;
	this.cutitems = new Array();
	this.copieditems = new Array();
	ctrl = false;
	shift = false;
	alt = false;
	lastclicked = 1;
	
	this.init = function()
	{
		menus = new Array;    //name,   short_desc      long_desc             function                        multi,single,zero,dirs,files,exts,keycode,icon
		menus[menus.length] = ['open','Ouvrir','Ouvrir un dossier (ENTREE)',function(){_this.open()},false, true, false, true, false ,[], '', 13, '/lib/fontawesome/folder-open.svg'];
		menus[menus.length] = ['mkdir','Créer Dossier','Créer un dossier (F10)',function(){_this.mkdir_window()},true, true, true, true, true ,[], '', 121, '/lib/fontawesome/folder-plus.svg'];
		menus[menus.length] = ['mkfile','Créer Fichier','Créer un fichier (F11)',function(){_this.mkfile_window()},true,true,true,true,true,[], '', 122, '/lib/fontawesome/file-medical.svg'];
		menus[menus.length] = ['cut','Couper','Couper (CTRL+X)',function(){_this.cut()},true,true,true,true,true,[], 'ctrl', 88, '/lib/fontawesome/cut.svg'];
		menus[menus.length] = ['copy','Copier','Copier (CTRL+C)',function(){_this.copy()},true,true,true,true,true,[], 'ctrl', 67, '/lib/fontawesome/copy.svg'];
		menus[menus.length] = ['paste','Coller','Coller (CTRL+V)',function(){_this.paste()},true,true,true,true,true,[], 'ctrl', 86, '/lib/fontawesome/paste.svg'];
		menus[menus.length] = ['rename','Renommer','Renommer (F2)',function(){_this.rename_window()},false,true,false,true,true,[], '', 113, '/lib/fontawesome/i-cursor.svg'];
		menus[menus.length] = ['delete','Supprimer','Supprimer (DEL)',function(){_this.delete()},true,true,false,true,true,[], '', 46, '/lib/fontawesome/trash.svg'];
		menus[menus.length] = ['download','Télécharger','Télécharger (CTRL+F5)',function(){_this.download()},false,true,false,false,true,[], 'ctrl', 116, '/lib/fontawesome/download.svg'];
		menus[menus.length] = ['zip','Zip','Télécharger en zip (CTRL+F6)',function(){_this.inprogress()},true,false,false,true,true,[], 'ctrl', 117, '/lib/fontawesome/file-archive.svg'];
		menus[menus.length] = ['generate','Générer','Générer un modèle (CTRL+G)',function(){_this.inprogress()},true,true,true,true,true,[], 'ctrl', 71, '/lib/fontawesome/file-import.svg'];
		menus[menus.length] = ['share','Partager','Partager (F8)',function(){_this.inprogress()},false,true,false,true,true,[], '', 119, '/lib/fontawesome/share-alt.svg'];
		menus[menus.length] = ['display','Afficher','Afficher / Streamer (CTRL+ENTREE)',function(){_this.display()},false,true,false,false,true,['mp3','ogg','wav','mp4','webm','ogg','jpg','jpeg','png','gif','bmp','webp','svg'], 'ctrl', 13, '/lib/fontawesome/stream.svg'];
		menus[menus.length] = ['edit','LibreOffice','Editer avec LibreOffice (CTRL+L)',function(){_this.edit_with_lo()},false,true,false,false,true,[], 'ctrl', 76, '/lib/fontawesome/edit.svg'];
		menus[menus.length] = ['edit','MS-Office','Editer avec MS-Office (CTRL+M)',function(){_this.edit_with_msoffice()},false,true,false,false,true,[], 'ctrl', 77, '/lib/fontawesome/edit.svg'];
		menus[menus.length] = ['convertpdf','&rarr; PDF','Convertir en pdf (CTRL+D)',function(){_this.convert_to_pdf()},false,true,false,false,true,[], 'ctrl', 68, '/lib/fontawesome/pdf.svg'];
		menus[menus.length] = ['help','Aide','Aide (CTRL+F1)',function(){_this.inprogress()},true,true,true,true,true,[], 'ctrl', 112, '/lib/fontawesome/help.svg'];
		
		var container = document.createElement('div'); container.className = "container";
		container.oncontextmenu = function(){_this.contextmenu(this,event);return false;}
		container.onclick = function(){contextmenu.style.display='none'}
		
		menu = document.createElement('table'); menu.className = "menu"; container.appendChild(menu);
		menu_thead = document.createElement('thead');menu.appendChild(menu_thead);
		menu_tr = document.createElement('tr');menu_thead.appendChild(menu_tr);
		for (i=0; i<menus.length; i++)
		{
			item = document.createElement('td'); 
			item.innerHTML = '<img src="'+menus[i][12]+'" style="height:32px;opacity:0.6"/><br/>'+menus[i][1]; 
			item.onclick = menus[i][3];
			item.style.display = 'none';
			menu_tr.appendChild(item);
		}
		menu_empty = document.createElement('td'); menu_empty.innerHTML = '';menu_tr.appendChild(menu_empty);
		
		breadcrumb = document.createElement('table');breadcrumb.className = "breadcrumb";container.appendChild(breadcrumb);
		breadcrumb_thead = document.createElement('thead');breadcrumb.appendChild(breadcrumb_thead);
		breadcrumb_thead_tr = document.createElement('tr');breadcrumb_thead.appendChild(breadcrumb_thead_tr);
		
		content_container = document.createElement('div');
		content_container.className = "content_container";
		container.appendChild(content_container);
		
		content = document.createElement('table');
		content.className = "content";
		content_container.appendChild(content);
		content_thead = document.createElement('THEAD');
		content.appendChild(content_thead);
		content_thead_tr = document.createElement('tr');
		content_thead.appendChild(content_thead_tr);
		content_thead_tr_td0 = document.createElement('td');
		content_thead_tr.appendChild(content_thead_tr_td0);
		content_thead_tr_td0_input = document.createElement('input');
		content_thead_tr_td0_input.type = 'checkbox';
		content_thead_tr_td0_input.onchange = function(){_this.selectall(this,event)}
		content_thead_tr_td0.appendChild(content_thead_tr_td0_input);
		content_thead_tr_td1 = document.createElement('td');
		content_thead_tr_td1.innerHTML = '<span></span>';
		content_thead_tr.appendChild(content_thead_tr_td1);
		content_thead_tr_td2 = document.createElement('td');
		content_thead_tr_td2.onclick = function(){if (_this.sort_column==1) _this.sort_column=-1; else _this.sort_column=1;_this.sort();_this.draw()}
		content_thead_tr_td2.innerHTML = '<span></span> Nom';
		content_thead_tr.appendChild(content_thead_tr_td2);
		content_thead_tr_td3 = document.createElement('td');
		content_thead_tr_td3.onclick = function(){if (_this.sort_column==2) _this.sort_column=-2; else _this.sort_column=2;_this.sort();_this.draw()}
		content_thead_tr_td3.innerHTML = '<span></span> Modifié le';
		content_thead_tr.appendChild(content_thead_tr_td3);
		content_thead_tr_td4 = document.createElement('td');
		content_thead_tr_td4.onclick = function(){if (_this.sort_column==3) _this.sort_column=-3; else _this.sort_column=3;_this.sort();_this.draw()}
		content_thead_tr_td4.innerHTML = '<span></span> Type';
		content_thead_tr.appendChild(content_thead_tr_td4);
		content_thead_tr_td5 = document.createElement('TD');
		content_thead_tr_td5.onclick = function(){if (_this.sort_column==4) _this.sort_column=-4; else _this.sort_column=4;_this.sort();_this.draw()}
		content_thead_tr_td5.innerHTML = '<span></span> Taille';
		content_thead_tr.appendChild(content_thead_tr_td5);
		content_tbody = document.createElement('tbody');
		content.appendChild(content_tbody);
		
		contextmenu = document.createElement('table');
		contextmenu.className = 'contextmenu';
		contextmenu.onclick = function(){contextmenu.style.display = 'none';}
		contextmenu.style.display = 'none';
		document.body.appendChild(contextmenu);
		contextmenu_tbody = document.createElement('tbody');
		contextmenu.appendChild(contextmenu_tbody);
		for (i=0; i<menus.length; i++)
		{
			item = document.createElement('tr'); 
			item.innerHTML = '<td><img src="'+menus[i][12]+'" />'+menus[i][2]+'</td>'; 
			item.onclick = menus[i][3];
			item.oncontextmenu =function(){return false}
			contextmenu_tbody.appendChild(item);
		}

		_this.populate();
		
		return container;
	}
	
	this.populate = function()
	{
		files = new Array();
		folders = new Array();
		backlink = new Array();
		backlink[0] = [0,'..',new Date().getTime(),'',0,'back'];
		client.propFind(_this.root, ['{DAV:}getcontentlength', '{DAV:}getlastmodified', '{DAV:}resourcetype'],'1').then(
		function(result) 
		{
			var re = /(?:\.([^.]+))?$/;
			for (var i=1; i<result['body'].length; i++)
			{
				extension = re.exec(result['body'][i]['href'])[1];
				
				lastmodified = new Date(result['body'][i]['propStat'][0]['properties']['{DAV:}getlastmodified']);
				lastmodified = lastmodified.getTime();
				if (result['body'][i]['propStat'][0]['properties']['{DAV:}resourcetype'])
				{
					type = 'folder';
					filename = decodeURIComponent(result['body'][i]['href']).replace(_this.root,'').replace('/','').replace('/','');
					extension = '';
					size = '';
					if (filename.substring(0,1) != '.')
						folders[folders.length] = [i,filename,lastmodified,extension,size,type];
				}
				else
				{
					type = 'file';
					filename = decodeURIComponent(result['body'][i]['href']).replace(_this.root,'').replace('/','').replace('/','');
					extension = re.exec(result['body'][i]['href'])[1];
					size = parseInt(result['body'][i]['propStat'][0]['properties']['{DAV:}getcontentlength']);
					if (filename.substring(0,1) != '.')
						files[files.length] = [i,filename,lastmodified,extension,size,type];
				}
			}
			_this.sort();
			_this.draw();
			_this.updatemenu();
			
		},
		function(err) 
		{
			console.log(err);
			return false;
		}
		)
	}

	this.sort = function()
	{
		_this.sortbycolumn(folders,_this.sort_column);
		_this.sortbycolumn(files,_this.sort_column);
	}
	
	this.sortbycolumn = function (a, col)
	{
		a.sort(sortfunction);
	
		function sortfunction(a, b)
		{
			if (typeof a[Math.abs(col)] == "string")
			{
				if (a[Math.abs(col)].toLowerCase() === b[Math.abs(col)].toLowerCase())
					return 0;
				else if (col>0)
					return (a[Math.abs(col)].toLowerCase() < b[Math.abs(col)].toLowerCase()) ? -1 : 1;
				else if (col<0)
					return (a[Math.abs(col)].toLowerCase() < b[Math.abs(col)].toLowerCase()) ? 1 : -1;
			}
			else
			{
				if (a[Math.abs(col)] === b[Math.abs(col)])
					return 0;
				else if (col>0)
					return (a[Math.abs(col)] < b[Math.abs(col)]) ? -1 : 1;
				else if (col<0)
					return (a[Math.abs(col)] < b[Math.abs(col)]) ? 1 : -1;
			}
		}
		
		return a;
	}

	this.draw = function()
	{
		breadcrumb_thead_tr.innerHTML='';
		breadcrumb_server = document.createElement('td'); breadcrumb_server.innerHTML = _this.server; breadcrumb_thead_tr.appendChild(breadcrumb_server);
		breadcrumb_levels = _this.root.split('/');
		newroot = '';
		for(i=1; i < breadcrumb_levels.length; i++)
		{
			breadcrumb_spacer = document.createElement('td'); breadcrumb_spacer.innerHTML = '&rsaquo;'; breadcrumb_thead_tr.appendChild(breadcrumb_spacer);
			breadcrumb_level = document.createElement('td');
			breadcrumb_level.innerHTML = breadcrumb_levels[i];
			newroot += '/' + breadcrumb_levels[i];
			breadcrumb_level.newroot = newroot;
			breadcrumb_level.onclick = function(){_this.root = this.newroot;history.pushState(null, null, '/modules/fichiers/index.php?server='+server+'&path='+_this.root);_this.populate()}
			breadcrumb_thead_tr.appendChild(breadcrumb_level);
		}
		breadcrumb_empty = document.createElement('td'); breadcrumb_empty.innerHTML = ''; breadcrumb_thead_tr.appendChild(breadcrumb_empty);
		
		items = backlink.concat(folders).concat(files);
		for (var h=1; h < content_thead_tr.cells.length-1; h++)
			if (Math.abs(_this.sort_column) == h && _this.sort_column > 0)
				content_thead_tr.cells[h+1].firstChild.innerHTML='&#9650;';
			else if (Math.abs(_this.sort_column) == h && _this.sort_column < 0)
				content_thead_tr.cells[h+1].firstChild.innerHTML='&#9660;';
			else
				content_thead_tr.cells[h+1].firstChild.innerHTML='&nbsp;';
		
		content_tbody.innerHTML='';
		for (var i=0; i < items.length; i++)
		{
			tr = document.createElement('tr');
			tr.id = items[i][0];
			tr.tabIndex = items[i][0];
			tr.setAttribute('type',items[i][5]);
			tr.oncontextmenu = function() {_this.contextmenu(this, event);return false};
			tr.style.webkitUserSelect = 'none';
			tr.onclick = function() {_this.click(this, event)};
			tr.ondblclick = function() {_this.dblclick(event,this);return false};
			
			td0 = document.createElement('td');
			if (i > 0)
				td0.innerHTML = '<input type="checkbox" />';
			tr.appendChild(td0);
			
			td1 = document.createElement('td');
			td1.style.paddingTop = '4px';
			if (items[i][5]=='back')
			{
				td1.innerHTML = '<img src="/images/filetypes/back.png"/>';
				tr.ondblclick = function(){_this.dblclick(event,this)};
			}
			else if (items[i][5]=='folder')
				td1.innerHTML = '<img src="/images/filetypes/folder.png"/>';
			else if (items[i][5]=='file')
				td1.innerHTML = '<img src="/images/filetypes/' + items[i][3].toLowerCase() + '.png" onerror="this.src=\'/images/filetypes/file.png\'"/>';
			tr.appendChild(td1);
			
			
			td2 = document.createElement('td');
			td2.innerHTML = items[i][1];
			tr.appendChild(td2);
			
			td3 = document.createElement('td');
			lastmodified = new Date(items[i][2]);
			if (items[i][5]!='back')
				td3.innerHTML = ("0" + lastmodified.getDate()).slice(-2) + "/" + ("0"+(lastmodified.getMonth()+1)).slice(-2) + "/" + lastmodified.getFullYear() + " " + ("0" + lastmodified.getHours()).slice(-2) + ":" + ("0" + lastmodified.getMinutes()).slice(-2); 
			tr.appendChild(td3);
			
			td4 = document.createElement('td');
			if (items[i][5]=='folder')
				td4.innerHTML = 'File folder';
			else if (items[i][5]=='back')
				td4.innerHTML = '';
			else if (items[i][5]=='file')
				td4.innerHTML = items[i][3].toUpperCase() + ' File';
			tr.appendChild(td4);
			
			td5 = document.createElement('td');
			td5.style.textAlign = 'right';
			if (items[i][5]=='file')
				td5.innerHTML = filesize(items[i][4]);
			tr.appendChild(td5);
			
			content_tbody.appendChild(tr);
		}
	}
	
	this.click = function(obj,event)
	{
		for(i=1; i < content_tbody.rows.length; i++)
			if (obj == content_tbody.rows[i])
				lastclicked = i;
		
		if (obj.id != '0')
		{
			if(event.target.checked == undefined && ctrl==false && shift==false)
			{
				for(i=1; i < content_tbody.rows.length; i++)
				{
					content_tbody.rows[i].style.backgroundColor = '#FFFFFF';
					content_tbody.rows[i].style.outline = '0px';
					content_tbody.rows[i].cells[0].firstChild.checked = false;
				}
				obj.style.backgroundColor = '#CCE8FF';
				obj.style.outline = '1px solid #99D1FF';
				obj.firstChild.firstChild.checked = true;
			}
			else if(shift == true)
			{
				for(i=1; i < content_tbody.rows.length; i++)
				{
					if (content_tbody.rows[i].cells[0].firstChild.checked==true)
						start = i;
					if (obj == content_tbody.rows[i])
						end = i;
				}
				if (typeof start != 'undefined')
				{
					for(i=Math.min(start,end); i <= Math.max(start,end); i++)
					{
						content_tbody.rows[i].style.backgroundColor = '#CCE8FF';
						content_tbody.rows[i].style.outline = '1px solid #99D1FF';
						content_tbody.rows[i].cells[0].firstChild.checked = true;
					}
				}
				else
				{
					obj.style.backgroundColor = '#CCE8FF';
					obj.style.outline = '1px solid #99D1FF';
					obj.firstChild.firstChild.checked = true;
				}
			}
			else
			{
				if (obj.style.outlineWidth == '1px')
				{
					obj.style.backgroundColor = '#FFFFFF';
					obj.style.outline = '0px';
					obj.firstChild.firstChild.checked = false;
				}
				else
				{
					obj.style.backgroundColor = '#CCE8FF';
					obj.style.outline = '1px solid #99D1FF';
					obj.firstChild.firstChild.checked = true;
				}
			}
			obj.focus();
			_this.updatemenu();
		}
	}
	
	this.updatemenu = function()
	{
		for (i=0; i < menus.length; i++)
			menu_tr.cells[i].style.display = 'none';
		
		for (i=0; i < contextmenu_tbody.rows.length; i++)
			contextmenu_tbody.rows[i].style.display = 'none';
		
		items = _this.selected_items();	

		loop1:
		for (i=0; i < menus.length; i++)
		{
			if (menus[i][4] == false && items.length > 1) continue;
			
			if (menus[i][5] == false && items.length == 1) continue;
			
			if (menus[i][6] == false && items.length == 0) continue;
			
			if (menus[i][7] == false)
				for (j=0; j < items.length; j++)
					if (items[j][1] == 'folder')
			 			continue loop1;
			
			if (menus[i][8] == false)
				for (k=0; k < items.length; k++)
					if (items[k][1] == 'file')
			 			continue loop1;
			
			if (menus[i][9].length > 0)
				for (l=0; l < items.length; l++)
					if (items[l][1] == 'file' && menus[i][9].includes(items[l][2]) == false)
						continue loop1;			
			
			menu_tr.cells[i].style.display = 'table-cell';
			contextmenu_tbody.rows[i].style.display = 'table-row';
		}
	}
	
	this.contextmenu = function(obj,event)
	{
		contextmenu.style.display = 'block';
		contextmenu.style.zIndex = 999;
		
		if (obj.firstChild.firstChild && obj.firstChild.firstChild.checked == false)
		{
			for(i=1; i < content_tbody.rows.length; i++)
			{
				content_tbody.rows[i].style.backgroundColor = '#FFFFFF';
				content_tbody.rows[i].style.outline = '0px';
				content_tbody.rows[i].cells[0].firstChild.checked = false;
			}
			_this.click(obj,event);
		}
		
		if (event.pageX > document.documentElement.offsetWidth / 2)
			contextmenu.style.left = event.pageX - contextmenu.offsetWidth + 'px';
		else
			contextmenu.style.left = event.pageX + 'px';

		if (event.pageY + contextmenu.offsetHeight > document.documentElement.offsetHeight)
				contextmenu.style.top = event.pageY - contextmenu.offsetHeight + 'px';
		else
			contextmenu.style.top = event.pageY + 'px';
		
		return false;
	}
	
	this.open = function()
	{
		items = _this.selected_items();
		if (items[0][1] == 'folder')
		{
			_this.root = _this.root + '/' + items[0][4];
			_this.populate();
			history.pushState(null, null,'/modules/fichiers/index.php?server='+server+'&path='+_this.root);
		}
	}
	
	
	this.dblclick = function(event,obj)
	{
		if (obj.getAttribute('type')=='folder')
		{
			_this.root = _this.root + '/' + obj.cells[2].innerHTML;
			history.pushState(null, null, '/modules/fichiers/index.php?server='+server+'&path='+_this.root);
			_this.populate();
		}
		else if (obj.getAttribute('type')=='back' && root.lastIndexOf('/'))
		{
			_this.root = _this.root.substring(0,_this.root.lastIndexOf('/'));
			history.pushState(null, null, '/modules/fichiers/index.php?server='+server+'&path='+_this.root);
			_this.populate();
		}
		else if (obj.getAttribute('type')=='file')
		{
			for(i=1; i < content_tbody.rows.length; i++)
				if (obj != content_tbody.rows[i])
				{
					content_tbody.rows[i].style.backgroundColor = '#FFFFFF';
					content_tbody.rows[i].style.outline = '0px';
					content_tbody.rows[i].cells[0].firstChild.checked = false;
				}
				else
				{
					content_tbody.rows[i].style.backgroundColor = '#CCE8FF';
					content_tbody.rows[i].style.outline = '1px solid #99D1FF';
					content_tbody.rows[i].cells[0].firstChild.checked = true;
				}
			_this.display();
		}
	}
	
	this.selectall = function(obj,event)
	{
		for(i=1; i < content_tbody.rows.length; i++)
			if (obj.checked == true)
			{
				content_tbody.rows[i].style.backgroundColor = '#CCE8FF';
				content_tbody.rows[i].style.outline = '1px solid #99D1FF';
				content_tbody.rows[i].cells[0].firstChild.checked = true;
			}
			else
			{
				content_tbody.rows[i].style.backgroundColor = '#FFFFFF';
				content_tbody.rows[i].style.outline = '0px';
				content_tbody.rows[i].cells[0].firstChild.checked = false;
			}
			
			_this.updatemenu();
	}
	
	document.ondragover = function(event) 
	{
		event.preventDefault();
	}
	
	document.ondragenter = function(event) 
	{
		event.preventDefault();
	}
	
	document.ondrop = function(event) 
	{
		box = lightbox_open(400,24);

		progress = document.createElement('progress');
		progress.id = 'progress';
		progress.style.width = '400px';
		progress.style.height = '24px';
		progress.value = 0;
		box.appendChild(progress);
  
		progress_value = document.createElement('span');
		progress_value.id = 'progress_value';
		progress_value.style.position = 'relative';
		progress_value.style.top = '-24px';
		progress_value.style.font = 'bold 20px Verdana';
		progress_value.innerHTML = '0%';
		box.appendChild(progress_value);
		
		br = document.createElement('br');
		box.appendChild(br);
		
		progress_text = document.createElement('span');
		progress_text.id = 'progress_text';
		progress_text.style.position = 'relative';
		progress_text.style.top = '-18px';
		progress_text.style.color = '#FFFFFF';
		progress_text.style.font = 'normal 14px Verdana';
		box.appendChild(progress_text);
		
		event.preventDefault();
		event.stopPropagation();
		
		items = event.dataTransfer.items;
		total_size = 0;
		total_count = 0;
		
		counter1 = 0;
		counter2 = 0;
		for (i=0; i<items.length; i++)
		{
			var item = items[i].webkitGetAsEntry();
			if (item)
				_this.upload(item);
		}
	}
	
	document.onkeydown = function (event)
	{
		if (event.keyCode == 27)
		{
			lightbox_close();
			return true;
		}
		
		if (event.keyCode == 16)
		{
			shift = true;
			ctrl = false;
			alt = false;
			return true;
		}
		else if (event.keyCode == 17)
		{
			shift = false;
			ctrl = true;
			alt = false;
			return true;
		}
		else if (event.keyCode == 18)
		{
			shift = false;
			ctrl = false;
			alt = true;
			return true;
		}
		
		if (typeof curtain != 'undefined')
			return true;
		
		if (event.keyCode == 13 || event.keyCode == 39)
		return(_this.open());
		
		if (event.keyCode == 8 || event.keyCode == 37)
			return(content_tbody.rows[0].ondblclick());
		
		if (content_tbody.rows.length < 2)
			return false;
		
		for (x=0; x < menus.length; x++)
			if (menus[x][10] == 'ctrl' && ctrl==true && event.keyCode == menus[x][11])
				return(menus[x][3]());
			else if (menus[x][10] == 'shift' && shift==true && event.keyCode == menus[x][11])
				return(menus[x][3]());
			else if (menus[x][10] == 'alt' && alt==true && event.keyCode == menus[x][11])
				return(menus[x][3]());
			else if (menus[x][10] == '' && ctrl==false && shift==false && alt==false && event.keyCode == menus[x][11])
				return(menus[x][3]());
		
		if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 65 && event.keyCode <= 90) || (event.keyCode >= 96 && event.keyCode <= 105))
		{
			for (x=1; x < content_tbody.rows.length; x++)
				if (content_tbody.rows[x].cells[2].innerHTML.substring(0,1).toLowerCase() == String.fromCharCode(event.keyCode).toLowerCase())
					return(content_tbody.rows[x].click());
			return true;
		}
		
		if (event.keyCode == 38)
		{
			event.preventDefault();

			if (lastclicked == 1)
				return true;

			if (shift == true)
			{
				if (content_tbody.rows[lastclicked-1].firstChild.firstChild.checked == false)
					content_tbody.rows[lastclicked-1].click();
				else
				{
					shift = false;
					ctrl = true;
					content_tbody.rows[lastclicked].click();
					shift = true;
					ctrl = false;
					lastclicked--;
				}
			}
			else if (ctrl == true)
			{
				lastclicked--;
				content_tbody.rows[lastclicked].firstChild.firstChild.focus();
			}
			else
			{
				content_tbody.rows[lastclicked-1].click();
				content_tbody.rows[lastclicked].click();
			}
			return true;
		}

		if (event.keyCode == 40)
		{
			event.preventDefault();

			if (lastclicked == content_tbody.rows.length - 1)
				return true;
			
			if (shift == true)
			{
				if (content_tbody.rows[lastclicked+1].firstChild.firstChild.checked == false)
					content_tbody.rows[lastclicked+1].click();
				else
				{
					shift = false;
					ctrl = true;
					content_tbody.rows[lastclicked].click();
					shift = true;
					ctrl = false;
					lastclicked++;
				}
			}
			else if (ctrl == true)
			{
				lastclicked++;
				content_tbody.rows[lastclicked].firstChild.firstChild.focus();
			}
			else
			{
				content_tbody.rows[lastclicked+1].click();
				content_tbody.rows[lastclicked].click();
			}
			return true;
		}
		
	}

	document.onkeyup = function (event)
	{
		if (event.keyCode == 16 || event.keyCode == 17 || event.keyCode == 18)
		{
			shift = false;
			ctrl = false;
			alt = false;
		}
	}

	this.upload = function(item, path)
	{
		loaded = new Array();
		processed_files = 0;
		total_files = 0;
		path = path || "";
		if (item.isFile) 
		{
			item.file(function(file) 
			{
				total_files++;
				progress.max += file.size;
				
				client.request('PUT', _this.root+'/'+path+file.name,'',file).then(
				function(result) 
				{
					processed_files++;
					if (processed_files==total_files)
					{
						lightbox_close();
						_this.populate();
					}

					console.log(result);
				},
				function(err) 
				{
					console.log(err);
				});
			});
		}
		else if (item.isDirectory) 
		{
			client.request('MKCOL', _this.root+'/'+path+item.name).then(
			function(result) 
			{
				console.log(result);
			},
			function(err) 
			{
				console.log(err);
			}
				);

		var dirReader = item.createReader();
		dirReader.readEntries(function(entries){for (var i=0; i<entries.length; i++) _this.upload(entries[i], path + item.name + "/");});
		}
	}
	
	this.selected_items = function()
	{
		items = new Array();
		for(i=1; i < content_tbody.rows.length; i++)
			if (content_tbody.rows[i].cells[0].firstChild.checked == true)
				items[items.length] = [content_tbody.rows[i],content_tbody.rows[i].getAttribute('type'), content_tbody.rows[i].cells[2].innerHTML.split('.').pop().toLowerCase(), _this.root + '/' + content_tbody.rows[i].cells[2].innerHTML, content_tbody.rows[i].cells[2].innerHTML];
		return items;
	}

	this.rename_window = function()
	{
		items = _this.selected_items();
		
		if (typeof curtain != 'undefined' || items.length==0) return false;
		
		box = lightbox_open(400,90);
		curtain.onclick = function(){lightbox_close()}

		box.style.border = '1px solid #000000';
		box.style.background = '#FFFFFF';
		box.style.font = 'normal 12px Verdana';
		box.style.padding = '10px';
		
		box.innerHTML = 'Nouveau nom:<br/><br/>';
		input_text = document.createElement('INPUT');
		input_text.type = 'text';
		input_text.id = 'rename_input';
		input_text.value = items[0][4];
		input_text.onkeyup = function(){filename_validation(this)}
		input_text.onkeydown = function(event){if (event&&event.keyCode==13) _this.rename_command(items[0][4],rename_input.value)}
		input_text.style.marginBottom = '16px';
		input_text.style.width = '350px';
		box.appendChild(input_text);
		br = document.createElement('BR');
		box.appendChild(br);
		input_button = document.createElement('INPUT');
		input_button.type = 'button';
		input_button.value = 'RENOMMER';
		input_button.onclick = function(){_this.rename_command(items[0][4],rename_input.value)}
		box.appendChild(input_button);
		rename_input.focus();
		rename_input.setSelectionRange(rename_input.value.lastIndexOf('.'),rename_input.value.lastIndexOf('.'));
	}

	this.rename_command = function(oldname,newname)
	{
		if (oldname != rename_input.value)
			request = client.request('MOVE',encodeURI(_this.root)+'/'+encodeURI(oldname),{'Destination':_this.server+encodeURI(_this.root)+'/'+encodeURI(newname)}).then
			(
				function(result) 
				{
					console.log(result);
					if (result.status < 300)
					{
						items[0][0].cells[2].innerHTML = newname;
						lightbox_close();
					}
					else
						alert('ERROR '+result.status+' : '+result.xhr.statusText+"\n"+result.xhr.responseText.split('<s:message>').pop().split('</s:message>').shift());
				},
				function(err) 
				{
					console.log(err);
				}
			)
		else
			lightbox_close();
	}

	this.delete = function()
	{
		items = _this.selected_items();
		
		if (items.length > 0)
		{
			box = lightbox_open(400,24);

			progress = document.createElement('progress');
			progress.id = 'progress';
			progress.style.width = '400px';
			progress.style.height = '24px';
			progress.value = 0;
			box.appendChild(progress);
	  
			progress_value = document.createElement('span');
			progress_value.id = 'progress_value';
			progress_value.style.position = 'relative';
			progress_value.style.top = '-24px';
			progress_value.style.font = 'bold 20px Verdana';
			progress_value.innerHTML = '0%';
			box.appendChild(progress_value);

			progress.max = items.length;

			for(i=0; i < items.length; i++)
			request = client.request('DELETE', items[i][3].replace(/&amp;/g, '%26')).then(
			function(result) 
			{
				console.log(result);
				progress.value++;
				progress_value.innerHTML =  Math.round(progress.value / progress.max * 100) + '%';

				if (result.status < 300)
				{
					for(i=0; i < items.length; i++)
						if (items[i][4] == decodeURI(result.xhr.responseURL).split('/').pop())
							content_tbody.removeChild(items[i][0]);
				}
				else
					alert(result.xhr.responseText);

				if (progress.value==progress.max)
					lightbox_close();
			},
			function(err) 
			{
				console.log(err);
				alert(result.xhr.responseText);
			}
			);
		}
	}

	this.mkdir_window = function()
	{
		if (typeof curtain != 'undefined') return false;
		box = lightbox_open(400,90);
		box.style.border = '1px solid #000000';
		box.style.background = '#FFFFFF';
		box.style.font = 'normal 12px Verdana';
		box.style.padding = '10px';

		box.innerHTML = 'Nom du dossier à créer:<br/><br/>';
		input_text = document.createElement('INPUT');
		input_text.type = 'text';
		input_text.id = 'mkdir_input';
		input_text.onkeyup = function(){filename_validation(this)}
		input_text.onkeydown = function(event){if (event&&event.keyCode==13) _this.mkdir_command(mkdir_input.value)}
		input_text.style.marginBottom = '16px';
		box.appendChild(input_text);
		br = document.createElement('BR');
		div.appendChild(br);
		input_button = document.createElement('INPUT');
		input_button.type = 'button';
		input_button.value = 'CREER LE DOSSIER';
		input_button.onclick = function(){_this.mkdir_command(mkdir_input.value)}
		box.appendChild(input_button);
		input_text.focus();
	}

	this.mkdir_command = function(newname)
	{
		request = client.request('MKCOL',encodeURI(_this.root)+'/'+encodeURI(newname)).then
		(
			function(result) 
			{
				console.log(result);
				folders[folders.length] = [content_tbody.rows.length,newname,new Date(),'','','folder'];
				_this.sort();
				_this.draw();
				lightbox_close();
			},
			function(err) 
			{
				console.log(err);
			}
		)
	}

	this.mkfile_window = function()
	{
		if (typeof curtain != 'undefined') return false;
		box = lightbox_open(400,90);
		box.style.border = '1px solid #000000';
		box.style.background = '#FFFFFF';
		box.style.font = 'normal 12px Verdana';
		box.style.padding = '10px';
		
		box.innerHTML = 'Nom du fichier à créer:<br/><br/>';
		input_text = document.createElement('INPUT');
		input_text.type = 'text';
		input_text.id = 'mkfile_input';
		input_text.onkeyup = function(){filename_validation(this)}
		input_text.onkeydown = function(event){if (event&&event.keyCode==13) _this.mkfile_command(mkfile_input.value)}
		input_text.style.marginBottom = '16px';
		box.appendChild(input_text);
		br = document.createElement('BR');
		div.appendChild(br);
		input_button = document.createElement('INPUT');
		input_button.type = 'button';
		input_button.value = 'CREER LE DOSSIER';
		input_button.onclick = function(){_this.mkfile_command(mkfile_input.value)}
		box.appendChild(input_button);
		mkfile_input.focus();
	}

	this.mkfile_command = function(newname)
	{
		request = client.request('PUT',encodeURI(_this.root)+'/'+encodeURI(newname)).then
		(
			function(result) 
			{
				console.log(result);
				files[files.length] = [content_tbody.rows.length,newname.split('.').shift(),new Date(),newname.split('.').pop(),0,'file'];
				_this.sort();
				_this.draw();
				lightbox_close();
			},
			function(err) 
			{
				console.log(err);
			}
		)
	}

	this.download = function()
	{
		items = _this.selected_items();
		anchor = document.createElement('a');
		anchor.href = _this.server+items[0][3];
		document.body.appendChild(anchor);
		anchor.click();
		document.body.removeChild(anchor);
	}

	this.display = function()
	{
		items = _this.selected_items();
		if (items[0][2]=='mp3' || items[0][2]=='ogg' || items[0][2]=='wav')
			_this.streamaudio(items[0][3]);
		else if (items[0][2]=='mp4' || items[0][2]=='webm' || items[0][2]=='ogg')
			_this.streamvideo(items[0][3]);
		else if (items[0][2]=='jpg' || items[0][2]=='jpeg' || items[0][2]=='png' || items[0][2]=='gif' || items[0][2]=='bmp' || items[0][2]=='webp' || items[0][2]=='svg')
			_this.streamimage(items[0][3]);
		else if (items[0][2]=='doc' || items[0][2]=='docx' || items[0][2]=='xls' || items[0][2]=='xlsx' || items[0][2]=='ppt' || items[0][2]=='pptx')
			_this.edit_with_msoffice(items[0][3]);
		else if (items[0][2]=='odt')
			_this.edit_with_lo(items[0][3]);
		else if (items[0][2]=='pdf')
			_this.streampdf(items[0][3]);
		else
			alert('ce format de fichier n\'est pas supporté par le navigateur','_blank');
		
	}

	this.streamaudio = function(file)
	{
		if (typeof curtain != 'undefined') return false;
		box = lightbox_open(document.documentElement.clientWidth,50);
		curtain.onclick = function(){lightbox_close()}
		audio = document.createElement('audio');
		audio.style.height = '50px';
		audio.controls = true;
		box.appendChild(audio);
		
		source = document.createElement('source');
		source.src = _this.server + file;
		audio.appendChild(source);
		
		audio.play();
	}

	this.streamvideo = function(file)
	{
		if (typeof curtain != 'undefined') return false;
		box = lightbox_open(document.documentElement.clientWidth * 0.75,document.documentElement.clientHeight * 0.75);
		curtain.onclick = function(){lightbox_close()}
		video = document.createElement('video');
		video.style.height = '100%';
		video.controls = true;
		box.appendChild(video);
		
		source = document.createElement('source');
		source.src = _this.server + file;
		video.appendChild(source);
		
		video.play();
	}

	this.streamimage = function(file)
	{
		if (typeof curtain != 'undefined') return false;
		box = lightbox_open('auto','auto');
		curtain.onclick = function(){lightbox_close()}
		img = document.createElement('img');
		img.src = _this.server + file;
		img.style.height = 'auto';
		img.style.width = 'auto';
		box.appendChild(img);
	}

	this.streampdf = function()
	{
		fetch(server + _this.root + '/' + items[0][4], {credentials: 'include'})
		.then(function(response) {return response.blob()})
		.then(function(blob) 
		{
			var reader = new FileReader() ;
			reader.onload = function()
			{
				new_window = window.open('','_blank');
				new_window.document.title = items[0][4];
				new_window.document.body.style.overflow = 'hidden';
				new_window.document.body.style.overflow = 'hidden';
				iframe = document.createElement('iframe');
				iframe.src = 'data:application/pdf;base64,' + this.result.replace('data:application/octet-stream;base64,','');
				iframe.style.position = 'absolute';
				iframe.style.left = 0;
				iframe.style.top = 0;
				iframe.style.border = 0;
				iframe.style.width = '100%';
				iframe.style.height = '100%';
				new_window.document.body.appendChild(iframe);
			}
			reader.readAsDataURL(blob) ;
		});
	}


	this.edit_with_msoffice = function()
	{
		items = _this.selected_items();
		extension = items[0][4].split('.')[items[0][4].split('.').length-1];
		if (extension == 'doc' || extension == 'odt' || extension == 'docx' || extension == 'dot')
			window.location.href = 'ms-word:ofe|u|'+_this.server+encodeURI(_this.root+'/'+items[0][4]);
		else if (extension == 'ppt' || extension == 'pptx')
			window.location.href = 'ms-powerpoint:ofe|u|'+_this.server+encodeURI(_this.root+'/'+items[0][4]);
		else if (extension == 'xls' || extension == 'xlsx')
			window.location.href = 'ms-excel:ofe|u|'+_this.server+encodeURI(_this.root+'/'+items[0][4]);
		else if (extension == 'pub')
			window.location.href = 'ms-publisher:ofe|u|'+_this.server+encodeURI(_this.root+'/'+items[0][4]);
		else if (extension == 'pdf')
			window.location.href = 'acrobat:ofe|u|'+_this.server+encodeURI(_this.root+'/'+items[0][4]);
	}

	this.edit_with_lo = function()
	{
		items = _this.selected_items();
		extension = items[0][4].split('.')[items[0][4].split('.').length-1];
		window.location.href = "vnd.libreoffice.command:ofe|u|"+_this.server+encodeURI(_this.root+'/'+items[0][4]);
	}

	this.convert_to_pdf = function()
	{
		document.documentElement.style.cursor = 'wait';
		items = _this.selected_items();
		response = api_call_sync(server.replace('https://cloud.',''),'optimus/'+_this.root.split('/')[2]+'/files/','GET',{'action':'convert_to_pdf','file':_this.root+'/'+items[0][4]})
		if (response.code == 200)
			_this.populate();
		document.documentElement.style.cursor = 'default';
	}

	this.cut = function()
	{
		items = _this.selected_items();
		_this.cutitems = items;
		_this.copieditems = new Array();
		for(i=0; i < items.length; i++)
			items[i][0].style.color = '#FF0000';
	}

	this.copy = function()
	{
		items = _this.selected_items();
		_this.copieditems = items;
		_this.cutitems = new Array();
		for(i=0; i < items.length; i++)
			items[i][0].style.color = '#00AA00';
	}

	this.paste = function()
	{
		if (_this.copieditems.length > 0)
		{
			pasteitems = _this.copieditems;
			command = 'COPY';
		}
		else if (_this.cutitems.length > 0)
		{
			pasteitems = _this.cutitems;
			command = 'MOVE';
		}
		else
			return false;
			
		if (_this.copieditems.length ==1 && pasteitems[0][3] == _this.root + '/' + pasteitems[0][4])
		{
			var re = /(?:\.([^.]+))?$/;
			filename = re.exec(pasteitems[0][4])[0];
			extension = pasteitems[0][4].split('.')[pasteitems[0][4].split('.').length-1];
			pasteitems[0][4] = pasteitems[0][4] + " (copy)." + extension;
		}

		box = lightbox_open(400,24);

		progress = document.createElement('progress');
		progress.id = 'progress';
		progress.style.width = '400px';
		progress.style.height = '24px';
		progress.value = 0;
		box.appendChild(progress);

		progress_value = document.createElement('span');
		progress_value.id = 'progress_value';
		progress_value.style.position = 'relative';
		progress_value.style.top = '-24px';
		progress_value.style.font = 'bold 20px Verdana';
		progress_value.innerHTML = '0%';
		box.appendChild(progress_value);

		progress.max = pasteitems.length;
		
		for (i=0; i < pasteitems.length; i++)
			request = client.request(command, pasteitems[i][3], {'Destination':_this.server+encodeURI(_this.root)+'/'+encodeURI(pasteitems[i][4]),'Overwrite':'F'}).then
			(
				function(result) 
				{
					progress.value++;
					progress_value.innerHTML =  Math.round(progress.value / progress.max * 100) + '%';
				
					console.log(result);
					
					if (progress.value==progress.max)
					{
						lightbox_close();
						_this.populate();
					}
				},
				function(err) 
				{
					console.log(err);
				}
			)
	}

	this.inprogress = function()
	{
		box = lightbox_open(500,24);
		curtain.onclick = function(){lightbox_close()}
		box.style.border = '1px solid #000000';
		box.style.background = '#FFFFFF';
		box.style.font = 'bold 16px Verdana';
		box.style.padding = '10px';
		alert = document.createElement('span');
		alert.style.color = '#FF0000';
		alert.innerHTML = "Cette fonction n'a pas encore été implémentée";
		box.appendChild(alert);
	}
}



function filesize(bytes) 
{
	if (bytes < 1024) return "1 KB";
	if (bytes < 1048576) return Math.round(bytes/1024) + " KB";
	if (bytes < 1073741824) return Math.round(bytes/1024/1024) + " MB";
	if (bytes < 1099511600000) return Math.round(bytes/1024/1024/1024) + " GB";
	return Math.round(bytes/1024/1024/1024/1024) + " TB";
}

function filename_validation(obj)
{
	val = obj.value.toString();
	start = obj.selectionStart;
	stp = obj.selectionEnd;
	len = val.length;
	val=val.replace(/\//g,'');
	val=val.replace(/\\/g,'');
	val=val.replace(/\?/g,'');
	val=val.replace(/\</g,'');
	val=val.replace(/\>/g,'');
	val=val.replace(/:/g,'');
	val=val.replace(/\*/g,'');
	val=val.replace(/\|/g,'');
	val=val.replace(/\s+/g,' ');
	val=val.replace(/[.](?=.*[.])/g, "");
	obj.value=val;
	obj.setSelectionRange(start+val.length-len,stp+val.length-len)
}