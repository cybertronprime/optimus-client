<!DOCTYPE HTML>
<html lang="fr">
	<head>
		<title>OPTIMUS</title>
		<meta name="viewport" content="width=410, initial-scale=1">
		<link rel="icon" type="image/png" sizes="16x16" href="/images/logo/favicon_16x16.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/images/logo/favicon_32x32.png">
		<link rel="icon" type="image/png" sizes="180x180" href="/images/logo/favicon_180x180.png">
		<link rel="icon" type="image/png" sizes="192x192" href="/images/logo/favicon_192x192.png">
		<link rel="icon" type="image/png" sizes="512x512" href="/images/logo/favicon_512x512.png">
		<link rel="stylesheet" href="/css/main.css">
		<link rel="stylesheet" href="/css/login.css">
		<script src="/js/cookies.js"></script>
	</head>

	<body align="center" style="font:normal 12px Verdana;background:url('')">
		
		<img id="logo" src="/images/logo/optimus-avocats.svg"/>
		<div class="login">
			Serveur : <input type="text" id="server" style="width:200px" spellcheck="false" autocomplete="url" onkeyup="if (event.keyCode === 13) login()" /><br/>
			Email : <input type="text" id="email" style="width:200px" spellcheck="false" autocomplete="email" onkeyup="if (event.keyCode === 13) login()"/><br/>
			Mot de passe : <input type="password" id="password" style="width:200px" spellcheck="false" autocomplete="current-password" onkeyup="if (event.keyCode === 13) login()"/><br/>
			<div style="text-align:center"><input type="submit" value="CONNEXION" onclick="login()"/></div>
		</div>
		
	</body>

	<script>
		if(self != top)
			document.getElementById('logo').style.display = 'none';
		
		if (get_cookie('server')!='undefined')
		{
			document.getElementById('server').value = get_cookie('server');
			document.getElementById('email').focus();
		}
		if (get_cookie('db'))
		{
			document.getElementById('email').value = get_cookie('db');
			document.getElementById('password').focus();
		}
		else
			document.getElementById('server').focus();
		
		const query = new URLSearchParams(window.location.search);
		//if (query.get('server') != 'undefined')
			//document.getElementById('server').value = query.get('server');

		function login()
		{
			fetch('https://api.' + document.getElementById('server').value + '/optimus/login',
			{
				method: "POST",
				credentials: "include",
				body: JSON.stringify({"email": document.getElementById('email').value, "password": document.getElementById('password').value})
			})
			.then(function(response)
			{
			if (response.status === 200)
			{
				set_cookie('server',document.getElementById('server').value,100000);
				set_cookie('db',document.getElementById('email').value,100000);
				set_cookie('user',document.getElementById('email').value,100000);
				parent.postMessage('logged','*');
			}
			else
				return response.json();
			})
			.then(function(response)
			{
			if (response && response.message)
				alert(response.message);
			})
			.catch(error => alert("Le serveur '" + document.getElementById('server').value + "' ne répond pas\n" + error.message));
		}
		
		if (window.location.toString().substring(0,13) == 'https://demo.')
		{
			document.getElementById('server').value = 'demoptimus.fr';
			document.getElementById('email').value = 'demo@demoptimus.fr';
			document.getElementById('password').value = '4f0JDn9JWTqlFVHrUihyybNVWJpF98hY';
		}
	</script>
</html>
